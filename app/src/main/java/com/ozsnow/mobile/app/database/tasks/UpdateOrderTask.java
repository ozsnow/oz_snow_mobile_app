package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.models.vos.OrderVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class UpdateOrderTask extends AsyncTask<OrderVo, Void, Void>
{
    private OrderDataSource dataSource;

    public UpdateOrderTask(OrderDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected Void doInBackground(OrderVo... orderVos)
    {
        dataSource.updateOrder(orderVos[0]);
        return null;
    }


}
