package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.models.vos.OrderVo;

import java.util.ArrayList;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class ReadAllOrdersTask extends AsyncTask<Void, Void, ArrayList<OrderVo>>
{
    private OrderDataSource dataSource;

    public ReadAllOrdersTask(OrderDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected ArrayList<OrderVo> doInBackground(Void... params)
    {
        return dataSource.readLiveOrders();
    }

}
