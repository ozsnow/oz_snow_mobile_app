package com.ozsnow.mobile.app.services.terminal;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ozsnow.mobile.app.services.ServiceDTO;
import com.ozsnow.mobile.app.services.OzSnowService;
import cz.msebera.android.httpclient.Header;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by russellmilburn on 25/03/2016.
 */
public class TerminalDataHandler extends Handler
{
    private static final String TAG = TerminalDataHandler.class.toString();
    private TerminalDataService service;
    private AsyncHttpClient client;
    private ServiceDTO vo;
    private int startId;

    @Override
    public void handleMessage(Message msg)
    {
        vo = (ServiceDTO) msg.obj;
        startId = msg.arg1;
        callTerminalIdService();
    }

    private void callTerminalIdService()
    {
        client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 1000);
        //client.setTimeout(1000);

        //%7BdeviceId%7D
        //{deviceId}
        String urlPrams = null;
        try {
            urlPrams = URLEncoder.encode("{deviceId}", "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        client.get(OzSnowService.BASE_URL + vo.command.getCommandPath() + "/"+vo.uuid, null, new AsyncHttpResponseHandler()
        {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
            {
                Log.i(TAG, "onSuccess() called with " + "i = [" + statusCode + "], headers = [" + headers + "], bytes = [" + responseBody + "]");
                int terminalId = Integer.parseInt(new String(responseBody));
                service.writeToPreferences(terminalId);
                service.stopSelf(startId);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable throwable)
            {
                Log.w(TAG, "onFailure() called with " + "i = [" + statusCode + "], headers = [" + headers + "], bytes = [" + responseBody + "]");

                //TODO: Add Error Handlers. App does not have a connection to the internet on start up warn user to connect and shutdown app
                //TODO: Shutdown service

                service.stopSelf(startId);
            }
        });
    }


    public void setService(TerminalDataService service) {
        this.service = service;
    }
}
