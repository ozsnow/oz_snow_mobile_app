package com.ozsnow.mobile.app.services.product;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;


import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.activities.MainMenuActivity;
import com.ozsnow.mobile.app.database.ProductDataSource;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.CategoryVo;
import com.ozsnow.mobile.app.models.vos.ProductTypeVo;
import com.ozsnow.mobile.app.models.vos.ProductVo;
import com.ozsnow.mobile.app.services.OzSnowService;
import com.ozsnow.mobile.app.services.ServiceDTO;


import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.conn.params.ConnManagerParams;
import cz.msebera.android.httpclient.params.HttpConnectionParams;
import cz.msebera.android.httpclient.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import static android.content.Context.MODE_PRIVATE;
import static com.ozsnow.mobile.app.OzSnowPosApplication.OZ_SNOW_PREF;

/**
 * Created by russellmilburn on 26/03/2016.
 */
public class ProductServiceHandler extends Handler
{

    private static final String TAG = ProductServiceHandler.class.toString();
    private ProductDownloadService service;
    private int startId;
    private ServiceDTO vo;
    private AsyncHttpClient client;
    private ProductDataSource dataSource;

    @Override
    public void handleMessage(Message msg)
    {
        vo = (ServiceDTO) msg.obj;
        startId = msg.arg1;

        switch (vo.command)
        {
            case CHECK_PRODUCTS_UPDATED:
                checkIfProductUpdated();
                break;

            case GET_ALL_PRODUCTS:
                getAllProducts();
                break;
        }
    }

    private void removeAllProductData()
    {
        dataSource.deleteProductData();
    }



    private void getAllProducts()
    {
        if (client == null)
        {
            client = new AsyncHttpClient();
        }

        client.get(OzSnowService.BASE_URL + vo.command.getCommandPath(), null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int status, Header[] headers, byte[] responseBody) {

                String contentString = new String(responseBody);

                removeAllProductData();

                try {
                    JSONObject data = new JSONObject(contentString);
                    JSONArray productList = data.getJSONArray("productList");
                    JSONArray productTypeList  = data.getJSONArray("productTypeList");
                    JSONArray categoryEntityList  = data.getJSONArray("categoryEntityList");
                    JSONObject pue = data.getJSONObject("lastUpdated");
                    Date date = new Date(Long.parseLong(pue.getString("lastUpdated")));

                    Log.i(TAG, String.valueOf(productList.length()));
                    Log.i(TAG, String.valueOf(productTypeList.length()));
                    Log.i(TAG, String.valueOf(categoryEntityList.length()));
                    Log.i(TAG, date.toString());

                    for (int i = 0; i < productList.length(); i++)
                    {
                        ProductVo vo = new ProductVo(productList.getJSONObject(i));
                        dataSource.createProduct(vo);
                    }

                    for (int j = 0; j < productTypeList.length(); j++)
                    {
                        ProductTypeVo productTypeVo = new ProductTypeVo(productTypeList.getJSONObject(j));
                        dataSource.createProductType(productTypeVo);
                    }

                    for (int k = 0; k < categoryEntityList.length(); k++)
                    {
                        CategoryVo categoryVo = new CategoryVo(categoryEntityList.getJSONObject(k));
                        dataSource.createCategory(categoryVo);
                    }

                    JSONArray seasonArray = data.getJSONArray("seasonData");
                    JSONObject thredbo = seasonArray.getJSONObject(0);
                    JSONObject pershier = seasonArray.getJSONObject(1);

                    Log.i(TAG, thredbo.toString());
                    Log.i(TAG, pershier.toString());

                    service.writeThredboSeason(thredbo);
                    service.writePerisherSeason(pershier);

//
                    service.writeToPreferences(date);

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }

                service.stopSelf(startId);

            }

            @Override
            public void onFailure(int i, Header[] headers, byte[] bytes, Throwable throwable)
            {
                //TODO: Add error checking
                //TODO: Shutdown service and start main menu
                service.openMainMenu();
                service.stopSelf(startId);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);
                Log.i(TAG, (bytesWritten / totalSize) * 100 +"");

                int p = (int) ((bytesWritten * 1.0 / totalSize) * 100);

                service.setLoadProgress(p);

                Log.i(TAG, String.format("Progress %d from %d (%2.0f%%)", bytesWritten, totalSize, (totalSize > 0) ? (bytesWritten * 1.0 / totalSize) * 100 : -1));
            }
        });

        //
    }

    private void checkIfProductUpdated()
    {
        if (client == null)
        {
            client = new AsyncHttpClient();
        }

        SharedPreferences settings = service.getSharedPreferences(OZ_SNOW_PREF, MODE_PRIVATE);
        long dateNumber = settings.getLong(PreferenceList.LAST_PRODUCT_UPDATE.toString(), -1);

        Log.i(TAG, dateNumber + "");

        client.get(OzSnowService.BASE_URL + vo.command.getCommandPath()+"/"+dateNumber, null, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.i(TAG, "onSuccess() called with " + "i = [" + statusCode + "], headers = [" + headers + "], bytes = [" + responseBody + "]");

                String contentString = new String(responseBody);

                Boolean downloadProductUpdates = Boolean.valueOf(contentString);
                //Boolean downloadProductUpdates = true;

                Log.i(TAG, "Download a new Set of Product " + downloadProductUpdates);

                if (downloadProductUpdates)
                {
                    Log.i(TAG, "make new call for update ");
                    service.checkForNewProducts();
                }
                else
                {
                    Log.i(TAG, "onSuccess() no need for update ");
                    service.openMainMenu();
                }

                service.stopSelf(startId);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {
                Log.i(TAG, "onFailure() called with " + "i = [" + statusCode + "], headers = [" + headers + "], bytes = [" + bytes + "]");

                //TODO: Add error checking
                //TODO: Shutdown service and start main menu


                service.openMainMenu();
                service.stopSelf(startId);
            }
        });
    }




    public void setService(ProductDownloadService service)
    {
        this.service = service;

        dataSource = new ProductDataSource(service.getBaseContext());

    }
}
