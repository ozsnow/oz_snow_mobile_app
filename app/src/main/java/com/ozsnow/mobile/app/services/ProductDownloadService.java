package com.ozsnow.mobile.app.services;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.ResponseHandlerInterface;
import com.ozsnow.mobile.app.database.OzSnowDatabaseHelper;
import com.ozsnow.mobile.app.models.emum.RemoteServiceCommand;
import com.ozsnow.mobile.app.models.vos.ProductTypeVo;
import com.ozsnow.mobile.app.models.vos.ProductVo;

/**
 * Created by russellmilburn on 15/03/2016.
 */
public class ProductDownloadService extends Service
{
    public static final String BASE_URL = "http://10.0.2.2:8080/api";

    protected static final String TAG = ProductDownloadService.class.toString();

    private AsyncHttpClient client;
    private OzSnowDatabaseHelper databaseHelper;
    private SQLiteDatabase database;
    private final IBinder binder = new ProductServiceBinder();

    @Override
    public IBinder onBind(Intent intent) 
    {
        Log.i(TAG, "onBind() ");
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate() ");
        client = new AsyncHttpClient();
        databaseHelper = new OzSnowDatabaseHelper(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) 
    {
        Log.i(TAG, "onStartCommand() ");
        return START_STICKY;
    }

    public void addProduct(ProductVo productVo)
    {
        database = databaseHelper.getWritableDatabase();

        ContentValues cv = new ContentValues();
        cv.put(ProductVo.ProductFields.PRODUCT_ID, productVo.getProductID());
        cv.put(ProductVo.ProductFields.PRODUCT_NAME, productVo.getProductName());
        cv.put(ProductVo.ProductFields.PRODUCT_DESC, productVo.getProductDesc());
        cv.put(ProductVo.ProductFields.MOUNTAIN, productVo.getMountain());
        cv.put(ProductVo.ProductFields.CATEGORY_ID, productVo.getCategoryID());

        database.insert(ProductVo.ProductFields.PRODUCT_TBL, null, cv);
    }

    public void addProductType(ProductTypeVo productTypeVo)
    {
        database = databaseHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_ID, productTypeVo.getProductTypeID());
        cv.put(ProductVo.ProductFields.PRODUCT_ID, productTypeVo.getProductID());
        cv.put(ProductTypeVo.ProductTypeFields.PRODUCT_TYPE, productTypeVo.getProductType());
        cv.put(ProductTypeVo.ProductTypeFields.IS_HIRE_PRODUCT, productTypeVo.isHireProduct());
        cv.put(ProductTypeVo.ProductTypeFields.RES_CODE, productTypeVo.getResCode());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_1D, productTypeVo.getPrice1D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_2D, productTypeVo.getPrice2D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_3D, productTypeVo.getPrice3D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_4D, productTypeVo.getPrice4D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_5D, productTypeVo.getPrice5D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_6D, productTypeVo.getPrice6D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_7D, productTypeVo.getPrice7D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_8D, productTypeVo.getPrice8D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_9D, productTypeVo.getPrice9D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_10D, productTypeVo.getPrice10D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_11D, productTypeVo.getPrice11D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_12D, productTypeVo.getPrice12D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_13D, productTypeVo.getPrice13D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_14D, productTypeVo.getPrice14D().doubleValue());
        database.insert(ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_TBL, null, cv);
    }

    public class ProductServiceBinder extends Binder
    {
        public ProductDownloadService getProductDownloadService()
        {
            return ProductDownloadService.this;
        }
    }

    public void getUpdatedProducts(final ResponseHandlerInterface handler)
    {
        Log.i(TAG, "getUpdatedProducts() ");
        client.get(BASE_URL + RemoteServiceCommand.GET_ALL_PRODUCTS.getCommandPath(), null, handler);
    }


}
