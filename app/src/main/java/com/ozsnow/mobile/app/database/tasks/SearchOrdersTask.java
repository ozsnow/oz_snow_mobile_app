package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.models.vos.OrderVo;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class SearchOrdersTask extends AsyncTask<Date, Void, ArrayList<OrderVo>>
{
    private OrderDataSource dataSource;

    public SearchOrdersTask(OrderDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected ArrayList<OrderVo> doInBackground(Date... dates)
    {
        return dataSource.readOrdersByDate(dates[0], dates[1]);
    }

}
