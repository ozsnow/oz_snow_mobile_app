package com.ozsnow.mobile.app.models.vos;

import android.provider.BaseColumns;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by russellmilburn on 15/03/2016.
 */
public class ProductVo  implements Comparable<ProductVo>, Serializable{



    public static final class ProductFields implements BaseColumns
    {
        public static final String PRODUCT_TBL = "ProductTbl";

        public static final String PRODUCT_ID = "productId";
        public static final String PRODUCT_NAME = "productName";
        public static final String PRODUCT_DESC = "productDesc";
        public static final String MOUNTAIN = "mountain";
        public static final String CATEGORY_ID = "categoryId";
    }

    private int productID;
    private int categoryID;
    private String productName;
    private String productDesc;
    private String mountain;
    private ProductTypeVo productTypeVo;
    private CategoryVo categoryVo;
    private BigDecimal price;
    private int duration;




    public ProductVo()
    {

    }

    public ProductVo(JSONObject vo)
    {
        try {
            this.productID = vo.getInt(ProductFields.PRODUCT_ID);
            this.productName = vo.getString(ProductFields.PRODUCT_NAME);
            this.productDesc = vo.getString(ProductFields.PRODUCT_DESC);
            this.mountain = vo.getString(ProductFields.MOUNTAIN);
            this.categoryID = vo.getInt(ProductFields.CATEGORY_ID);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getMountain() {
        return mountain;
    }

    public void setMountain(String mountain) {
        this.mountain = mountain;
    }

    public ProductTypeVo getProductTypeVo() {
        return productTypeVo;
    }

    public void setProductTypeVo(ProductTypeVo productTypeVo) {
        this.productTypeVo = productTypeVo;
    }

    public CategoryVo getCategoryVo() {
        return categoryVo;
    }

    public void setCategoryVo(CategoryVo categoryVo) {
        this.categoryVo = categoryVo;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public int compareTo(ProductVo another)
    {
        int compare=((ProductVo)another).getDuration();
        return this.duration-compare;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString()
    {
        return "[ProductVo id: " + this.productID +
                ", productName: " + productName +
                "]";
    }
}
