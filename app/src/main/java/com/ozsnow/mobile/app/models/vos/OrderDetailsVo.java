package com.ozsnow.mobile.app.models.vos;

import android.provider.BaseColumns;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by russellmilburn on 28/04/2016.
 */
public class OrderDetailsVo implements Serializable
{
    public static final class OrderDetailsFields implements BaseColumns
    {
        public static final String ORDER_DETAILS_TBL = "OrderDetailsTbl";

        public static final String ORDER_DETAILS_ID = "orderDetailsId";
        public static final String QUANTITY = "quantity";
        public static final String PRICE = "price";
        public static final String RES_CODE = "resCode";
        public static final String NUMBER_OF_DAYS = "NoOfDays";

    }

    private int orderDetailsId;
    private int orderId;
    private int productId;
    private String productName;
    private String productDesc;
    private int categoryId;
    private int quantity;
    private String resCode;
    private int noOfDays;
    private BigDecimal price;

    public int getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(int orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getResCode() {
        return resCode;
    }

    public void setResCode(String resCode) {
        this.resCode = resCode;
    }

    public int getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(int noOfDays) {
        this.noOfDays = noOfDays;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }


    @Override
    public String toString()
    {
        return "[OrderDetailsVo id: " + this.orderDetailsId +
                ", orderId: " + orderId +
                ", productId: " + productId +
                ", price: " + price +
                "]";
    }
}
