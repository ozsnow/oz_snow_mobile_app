package com.ozsnow.mobile.app.services.printer;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.zj.btsdk.BluetoothService;

/**
 * Created by russellmilburn on 01/05/2016.
 */
public class ConnectionHandler extends Handler
{
    public static final String TAG  = ConnectionHandler.class.toString();

    public static final String DEVICE_CONNECTED = "deviceConnected";
    public static final String DEVICE_CONNECTING = "deviceConnecting";
    public static final String DEVICE_CONNECTION_LOST = "deviceConnectionLost";

    private Context context;

    private ThermalPrinterService service;


    public ConnectionHandler(Context context, ThermalPrinterService service)
    {
        super();
        this.context = context;
        this.service = service;
    }



    @Override
    public void handleMessage(Message msg)
    {
        super.handleMessage(msg);

        switch (msg.what)
        {
            case BluetoothService.MESSAGE_STATE_CHANGE:
                Log.i(TAG, msg.arg1 +"");
                switch (msg.arg1)
                {
                    case BluetoothService.STATE_CONNECTED:

                        Log.i(TAG, "BluetoothService.STATE_CONNECTED");

                        service.setConnected(true);
                        service.onConnectionStatus(DEVICE_CONNECTED);

                        break;
                    case BluetoothService.STATE_CONNECTING:
                        Log.i(TAG, "BluetoothService.STATE_CONNECTING");
                        service.onConnectionStatus(DEVICE_CONNECTING);

                        break;
                    case BluetoothService.STATE_LISTEN:
                        Log.i(TAG, "BluetoothService.STATE_LISTEN");
                        break;
                    case BluetoothService.STATE_NONE:
                        Log.i(TAG, "BluetoothService.STATE_NONE");
                        break;
                }
                break;
            case BluetoothService.MESSAGE_CONNECTION_LOST:
                Log.i(TAG, "BluetoothService.MESSAGE_CONNECTION_LOST");

                service.setConnected(false);
                service.onConnectionStatus(DEVICE_CONNECTION_LOST);

                break;
            case BluetoothService.MESSAGE_UNABLE_CONNECT:
                Log.i(TAG, "BluetoothService.MESSAGE_UNABLE_CONNECT");
                break;
        }

    }


    public ThermalPrinterService getService() {
        return service;
    }

    public void setService(ThermalPrinterService service) {
        this.service = service;
    }
}
