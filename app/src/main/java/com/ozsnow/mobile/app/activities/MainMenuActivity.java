package com.ozsnow.mobile.app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.ozsnow.mobile.app.MainActivity;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.database.tasks.ReadCurrentTourGuidesTask;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.SeasonVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;
import com.ozsnow.mobile.app.services.sync.SyncDataService;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by russellmilburn on 07/03/2016.
 */
public class MainMenuActivity extends AppCompatActivity
{
    public static final String TAG = MainMenuActivity.class.toString();

    public static final int CREATE_TOUR_GUIDE_REQUEST = 0;
    public static final String RESET_ORDER = "resetOrder";

    private Button newOrderBtn;
    private Button voidOrderBtn;
    private Button syncDataBtn;
    private Button addGuideBtn;
    private Button searchOrdersBtn;
//    private Button changeGuideBtn;
    private Button printerBtn;
    private TextView tourGuideText;

    private TourGuideVo currentTourGuide;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.primary_dark));


        setContentView(R.layout.activity_main_menu);

        newOrderBtn = (Button) findViewById(R.id.createOrderBtn);
        voidOrderBtn = (Button) findViewById(R.id.voidOrderBtn);
        syncDataBtn = (Button) findViewById(R.id.syncBtn);
        addGuideBtn = (Button) findViewById(R.id.addGuideBtn);
//        changeGuideBtn = (Button) findViewById(R.id.changeGuideBtn);
        printerBtn = (Button) findViewById(R.id.printerSettingsBtn);
        searchOrdersBtn = (Button) findViewById(R.id.searchOrdersBtn);

        tourGuideText = (TextView) findViewById(R.id.tourGuideText);

        addGuideBtn.setOnClickListener(addGuideClickListener);
        newOrderBtn.setOnClickListener(newOrderClickListener);
//        changeGuideBtn.setOnClickListener(changeTourGuideListener );
        printerBtn.setOnClickListener(printerSettingListener );
        voidOrderBtn.setOnClickListener(voidOrderListener );
        syncDataBtn.setOnClickListener(syncDataListener);
        searchOrdersBtn.setOnClickListener(searchOrdersListener);

        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);

        int tourGuideId = getTourGuideId();


        if (tourGuideId != -1)
        {
            retrieveTourGuide(tourGuideId);
        }

        if (getIntent().getAction() != null && getIntent().getAction().equals(RESET_ORDER))
        {
            SharedPreferences.Editor editor = settings.edit();
            editor.remove(PreferenceList.CURRENT_CUSTOMER.toString());
            editor.remove(PreferenceList.CURRENT_ORDER_ID.toString());
            editor.remove(PreferenceList.DESTINATION.toString());
        }



    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void retrieveTourGuide(int id)
    {
        TourGuideDataSource dataSource = new TourGuideDataSource(this);
        ReadCurrentTourGuidesTask task = new ReadCurrentTourGuidesTask(dataSource);
        TourGuideVo guide = null;

        try {
            guide = task.execute(id).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        currentTourGuide = guide;
        tourGuideText.setText(getResources().getString(R.string.current_tour_guide_text)+ " " + guide.getGuideName());

    }

    View.OnClickListener addGuideClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            if (getTourGuideId() == -1)
            {
                addTourGuide();
            }

        }
    };

    View.OnClickListener newOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            if (currentTourGuide == null)
            {
                Toast.makeText(MainMenuActivity.this, "You must choose a tour guide before you can place an order", Toast.LENGTH_LONG).show();
                return;
            }

            startNewOrder();
        }
    };


    View.OnClickListener changeTourGuideListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            changeTourGuide();
        }
    };



    View.OnClickListener searchOrdersListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainMenuActivity.this, ViewOrdersActivity.class);
            startActivity(intent);
        }
    };

    View.OnClickListener printerSettingListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainMenuActivity.this, PrinterSettingsActivity.class);
            startActivity(intent);
        }
    };


    View.OnClickListener voidOrderListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(MainMenuActivity.this, VoidOrderActivity.class);
            startActivity(intent);
        }
    };

    View.OnClickListener syncDataListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);

            if (!wifi.isWifiEnabled())
            {
                Toast.makeText(MainMenuActivity.this, "Wifi not connected. Connect to wifi and try again",Toast.LENGTH_LONG).show();
                return;
            }

            OrderDataSource dataSource = new OrderDataSource(MainMenuActivity.this);
            int unsyncedOrders = dataSource.checkNumberOfUnsyncedOrders();

            if (unsyncedOrders > 0)
            {
                Intent intent = new Intent(MainMenuActivity.this, SyncProgressActivity.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(MainMenuActivity.this, "No orders need to be synced.", Toast.LENGTH_LONG).show();
            }


        }
    };

    private void changeTourGuide()
    {
        Intent intent = new Intent(this, GuideListActivity.class);
        startActivityForResult(intent, CREATE_TOUR_GUIDE_REQUEST);
    }


    private void addTourGuide()
    {
        Intent intent = new Intent(this, TourGuideFormActivity.class);
        startActivityForResult(intent, CREATE_TOUR_GUIDE_REQUEST);
    }

    private void startNewOrder()
    {
        checkIfPeakSeason();

        Intent intent = new Intent(this, CustomerFormActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i(TAG, requestCode + "");

        switch (requestCode)
        {
            case CREATE_TOUR_GUIDE_REQUEST:

                if (resultCode == RESULT_OK)
                {
                    currentTourGuide = (TourGuideVo) data.getSerializableExtra(TourGuideFormActivity.TOUR_GUIDE_EXTRA);

                    Log.i(TAG, currentTourGuide.toString());
                    tourGuideText.setText(getResources().getString(R.string.current_tour_guide_text)+ " " + currentTourGuide.getGuideName());

                    SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putInt(PreferenceList.CURRENT_TOUR_GUIDE.toString(), currentTourGuide.getGuideId());
                    editor.commit();
                }
                break;
        }
    }


    private void checkIfPeakSeason()
    {
        boolean isPeak;

        SeasonVo thredbo = getSeasonData(PreferenceList.THREDBO_SEASON_DATES.toString());
        SeasonVo persiher = getSeasonData(PreferenceList.PERISHER_SEASON_DATES.toString());

        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(PreferenceList.IS_THREDBO_PEAK.toString(), isPeak(thredbo));
        editor.commit();

        editor.putBoolean(PreferenceList.IS_PERISHER_PEAK.toString(), isPeak(persiher));
        editor.commit();

    }

    private boolean isPeak(SeasonVo vo)
    {
        Date today = Calendar.getInstance().getTime();

        if (today.getTime() >= vo.getPeakStart() && today.getTime() <= vo.getPeakEnd() )
        {
            Log.i(TAG, vo.getDestination() + "True");
            return true;
        }
        else
        {
            Log.i(TAG, vo.getDestination() + "False");
            return false;
        }
    }

    private SeasonVo getSeasonData(String pref)
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        String seasonData = null;
        JSONObject json = null;
        SeasonVo vo = null;

        seasonData = settings.getString(pref, "");
        try
        {
            vo = new SeasonVo(new JSONObject(seasonData));
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }

        return vo;
    }

    private int getTourGuideId()
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        return settings.getInt(PreferenceList.CURRENT_TOUR_GUIDE.toString(), -1);
    }

}