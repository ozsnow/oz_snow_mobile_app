package com.ozsnow.mobile.app.activities;

import android.app.Activity;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.database.tasks.*;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.*;
import com.ozsnow.mobile.app.services.printer.ConnectionHandler;
import com.ozsnow.mobile.app.services.printer.ThermalPrinterService;
import com.ozsnow.mobile.app.utils.PrintBitmap;
import com.ozsnow.mobile.app.utils.PrinterFormatUtils;
import com.zj.btsdk.PrintPic;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import static com.ozsnow.mobile.app.OzSnowPosApplication.OZ_SNOW_PREF;


public class NumberKeypadActivity extends AppCompatActivity implements ThermalPrinterService.ConnectionListener
{
    public static final String TAG = NumberKeypadActivity.class.toString();

    private static final String DOT = ".";
    private static final String RESET_VALUE = "$0.00";

    private Button oneBtn;
    private Button twoBtn;
    private Button threeBtn;
    private Button fourBtn;
    private Button fiveBtn;
    private Button sixBtn;
    private Button sevenBtn;
    private Button eightBtn;
    private Button nineBtn;
    private Button zeroBtn;
    private Button pointBtn;
    private Button clearBtn;
    private TextView changDueField;
    private TextView totalDueField;
    private TextView cashTenderedField;
    private BigDecimal orderTotal;
    private Button finishBtn;
    private BigDecimal tendered = new BigDecimal(0.00);
    private BigDecimal change = new BigDecimal(0.00);


    private String dollarString = "";
    private String centString = "00";
    private int index = -1;
    private Boolean hasDecimal = false;

    private boolean isBound;
    private boolean isPrinterConnected;
    private ThermalPrinterService printerService;

    private OrderVo orderVo;
    private CustomerVo customerVo;
    private TourGuideVo guideVo;

    private OrderDataSource orderDataSource;
    private CustomerDataSource customerDataSource;

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder)
        {
            Log.i(TAG, "onServiceConnected() ");
            isBound = true;
            ThermalPrinterService.LocalBinder localBinder = (ThermalPrinterService.LocalBinder) binder;
            printerService = localBinder.getService();
            printerService.setListener(NumberKeypadActivity.this);

        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_number_keypad);

        oneBtn = (Button) findViewById(R.id.one_btn);
        twoBtn = (Button) findViewById(R.id.two_btn);
        threeBtn = (Button) findViewById(R.id.three_btn);
        fourBtn = (Button) findViewById(R.id.four_btn);
        fiveBtn = (Button) findViewById(R.id.five_btn);
        sixBtn = (Button) findViewById(R.id.six_btn);
        sevenBtn = (Button) findViewById(R.id.seven_btn);
        eightBtn = (Button) findViewById(R.id.eight_btn);
        nineBtn = (Button) findViewById(R.id.nine_btn);
        zeroBtn = (Button) findViewById(R.id.zero_btn);
        pointBtn = (Button) findViewById(R.id.point_btn);
        clearBtn = (Button) findViewById(R.id.clear_btn);
        finishBtn = (Button) findViewById(R.id.finish_btn);

        changDueField = (TextView) findViewById(R.id.keypad_change_due);
        cashTenderedField = (TextView) findViewById(R.id.keypad_cash_tendered);
        totalDueField = (TextView) findViewById(R.id.keypad_total_due);

        oneBtn.setOnClickListener(keypadClickListener);
        twoBtn.setOnClickListener(keypadClickListener);
        threeBtn.setOnClickListener(keypadClickListener);
        fourBtn.setOnClickListener(keypadClickListener);
        fiveBtn.setOnClickListener(keypadClickListener);
        sixBtn.setOnClickListener(keypadClickListener);
        sevenBtn.setOnClickListener(keypadClickListener);
        eightBtn.setOnClickListener(keypadClickListener);
        nineBtn.setOnClickListener(keypadClickListener);
        zeroBtn.setOnClickListener(keypadClickListener);
        pointBtn.setOnClickListener(keypadClickListener);

        clearBtn.setOnClickListener(clearClickListener);
        finishBtn.setOnClickListener(finishClickListener);


        Intent intent = getIntent();
        if (intent.hasExtra(OrderDetailsActivity.ORDER_TOTAL_EXTRA))
        {
            orderVo = (OrderVo) intent.getSerializableExtra(OrderDetailsActivity.ORDER_TOTAL_EXTRA);
            orderTotal = orderVo.getOrderTotal();
            totalDueField.setText(NumberFormat.getCurrencyInstance().format(orderVo.getOrderTotal()));
        }

        orderDataSource = new OrderDataSource(NumberKeypadActivity.this);
        customerDataSource = new CustomerDataSource(NumberKeypadActivity.this);

        ReadOrderTask task = new ReadOrderTask(orderDataSource);
        ReadCustomerTask readCustomerTask = new ReadCustomerTask(customerDataSource);
        ReadCurrentTourGuidesTask readCurrentTourGuidesTask = new ReadCurrentTourGuidesTask(new TourGuideDataSource(NumberKeypadActivity.this));

        try {
            orderVo = task.execute(orderVo.getOrderId()).get();
            customerVo = readCustomerTask.execute(orderVo.getCustomerId()).get();
            guideVo = readCurrentTourGuidesTask.execute(orderVo.getGuideId()).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i(TAG, "onStart");
        Intent intent = new Intent(NumberKeypadActivity.this, ThermalPrinterService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (isBound)
        {
            unbindService(connection);
            isBound = false;
        }
    }

    View.OnClickListener keypadClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button btn = (Button) v;
            String keypadLabel = btn.getText().toString();

            if (keypadLabel.equals(DOT)) {
                hasDecimal = true;
                index++;
                return;
            }

            if (hasDecimal == true) {
                if (index == 0) {
                    centString = keypadLabel + "0";
                    index++;
                } else if (index == 1) {
                    centString = centString.charAt(0) + keypadLabel;
                    index++;
                } else {
                    return;
                }
            } else {
                if (dollarString.length() >= 8) {
                    return;
                }

                dollarString = dollarString + keypadLabel;
            }

            String amountTendered = dollarString + DOT + centString;

            tendered = new BigDecimal(amountTendered);

            cashTenderedField.setText("$" + amountTendered);

            calcChange(new BigDecimal(amountTendered));


        }


    };

    private void calcChange(BigDecimal amountTendered)
    {
        change = amountTendered.subtract(orderTotal);

        if (change.doubleValue() > 0.00)
        {
            changDueField.setText(NumberFormat.getCurrencyInstance().format(change));
        }
    }

    View.OnClickListener clearClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            cashTenderedField.setText(RESET_VALUE);
            dollarString = "";
            change = new BigDecimal(0.00);
            tendered = new BigDecimal(0.00);
            centString = "00";
            hasDecimal = false;
            index = -1;
            changDueField.setText(RESET_VALUE);
        }
    };

    View.OnClickListener finishClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (change.doubleValue() < 0.00)
            {
                Toast.makeText(NumberKeypadActivity.this, "Amount tendered is less than the order amount", Toast.LENGTH_SHORT).show();
                return;
            }
            else if (tendered.doubleValue() == 0.00)
            {
                Toast.makeText(NumberKeypadActivity.this, "No cash has been tendered", Toast.LENGTH_SHORT).show();
                return;
            }
            else
            {
                if (isPrinterConnected)
                {
                    printOrder();

                    Intent intent = new Intent(NumberKeypadActivity.this, MainMenuActivity.class);
                    intent.setAction(MainMenuActivity.RESET_ORDER);
                    startActivity(intent);

                }
                else
                {
                    //TODO: enable after testing
                    DeleteCustomerTask deleteCustomerTask =  new DeleteCustomerTask(customerDataSource);
                    deleteCustomerTask.execute(customerVo.getCustomerID());

                    DeleteOrderTask deleteOrderTask = new DeleteOrderTask(orderDataSource);
                    deleteOrderTask.execute(orderVo.getOrderId());

                    Intent intent = new Intent(NumberKeypadActivity.this, MainMenuActivity.class);
                    intent.setAction(MainMenuActivity.RESET_ORDER);
                    startActivity(intent);


                    Toast.makeText(NumberKeypadActivity.this, "Printer is not connected.\n" +
                            "The order has been cancelled\n" +
                            "Please reenter the when the printer is connected", Toast.LENGTH_LONG).show();
//
//                    Toast.makeText(NumberKeypadActivity.this, "Printer is not connected.\n" +
//                            "The order has been recorded\n" +
//                            "******* TESTING BUILD ONLY *******", Toast.LENGTH_LONG).show();

                }
            }
        }
    };


    private void printOrder()
    {
        if (orderVo == null || customerVo == null || guideVo == null)
        {
            return;
        }

        printImage();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        String formatted = sdf.format(new Date(orderVo.getOrderDate()));

        SharedPreferences settings = getSharedPreferences(OZ_SNOW_PREF, MODE_PRIVATE);
        int terminalId = settings.getInt(PreferenceList.TERMINAL_ID.toString(), -1);


        StringBuilder textData = new StringBuilder();
        textData.append("Snowy Valley Resort\n");
        textData.append("Kosciusko Road\n");
        textData.append("Jindabyne East, NSW 2627\n");
        textData.append("tel: (02) 6456 7138\n");
        textData.append("email: info@ozsnow.com\n");
        textData.append("--------------------------------\n");
        textData.append("Order ID: " + terminalId + "010" + orderVo.getOrderId()+"\n");
        textData.append("Date: "+ formatted +"\n");
        textData.append("Guide: "+ guideVo.getGuideName() +"\n");
        textData.append("Customer: " + customerVo.getFirstName() + " " + customerVo.getLastName() + "\n");


        String destination = settings.getString(PreferenceList.DESTINATION.toString(), "thredbo");

        textData.append("Destination: " + destination + "\n");

        textData.append("\n");


        for (int i = 0; i < orderVo.getOrderItems().size(); i++)
        {
            OrderDetailsVo item = orderVo.getOrderItems().get(i);
            textData.append(PrinterFormatUtils.formatItem(item)+"\n");
        }

        textData.append("--------------------------------\n");
        textData.append(PrinterFormatUtils.formatTotal(orderVo.getOrderTotal()));
        textData.append(PrinterFormatUtils.formatAmountTendered(tendered));
        textData.append(PrinterFormatUtils.formatChange(change));

        textData.append("\n");
        textData.append("Equipment Serial Number:\n");
        textData.append("********************************\n");
        textData.append("*                              *\n");
        textData.append("*                              *\n");
        textData.append("*                              *\n");
        textData.append("********************************\n");
        textData.append("\n");
        textData.append("\n");

        Intent intent = new Intent(NumberKeypadActivity.this, ThermalPrinterService.class);
        intent.setAction(PrinterSettingsActivity.PRINT_REQUEST);
        intent.putExtra(PrinterSettingsActivity.PRINT_ORDER_EXTRA, textData.toString());
        startService(intent);



    }

    @Override
    public void onBackPressed()
    {
        orderDataSource.deleteOrderDetails(orderVo.getOrderId());
        super.onBackPressed();
    }

    private void printImage() {
        byte[] sendData = null;
        PrintBitmap pg = new PrintBitmap();
        pg.initCanvas(374);
        pg.initPaint();
        pg.drawImage(0, 0, getResources(), R.drawable.test_2);
        sendData = pg.printDraw();


        Intent intent = new Intent(NumberKeypadActivity.this, ThermalPrinterService.class);
        intent.setAction(PrinterSettingsActivity.PRINT_IMAGE_REQUEST);
        intent.putExtra(PrinterSettingsActivity.PRINT_IMAGE_EXTRA, sendData);
        startService(intent);
    }



    @Override
    public void onConnectToService()
    {
        isPrinterConnected = printerService.isConnected();
    }

    @Override
    public void onConnectionStatus(String action)
    {
        if (ConnectionHandler.DEVICE_CONNECTION_LOST.equals(action))
        {
            //TODO disable print button
            isPrinterConnected = false;
        }
    }

    @Override
    public void onDeviceFound(DeviceVo vo) {

    }

    @Override
    public void onScanningStatus(String action) {

    }
};

