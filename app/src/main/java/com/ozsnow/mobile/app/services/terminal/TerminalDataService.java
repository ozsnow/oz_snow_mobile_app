package com.ozsnow.mobile.app.services.terminal;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.emum.RemoteServiceCommand;
import com.ozsnow.mobile.app.services.ServiceDTO;

/**
 * Created by russellmilburn on 04/03/2016.
 */
public class TerminalDataService extends Service
{

    protected static final String TAG = TerminalDataService.class.toString();

    private TerminalDataHandler handler;

    @Override
    public void onCreate() {
        super.onCreate();

        TerminalDataThread thread = new TerminalDataThread();
        thread.setName("TerminalDataThread");
        thread.start();

        while (thread.getTerminalDataHandler() == null)
        {

        }

        handler = thread.getTerminalDataHandler();
        handler.setService(this);
    }

    public void writeToPreferences(int terminalId)
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(PreferenceList.TERMINAL_ID.toString(), terminalId);
        editor.commit();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent.hasExtra(RemoteServiceCommand.GET_TERMINAL_ID.name()))
        {
            ServiceDTO vo = new ServiceDTO();
            vo.uuid = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            vo.command = (RemoteServiceCommand) intent.getSerializableExtra(RemoteServiceCommand.GET_TERMINAL_ID.name());
            Message message = Message.obtain();
            message.obj = vo;
            message.arg1 = startId;
            handler.sendMessage(message);
        }
        return Service.START_REDELIVER_INTENT;
    }
}
