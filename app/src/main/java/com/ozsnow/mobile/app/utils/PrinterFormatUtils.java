package com.ozsnow.mobile.app.utils;

import android.util.Log;
import com.ozsnow.mobile.app.models.vos.OrderDetailsVo;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * Created by russellmilburn on 07/05/2016.
 */
public class PrinterFormatUtils {
    public static final String TAG = PrinterFormatUtils.class.toString();


    public static String formatTotal(BigDecimal total) {
        String totalString = "TOTAL: ";
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatGap(totalString, total));
        lineItem.append(totalString + NumberFormat.getCurrencyInstance().format(total) + "\n");
        return lineItem.toString();
    }

    public static String formatAmountTendered(BigDecimal tendered) {
        StringBuilder lineItem = new StringBuilder();
        String tenderedString = "AMOUNT TENDERED: ";
        lineItem.append(formatGap(tenderedString, tendered));
        lineItem.append(tenderedString + NumberFormat.getCurrencyInstance().format(tendered) + "\n");
        return lineItem.toString();
    }

    public static String formatChange(BigDecimal change) {
        StringBuilder lineItem = new StringBuilder();
        String changeString = "CHANGE: ";
        lineItem.append(formatGap(changeString, change));
        lineItem.append(changeString + NumberFormat.getCurrencyInstance().format(change) + "\n");
        return lineItem.toString();
    }

    private static String formatGap(String text, BigDecimal number) {
        StringBuilder lineItem = new StringBuilder();
        String numberString = NumberFormat.getCurrencyInstance().format(number);

        int numberOfSpaces = 33 - (text.length() + numberString.length());
        int i = 0;
        do {
            lineItem.append(" ");
            i++;
        }
        while (i < numberOfSpaces - 1);
        return lineItem.toString();
    }


    public static String formatLineItem(String item, BigDecimal price) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(item);
        lineItem.append(formatGap(item, price));
        lineItem.append(NumberFormat.getCurrencyInstance().format(price) + "\n");
        return lineItem.toString();
    }

    public static String formatClothesHire(int noOfDays, String item, BigDecimal price) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatLineItem(noOfDays + " Day " + item, price));
        lineItem.append(" [ ] " + item);
        return lineItem.toString() + "\n";
    }


    public static String formatLiftTicketItem(int noOfDays, BigDecimal price, boolean hasLessons) {
        StringBuilder lineItem = new StringBuilder();
        String days = "";

        if (noOfDays == 5)
        {
            days = "5.5";
        }
        else
        {
            days = String.valueOf(noOfDays);
        }

        lineItem.append(formatLineItem(days + " Day Lift Pass", price));
        lineItem.append(formatLiftTicket(days));

        if (hasLessons == true) {
            lineItem.append(" [ ] Lessons\n");
        }

        return lineItem.toString();
    }

    public static String formatLiftTicket(int noOfDays) {
        return " [ ] " + noOfDays + " Day Lift Pass\n";
    }

    public static String formatLiftTicket(String noOfDays) {
        return " [ ] " + noOfDays + " Day Lift Pass\n";
    }


    public static String formatHireItem(String item, boolean isPerformance) {
        StringBuilder lineItem = new StringBuilder();
        if (isPerformance) {
            lineItem.append(" [ ] " + item + " (PRO)\n");
        }
        else {
            lineItem.append(" [ ] " + item + " (STD)\n");
        }

        return lineItem.toString();
    }

    public static String formatFreeHireItem(String item, boolean isScoopon) {
        StringBuilder lineItem = new StringBuilder();
        if (isScoopon) {
            lineItem.append(" [ ] Scoopon " + item + "\n");
        }
        else {
            lineItem.append(" [ ] f.o.c " + item + "\n");
        }

        return lineItem.toString();
    }

    public static String formatSkiPack(int noOfDays, BigDecimal price, boolean isPerformance, boolean hasLessons) {
        StringBuilder lineItem = new StringBuilder();

        String days = "";

        if (noOfDays == 5)
        {
            days = "5.5";
        }
        else
        {
            days = String.valueOf(noOfDays);
        }

        lineItem.append(formatLineItem(days + " Day Ski Pack", price));

        lineItem.append(formatHireItem("Skis", isPerformance));
        lineItem.append(formatHireItem("Skis Boots", isPerformance));
        lineItem.append(formatHireItem("Poles", isPerformance));
        lineItem.append(formatLiftTicket(days));
        if (hasLessons == true) {
            lineItem.append(" [ ] Lessons\n");
        }
        return lineItem.toString();
    }

    public static String formatSnowboardPack(int noOfDays, BigDecimal price, boolean isPerformance, boolean hasLessons) {
        StringBuilder lineItem = new StringBuilder();
        String days = "";

        if (noOfDays == 5)
        {
            days = "5.5";
        }
        else
        {
            days = String.valueOf(noOfDays);
        }

        lineItem.append(formatLineItem(days + " Day Board Pack", price));
        lineItem.append(formatHireItem("Snowboard", isPerformance));
        lineItem.append(formatHireItem("Snowboard Boots", isPerformance));
        lineItem.append(formatLiftTicket(days));
        if (hasLessons == true) {
            lineItem.append(" [ ] Lessons\n");
        }
        Log.i(TAG, lineItem.toString());
        return lineItem.toString();
    }

    public static String formatClothesPack(int noOfDays, BigDecimal price) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatLineItem(noOfDays + " Day Parka and Pants", price));
        lineItem.append(" [ ] Parka\n");
        lineItem.append(" [ ] Ski Pants\n");
        return lineItem.toString();
    }

    public static String formatAccessoryPack(int noOfDays, BigDecimal price) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatLineItem(noOfDays + " Day Helmet and Guards", price));
        lineItem.append(" [ ] Helmet\n");
        lineItem.append(" [ ] Wrist Guards\n");
        return lineItem.toString();
    }


    public static String formatSkiHire(int noOfDays, BigDecimal price, boolean isPerformance) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatLineItem(noOfDays + " Day Ski Hire", price));
        lineItem.append(formatHireItem("Skis", isPerformance));
        lineItem.append(formatHireItem("Poles", isPerformance));
        lineItem.append(formatHireItem("Ski Boots", isPerformance));

        return lineItem.toString();
    }

    public static String formatFreeSkiHire(int noOfDays, BigDecimal price, boolean isScoopon) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatLineItem(noOfDays + " Day Ski Hire", price));
        lineItem.append(formatFreeHireItem("Skis", isScoopon));
        lineItem.append(formatFreeHireItem("Poles", isScoopon));
        lineItem.append(formatFreeHireItem("Ski Boots", isScoopon));

        return lineItem.toString();
    }


    public static String formatSnowboardHire(int noOfDays, BigDecimal price, boolean isPerformance) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatLineItem(noOfDays + " Day Board Hire", price));
        lineItem.append(formatHireItem("Snowboard", isPerformance));
        lineItem.append(formatHireItem("Snowboard Boots", isPerformance));
        return lineItem.toString();
    }

    public static String formatFreeSnowboardHire(int noOfDays, BigDecimal price, boolean isScoopon) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatLineItem(noOfDays + " Day Board Hire", price));
        lineItem.append(formatFreeHireItem("Snowboard", isScoopon));
        lineItem.append(formatFreeHireItem("Snowboard Boots", isScoopon));
        return lineItem.toString();
    }

    public static String formatSnowmanHire(int noOfDays, BigDecimal price, boolean isPerformance) {
        StringBuilder lineItem = new StringBuilder();
        lineItem.append(formatLineItem(noOfDays + " Day Snowman Hire", price));
        lineItem.append(formatHireItem("Toboggan", isPerformance));
        lineItem.append(formatHireItem("Jacket", isPerformance));
        lineItem.append(formatHireItem("Pants", isPerformance));
        lineItem.append(formatHireItem("Boots", isPerformance));
        return lineItem.toString();
    }


    public static String formatItem(OrderDetailsVo orderItem) {
        boolean isEquipment = false;
        boolean isPack = false;
        boolean isLiftPass = false;
        boolean isClothes = false;
        boolean isPerformance = orderItem.getProductName().toLowerCase().matches(".*performance.*");
        boolean isSkis = orderItem.getProductName().toLowerCase().matches(".*ski.*");
        boolean hasLessons = orderItem.getProductName().toLowerCase().matches(".*lessons.*");
        boolean isSnowman = orderItem.getProductName().toLowerCase().matches(".*snowman.*");
        boolean isScoopon = orderItem.getProductName().toLowerCase().matches(".*scoopon.*");

        int noOfDays = 0;

        try {
            noOfDays = Integer.parseInt(orderItem.getProductDesc().charAt(0) + "");
        } catch (NumberFormatException nfe) {
            StringBuilder lineItem = new StringBuilder();
            lineItem.append(formatLineItem("1 x Gloves", orderItem.getPrice()));
            lineItem.append(formatHireItem("Gloves", isPerformance));
            return lineItem.toString();
        }


        // Check what category
        if (orderItem.getCategoryId() == 1)
        {
            isEquipment = true;
        }
        else if (orderItem.getCategoryId() == 2)
        {
            isPack = true;
        }
        else if (orderItem.getCategoryId() == 3)
        {
            isLiftPass = true;
        }
        else if (orderItem.getCategoryId() == 4)
        {
            isClothes = true;
        }

        if (isLiftPass)
        {
            return formatLiftTicketItem(noOfDays, orderItem.getPrice(), hasLessons);
        }

        if (isEquipment)
        {
            if (isSkis)
            {
                Log.i(TAG, "isSkis :" + isSkis);
                if (orderItem.getProductId() == 35)
                {
                    return formatClothesHire(noOfDays, orderItem.getProductName(), orderItem.getPrice());
                }
                else
                {
                    if (orderItem.getProductId() == 41 || orderItem.getProductId() == 42)
                    {
                        return formatFreeSkiHire(noOfDays, orderItem.getPrice(), isScoopon);
                    }

                    return formatSkiHire(noOfDays, orderItem.getPrice(), isPerformance);
                }
            }
            else
            {
                if (orderItem.getProductId() == 2 || orderItem.getProductId() == 43 || orderItem.getProductId() == 45)
                {
                    if (orderItem.getProductId() == 43 || orderItem.getProductId() == 45)
                    {
                        return formatFreeSnowboardHire(noOfDays, orderItem.getPrice(), isScoopon);
                    }

                    return formatSnowboardHire(noOfDays, orderItem.getPrice(), isPerformance);
                }
                else if (orderItem.getProductId() == 34)
                {
                    return formatAccessoryPack(noOfDays, orderItem.getPrice());
                }
                else
                {
                    return formatClothesHire(noOfDays, orderItem.getProductName(), orderItem.getPrice());
                }
            }

        }

        if (isPack)

        {
            Log.i(TAG, "isPack :" + isPack);
            if (isSkis)
            {
                Log.i(TAG, "isSkis :" + isSkis);
                return formatSkiPack(noOfDays, orderItem.getPrice(), isPerformance, hasLessons);
            }
            else
            {
                if (isSnowman)
                {
                    return formatSnowmanHire(noOfDays, orderItem.getPrice(), isPerformance);
                }

                Log.i(TAG, "isSkis :" + isSkis);
                return formatSnowboardPack(noOfDays, orderItem.getPrice(), isPerformance, hasLessons);
            }
        }

        if (isClothes)
        {
            if (orderItem.getProductId() == 33  )
            {
                return formatClothesPack(noOfDays, orderItem.getPrice());
            }
            return formatClothesHire(noOfDays, orderItem.getProductName(), orderItem.getPrice());
        }

        return null;
    }

}
