package com.ozsnow.mobile.app.services.sync;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

/**
 * Created by russellmilburn on 15/05/2016.
 */
public class SyncDataHandler extends Handler
{
    protected static final String TAG = SyncDataHandler.class.toString();

    private SyncDataService service;

    @Override
    public void handleMessage(Message msg)
    {
        super.handleMessage(msg);

        Log.i(TAG, "handleMessage");

        getService().startSync();

    }

    public SyncDataService getService() {
        return service;
    }

    public void setService(SyncDataService service) {
        this.service = service;
    }
}
