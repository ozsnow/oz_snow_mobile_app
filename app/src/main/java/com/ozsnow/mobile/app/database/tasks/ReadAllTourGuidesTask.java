package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

import java.util.ArrayList;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class ReadAllTourGuidesTask extends AsyncTask<Void, Void, ArrayList<TourGuideVo>>
{
    private TourGuideDataSource dataSource;

    public ReadAllTourGuidesTask(TourGuideDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected ArrayList<TourGuideVo> doInBackground(Void... params)
    {
        return dataSource.readAllTourGuides();
    }

}
