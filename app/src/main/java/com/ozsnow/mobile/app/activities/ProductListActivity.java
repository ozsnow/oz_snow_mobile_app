package com.ozsnow.mobile.app.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.adapters.ProductListAdapter;
import com.ozsnow.mobile.app.activities.fragments.OrderMenuFragment;
import com.ozsnow.mobile.app.database.ProductDataSource;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.ProductVo;

import java.util.ArrayList;
import java.util.List;

import static com.ozsnow.mobile.app.activities.adapters.ProductListAdapter.*;

public class ProductListActivity extends AppCompatActivity {

    public static final String TAG = ProductListActivity.class.toString();
    public static final String SELECTED_PRODUCT_EXTRA = "selectedProductExtra";

    public static final int SELECT_PRODUCT_REQUEST = 1;

    private RecyclerView recyclerView;
    private ProductListAdapter productListAdapter;
    private Button filterABtn;
    private Button filterBBtn;
    private Button filterCBtn;
    private List<ProductVo> fullProductList;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        boolean isPeak = false;

        recyclerView = (RecyclerView) findViewById(R.id.productListRecyclerView);
        filterABtn = (Button) findViewById(R.id.product_filter_btn_A);
        filterBBtn = (Button) findViewById(R.id.product_filter_btn_B);
        filterCBtn = (Button) findViewById(R.id.product_filter_btn_C);

        filterABtn.setOnClickListener(filterAClickListener);
        filterBBtn.setOnClickListener(filterBClickListener);
        filterCBtn.setOnClickListener(filterCClickListener);

        ProductDataSource dataSource = new ProductDataSource(this);
        Intent intent = getIntent();

        int categoryId = -1;
        String mountain = "";


        if(intent.hasExtra(OrderMenuFragment.CATEGORY_ID_EXTRA))
        {
            categoryId = intent.getIntExtra(OrderMenuFragment.CATEGORY_ID_EXTRA, -1);
        }

        if (intent.hasExtra(OrderMenuFragment.MOUNTAIN_EXTRA))
        {
            mountain = intent.getStringExtra(OrderMenuFragment.MOUNTAIN_EXTRA);

            SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);

            if (mountain.equals(getResources().getString(R.string.rb_thredbo_text)))
            {
                isPeak = settings.getBoolean(PreferenceList.IS_THREDBO_PEAK.toString(), false);
            }
            else if (mountain.equals(getResources().getString(R.string.rb_perisher_text)))
            {
                isPeak = settings.getBoolean(PreferenceList.IS_PERISHER_PEAK.toString(), false);
            }
        }

        //TODO: Make product requests async task

        switch (categoryId)
        {
            case 1:
                fullProductList = dataSource.getEquipment(categoryId);
                break;
            case 2:
                fullProductList = dataSource.getPackPriceList(categoryId, mountain, isPeak);
                break;
            case 3:
                fullProductList = dataSource.getLiftPasses(categoryId, mountain, isPeak);
                break;
            case 4:
                fullProductList = dataSource.getClothes(categoryId);
                break;
        }

        //There are a lot of space so that the destination will be right aligned
        setTitle("Select a product:                                   Destination: " + mountain);

        productListAdapter = new ProductListAdapter(ProductListActivity.this, fullProductList);
        recyclerView.setAdapter(productListAdapter);
        filter("1-2");

        filterABtn.setEnabled(false);
    }

    View.OnClickListener filterAClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            filter("1-2");
            filterABtn.setEnabled(false);
            filterBBtn.setEnabled(true);
            filterCBtn.setEnabled(true);
        }
    };

    View.OnClickListener filterBClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            filter("3-4");
            filterABtn.setEnabled(true);
            filterBBtn.setEnabled(false);
            filterCBtn.setEnabled(true);
        }
    };

    View.OnClickListener filterCClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            filter("5-6");
            filterABtn.setEnabled(true);
            filterBBtn.setEnabled(true);
            filterCBtn.setEnabled(false);
        }
    };


    private void filter(String filterValue)
    {

        ProductListAdapter.ProductFilter filter = new ProductListAdapter.ProductFilter(productListAdapter, fullProductList);
        productListAdapter.setFilter(filter);
        productListAdapter.getFilter().filter(filterValue);

    }

    public void productSelected(ProductVo product)
    {
        Log.i(TAG,  product.toString());
        Intent intent = new Intent();

        intent.putExtra(SELECTED_PRODUCT_EXTRA, product);
        setResult(RESULT_OK, intent);
        finish();
    }
}
