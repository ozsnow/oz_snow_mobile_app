package com.ozsnow.mobile.app.services;

import com.ozsnow.mobile.app.models.emum.RemoteServiceCommand;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

/**
 * Created by russellmilburn on 06/03/2016.
 */
public class ServiceDTO
{
    public RemoteServiceCommand command;
    public String uuid;
    public TourGuideVo tourGuideVo;

}
