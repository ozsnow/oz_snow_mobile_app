package com.ozsnow.mobile.app.models.vos;

import android.provider.BaseColumns;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by russellmilburn on 28/04/2016.
 */
public class OrderVo implements Serializable
{
    public static final class OrderFields implements BaseColumns
    {
        public static final String ORDER_TBL = "OrderTbl";

        public static final String ORDER_ID = "orderId";
        public static final String CUSTOMER_ID = "customerId";
        public static final String IS_VOID_ORDER = "isVoidOrder";
        public static final String TRIP_DURATION = "tripDuration";
        public static final String HAS_SYNCED = "hasSynced";
        public static final String ORDER_TOTAL = "orderTotal";
        public static final String ORDER_DATE = "orderDate";
    }

    private int orderId;
    private int customerId;
    private int guideId;
    private boolean isVoidOrder;
    private int tripDuration;
    private boolean hasSynced;
    private BigDecimal orderTotal;
    private long orderDate;

    private ArrayList<OrderDetailsVo> orderItems;
    private ArrayList<ProductVo> selectedProducts;


    public OrderVo()
    {

    }

    public long getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(long orderDate) {
        this.orderDate = orderDate;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getGuideId() {
        return guideId;
    }

    public void setGuideId(int guideId) {
        this.guideId = guideId;
    }

    public boolean isVoidOrder() {
        return isVoidOrder;
    }

    public void setVoidOrder(boolean voidOrder) {
        isVoidOrder = voidOrder;
    }

    public int getTripDuration() {
        return tripDuration;
    }

    public void setTripDuration(int tripDuration) {
        this.tripDuration = tripDuration;
    }

    public boolean isHasSynced() {
        return hasSynced;
    }

    public void setHasSynced(boolean hasSynced) {
        this.hasSynced = hasSynced;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public ArrayList<OrderDetailsVo> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderDetailsVo> orderItems) {
        this.orderItems = orderItems;
    }

    public ArrayList<ProductVo> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(ArrayList<ProductVo> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    @Override
    public String toString()
    {
        return "[OrderVo id: " + this.orderId +
                ", customerId: " + customerId +
                ", orderDate: " + orderDate +
                ", orderTotal: " + orderTotal +
                "]";
    }
}
