package com.ozsnow.mobile.app.activities.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.PrinterSettingsActivity;
import com.ozsnow.mobile.app.models.vos.DeviceVo;
import com.ozsnow.mobile.app.models.vos.ProductVo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by russellmilburn on 01/05/2016.
 */
public class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.DeviceViewHolder>
{
    public static final String TAG = DeviceListAdapter.class.toString();

    protected Context context;
    protected List<DeviceVo> devices;

    public DeviceListAdapter(Context context, List<DeviceVo> list)
    {
        this.context = context;
        devices = list;
    }

    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.device_list_item, viewGroup, false);
        return new DeviceViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DeviceViewHolder holder, int position)
    {
        holder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    public List<DeviceVo> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceVo> devices) {
        this.devices = devices;
    }

    public void add(DeviceVo vo)
    {
        devices.add(vo);
        int itemIndex = devices.indexOf(vo);
        notifyItemInserted(itemIndex);
        notifyDataSetChanged();
    }

    public void removeAll()
    {
        devices = new ArrayList<>();
        notifyDataSetChanged();
    }

    public void remove(DeviceVo vo)
    {
        devices.add(vo);
        int itemIndex = devices.indexOf(vo);
        notifyItemInserted(itemIndex);
        notifyDataSetChanged();
    }

    public class DeviceViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private int position;
        private TextView deviceName;
        private TextView macAddress;

        public DeviceViewHolder(View itemView) {
            super(itemView);

            deviceName = (TextView) itemView.findViewById(R.id.device_name_field);
            macAddress = (TextView) itemView.findViewById(R.id.mac_address_field);
            itemView.setOnClickListener(this);
        }

        public void bindView(int position)
        {
            this.position = position;
            DeviceVo vo = devices.get(position);
            deviceName.setText(vo.getDeviceName());
            macAddress.setText(vo.getMacAddress());

        }

        @Override
        public void onClick(View v) {
            Log.i(TAG, "onClick");
            PrinterSettingsActivity activity = (PrinterSettingsActivity) context;
            activity.connectToDevice(devices.get(position));
        }
    }
}
