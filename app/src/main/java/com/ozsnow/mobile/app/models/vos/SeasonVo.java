package com.ozsnow.mobile.app.models.vos;

import android.provider.BaseColumns;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by russellmilburn on 17/05/2016.
 */
public class SeasonVo implements Serializable
{
    public static final class SeasonFields implements BaseColumns
    {
        public static final String SEASON_ID = "seasonId";
        public static final String YEAR = "year";
        public static final String PEAK_START = "peakStart";
        public static final String PEAK_END = "peakEnd";
        public static final String DESTINATION = "destination";
    }


    private String destination;
    private long peakStart;
    private long peakEnd;
    private int id;
    private int year;

    public SeasonVo()
    {

    }

    public SeasonVo(JSONObject vo)
    {
        try
        {
            this.id = vo.getInt(SeasonFields.SEASON_ID);
            this.year = vo.getInt(SeasonFields.YEAR);
            this.peakStart = vo.getLong(SeasonFields.PEAK_START);
            this.peakEnd = vo.getLong(SeasonFields.PEAK_END);
            this.destination = vo.getString(SeasonFields.DESTINATION);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public long getPeakStart() {
        return peakStart;
    }

    public void setPeakStart(long peakStart) {
        this.peakStart = peakStart;
    }

    public long getPeakEnd() {
        return peakEnd;
    }

    public void setPeakEnd(long peakEnd) {
        this.peakEnd = peakEnd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
