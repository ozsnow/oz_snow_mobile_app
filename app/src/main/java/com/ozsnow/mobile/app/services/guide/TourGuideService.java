package com.ozsnow.mobile.app.services.guide;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import com.ozsnow.mobile.app.models.emum.LocalServiceCommand;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;
import com.ozsnow.mobile.app.services.ServiceDTO;
import com.ozsnow.mobile.app.services.terminal.TerminalDataThread;

/**
 * Created by russellmilburn on 06/04/2016.
 */
public class TourGuideService extends Service
{
    protected static final String TAG = TourGuideService.class.toString();

    private TourGuideHandler handler;


    @Override
    public void onCreate()
    {
        super.onCreate();

        TourGuideThread thread = new TourGuideThread();
        thread.setName("TourGuideThread");
        thread.start();

        while (thread.getTourGuideHandler() == null)
        {

        }

        handler = thread.getTourGuideHandler();
        handler.setService(this);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if (intent.hasExtra(LocalServiceCommand.CREATE_TOUR_GUIDE.name()))
        {
            ServiceDTO dto = new ServiceDTO();
            dto.tourGuideVo = (TourGuideVo) intent.getSerializableExtra(LocalServiceCommand.CREATE_TOUR_GUIDE.name());
            Message message = Message.obtain();
            message.obj = dto;
            message.arg1 = startId;
            handler.sendMessage(message);
        }
        return Service.START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
