package com.ozsnow.mobile.app.models.emum;

/**
 * Created by russellmilburn on 06/03/2016.
 */
public enum PreferenceList
{
    TERMINAL_ID,
    LAST_PRODUCT_UPDATE,
    CURRENT_CUSTOMER,
    DESTINATION,
    CURRENT_TOUR_GUIDE,
    CURRENT_ORDER_ID,
    THREDBO_SEASON_DATES,
    PERISHER_SEASON_DATES,
    IS_THREDBO_PEAK,
    IS_PERISHER_PEAK
    ;
}
