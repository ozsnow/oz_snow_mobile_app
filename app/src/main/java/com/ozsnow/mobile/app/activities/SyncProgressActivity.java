package com.ozsnow.mobile.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.ozsnow.mobile.app.MainActivity;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.services.sync.SyncDataService;
import com.ozsnow.mobile.app.services.sync.SyncServiceListener;

public class SyncProgressActivity extends AppCompatActivity implements SyncServiceListener
{
    public static final String TAG = SyncProgressActivity.class.toString();

    private static final String SYNC_PIN = "6738";

    private boolean bound = false;
    private ProgressDialog progressDialog;
    private SyncDataService service;
    private EditText pinField;
    private Button continueBtn;
    private Button syncOrders;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync_progress);

        setTitle("Sync orders with cloud:");

        pinField = (EditText) findViewById(R.id.sync_pin_field);

        continueBtn = (Button) findViewById(R.id.continueBtn);
        syncOrders = (Button) findViewById(R.id.sync_orders_btn);

        continueBtn.setOnClickListener(continueBtnListener);
        syncOrders.setOnClickListener(syncBtnListener);


        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Waiting To Sync");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgress(0);
        //progressDialog.show();

    }

    View.OnClickListener continueBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            String pin = pinField.getText().toString();
            if (pin.contentEquals(SYNC_PIN)) {
                syncOrders.setVisibility(View.VISIBLE);
            }
            else
            {
                Toast.makeText(SyncProgressActivity.this, "Incorrect pin code", Toast.LENGTH_SHORT).show();
            }
        }
    };

    View.OnClickListener syncBtnListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            syncOrders.setVisibility(View.INVISIBLE);
            progressDialog.show();

            Intent intent = new Intent(SyncProgressActivity.this, SyncDataService.class);
            startService(intent);


        }
    };



    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(SyncProgressActivity.this, SyncDataService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder)
        {
            Log.i(TAG, "onServiceConnected");
            bound = true;
            SyncDataService.LocalBinder localBinder = (SyncDataService.LocalBinder) binder;
            service = localBinder.getService();
            service.setListener(SyncProgressActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            bound = false;
            service.setListener(null);
            Log.i(TAG, "onServiceDisconnected");
        }
    };

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    @Override
    public void onChangeState(final  int state, final int total)
    {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (SyncDataService.GUIDE_STATE == state)
                {
                    progressDialog.setProgress(0);
                    progressDialog.setMessage("Syncing Tour Guides");
                    progressDialog.setMax(total);
                }
                else if (state == SyncDataService.CUSTOMER_STATE)
                {
                    progressDialog.setProgress(0);
                    progressDialog.setMessage("Syncing Customers");
                    progressDialog.setMax(total);
                }
                else if (state == SyncDataService.ORDER_STATE)
                {
                    progressDialog.setProgress(0);
                    progressDialog.setMessage("Syncing Orders");
                    progressDialog.setMax(total);
                }
            }
        });
    }

    @Override
    public void onProgress(final  int progress) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.setProgress(progress);
            }
        });
    }

    @Override
    public void onComplete()
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressDialog.setMessage("Sync Complete Successfully");
                progressDialog.setProgress(100);
                progressDialog.setMax(100);
            }
        });

        Intent intent = new Intent(SyncProgressActivity.this, MainMenuActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onStop() {

        Log.i(TAG, "onStop Here");
        if (bound)
        {
            Log.i(TAG, "onStop bound true");
            unbindService(connection);
            bound = false;
        }

        super.onStop();
    }
}
