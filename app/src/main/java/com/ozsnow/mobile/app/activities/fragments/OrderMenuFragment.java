package com.ozsnow.mobile.app.activities.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.NumberKeypadActivity;
import com.ozsnow.mobile.app.activities.OrderDetailsActivity;
import com.ozsnow.mobile.app.activities.ProductListActivity;
import com.ozsnow.mobile.app.activities.adapters.ProductListAdapter;
import com.ozsnow.mobile.app.models.emum.PreferenceList;

/**
 * Created by russellmilburn on 11/04/2016.
 */
public class OrderMenuFragment extends Fragment
{
    protected static final String TAG = OrderMenuFragment.class.toString();

    public static final String CATEGORY_ID_EXTRA = "categoryIdExtra";
    public static final String MOUNTAIN_EXTRA = "mountainExtra";
    public static final String TRIP_DURATION_EXTRA = "tripDurationExtra";



    private Button equipmentBtn;
    private Button liftPassesBtn;
    private Button packsBtn;
    private Button completeOrderBtn;
    private Button cancelOrderBtn;
    private Button clothesBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_order_menu, container, false);
        liftPassesBtn = (Button)view.findViewById(R.id.liftPassBtn);
        equipmentBtn = (Button)view.findViewById(R.id.equipmentBtn);
        packsBtn = (Button)view.findViewById(R.id.packsBtn);
        completeOrderBtn = (Button)view.findViewById(R.id.completeOrderBtn);
        cancelOrderBtn = (Button)view.findViewById(R.id.cancelOrderBtn);
        clothesBtn = (Button)view.findViewById(R.id.clothesBtn);

        equipmentBtn.setOnClickListener(addEquipmentListener);

        packsBtn.setOnClickListener(addPackListener);

        liftPassesBtn.setOnClickListener(addLiftPassListener);

        completeOrderBtn.setOnClickListener(completeOrderListener);

        cancelOrderBtn.setOnClickListener(cancelOrderListener);
        clothesBtn.setOnClickListener(clothesClickListener);

        return view;
    }

    View.OnClickListener addEquipmentListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String destination = getDestination();
            Intent intent = new Intent(getActivity(), ProductListActivity.class);
            intent.putExtra(CATEGORY_ID_EXTRA, 1);
            intent.putExtra(MOUNTAIN_EXTRA, destination);
            Log.i(TAG, ProductListActivity.SELECT_PRODUCT_REQUEST + "");

            getActivity().startActivityForResult(intent, ProductListActivity.SELECT_PRODUCT_REQUEST);
        }
    };

    View.OnClickListener addPackListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            String destination = getDestination();
            Intent intent = new Intent(getActivity(), ProductListActivity.class);
            intent.putExtra(CATEGORY_ID_EXTRA, 2);
            intent.putExtra(MOUNTAIN_EXTRA, destination);
            getActivity().startActivityForResult(intent, ProductListActivity.SELECT_PRODUCT_REQUEST);

        }
    };

    View.OnClickListener addLiftPassListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            String destination = getDestination();
            Intent intent = new Intent(getActivity(), ProductListActivity.class);
            intent.putExtra(CATEGORY_ID_EXTRA, 3);
            intent.putExtra(MOUNTAIN_EXTRA, destination);
            getActivity().startActivityForResult(intent, ProductListActivity.SELECT_PRODUCT_REQUEST);

        }
    };

    View.OnClickListener clothesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(getActivity(), ProductListActivity.class);
            intent.putExtra(CATEGORY_ID_EXTRA, 4);
            getActivity().startActivityForResult(intent, ProductListActivity.SELECT_PRODUCT_REQUEST);

        }
    };

    View.OnClickListener completeOrderListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ((OrderDetailsActivity)getActivity()).saveOrderToDatabase();

        }
    };

    View.OnClickListener cancelOrderListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            ((OrderDetailsActivity)getActivity()).cancelOrder();

        }
    };

    private String getDestination()
    {
        SharedPreferences settings = getActivity().getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, Activity.MODE_PRIVATE);
        return settings.getString(PreferenceList.DESTINATION.toString(), "thredbo");
    }


}