package com.ozsnow.mobile.app.models.vos;

import android.provider.BaseColumns;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by russellmilburn on 06/04/2016.
 */
public class TourGuideVo implements Serializable
{
    public static final class TourGuideFields implements BaseColumns
    {
        public static final String TOUR_GUIDE_TBL = "TourGuideTbl";

        public static final String GUIDE_ID = "guideId";
        public static final String GUIDE_NAME = "firstName";
        public static final String DATE = "date";
        public static final String HAS_SYNCED = "hasSynced";
    }

    private int guideId = -1;
    private String guideName;
    private long date;
    private boolean hasSynced;

    public int getGuideId() {
        return guideId;
    }

    public void setGuideId(int guideId) {
        this.guideId = guideId;
    }

    public String getGuideName() {
        return guideName;
    }

    public void setGuideName(String guideName) {
        this.guideName = guideName;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public boolean hasSynced() {
        return hasSynced;
    }

    public void setHasSynced(boolean hasSynced) {
        this.hasSynced = hasSynced;
    }

    @Override
    public String toString()
    {
        return "[TourGuideVo id: " + this.guideId +
                ", guideName: " + guideName +
                ", date: " + new Date(date).toString() +
                ", hasSynced: " + hasSynced +
                "]";
    }
}
