package com.ozsnow.mobile.app.models.vos;

import android.provider.BaseColumns;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by russellmilburn on 15/03/2016.
 */
public class ProductTypeVo implements Serializable
{
    public static final class ProductTypeFields implements BaseColumns
    {
        public static final String PRODUCT_TYPE_TBL = "ProductTypeTbl";

        public static final String PRODUCT_TYPE_ID = "productTypeId";
        public static final String PRICE_1D = "price1D";
        public static final String PRICE_2D = "price2D";
        public static final String PRICE_3D = "price3D";
        public static final String PRICE_4D = "price4D";
        public static final String PRICE_5D = "price5D";
        public static final String PRICE_6D = "price6D";
        public static final String PRICE_7D = "price7D";
        public static final String PRICE_8D = "price8D";
        public static final String PRICE_9D = "price9D";
        public static final String PRICE_10D = "price10D";
        public static final String PRICE_11D = "price11D";
        public static final String PRICE_12D = "price12D";
        public static final String PRICE_13D = "price13D";
        public static final String PRICE_14D = "price14D";
        public static final String RES_CODE = "resCode";
        public static final String IS_HIRE_PRODUCT = "isHireProduct";
        public static final String PRODUCT_TYPE = "productType";
    }

    public ProductTypeVo()
    {

    }

    public ProductTypeVo(JSONObject vo)
    {
        try {
            this.productTypeID = vo.getInt(ProductTypeFields.PRODUCT_TYPE_ID);
            this.isHireProduct = (vo.getInt(ProductTypeFields.IS_HIRE_PRODUCT) != 0);
            this.resCode = vo.getString(ProductTypeFields.RES_CODE);
            this.productType = vo.getString(ProductTypeFields.PRODUCT_TYPE);
            this.price1D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_1D));
            this.price2D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_2D));
            this.price3D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_3D));
            this.price4D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_4D));
            this.price5D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_5D));
            this.price6D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_6D));
            this.price7D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_7D));
            this.price8D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_8D));
            this.price9D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_9D));
            this.price10D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_10D));
            this.price11D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_11D));
            this.price12D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_12D));
            this.price13D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_13D));
            this.price14D =  BigDecimal.valueOf(vo.getDouble(ProductTypeFields.PRICE_14D));
            this.productID = vo.getInt(ProductVo.ProductFields.PRODUCT_ID);
            priceArray = new BigDecimal[]{price1D, price2D, price3D, price4D, price5D, price6D,
                    price7D, price8D, price9D, price10D, price11D, price12D, price13D, price14D};
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }


    private int productTypeID;
    private BigDecimal price1D;
    private BigDecimal price2D;
    private BigDecimal price3D;
    private BigDecimal price4D;
    private BigDecimal price5D;
    private BigDecimal price6D;
    private BigDecimal price7D;
    private BigDecimal price8D;
    private BigDecimal price9D;
    private BigDecimal price10D;
    private BigDecimal price11D;
    private BigDecimal price12D;
    private BigDecimal price13D;
    private BigDecimal price14D;
    private String resCode = "";
    private boolean isHireProduct;
    private String productType;
    private int productID;
    private BigDecimal[] priceArray;

    public int getProductTypeID() {
        return productTypeID;
    }

    public void setProductTypeID(int productTypeID) {
        this.productTypeID = productTypeID;
    }

    public BigDecimal getPrice1D() {
        return price1D;
    }

    public void setPrice1D(BigDecimal price1D) {
        this.price1D = price1D;
    }

    public BigDecimal getPrice2D() {
        return price2D;
    }

    public void setPrice2D(BigDecimal price2D) {
        this.price2D = price2D;
    }

    public BigDecimal getPrice3D() {
        return price3D;
    }

    public void setPrice3D(BigDecimal price3D) {
        this.price3D = price3D;
    }

    public BigDecimal getPrice4D() {
        return price4D;
    }

    public void setPrice4D(BigDecimal price4D) {
        this.price4D = price4D;
    }

    public BigDecimal getPrice5D() {
        return price5D;
    }

    public void setPrice5D(BigDecimal price5D) {
        this.price5D = price5D;
    }

    public BigDecimal getPrice6D() {
        return price6D;
    }

    public void setPrice6D(BigDecimal price6D) {
        this.price6D = price6D;
    }

    public BigDecimal getPrice7D() {
        return price7D;
    }

    public void setPrice7D(BigDecimal price7D) {
        this.price7D = price7D;
    }

    public BigDecimal getPrice8D() {
        return price8D;
    }

    public void setPrice8D(BigDecimal price8D) {
        this.price8D = price8D;
    }

    public BigDecimal getPrice9D() {
        return price9D;
    }

    public void setPrice9D(BigDecimal price9D) {
        this.price9D = price9D;
    }

    public BigDecimal getPrice10D() {
        return price10D;
    }

    public void setPrice10D(BigDecimal price10D) {
        this.price10D = price10D;
    }

    public BigDecimal getPrice11D() {
        return price11D;
    }

    public void setPrice11D(BigDecimal price11D) {
        this.price11D = price11D;
    }

    public BigDecimal getPrice12D() {
        return price12D;
    }

    public void setPrice12D(BigDecimal price12D) {
        this.price12D = price12D;
    }

    public BigDecimal getPrice13D() {
        return price13D;
    }

    public void setPrice13D(BigDecimal price13D) {
        this.price13D = price13D;
    }

    public BigDecimal getPrice14D() {
        return price14D;
    }

    public void setPrice14D(BigDecimal price14D) {
        this.price14D = price14D;
    }

    public String getResCode() {
        return resCode;
    }

    public void setResCode(String resCode) {
        this.resCode = resCode;
    }

    public boolean isHireProduct() {
        return isHireProduct;
    }

    public void setHireProduct(boolean hireProduct) {
        isHireProduct = hireProduct;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public BigDecimal[] getPriceArray()
    {
        return new BigDecimal[]{price1D, price2D, price3D, price4D, price5D, price6D,
                price7D, price8D, price9D, price10D, price11D, price12D, price13D, price14D};
    }

    @Override
    public String toString()
    {
        return "[ProductTypeVo id: " + this.productTypeID +
                ", productId: " + this.productID +
                ", resCode: " + this.resCode +
                "]";
    }
}
