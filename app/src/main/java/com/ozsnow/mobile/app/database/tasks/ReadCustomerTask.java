package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.models.vos.CustomerVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class ReadCustomerTask extends AsyncTask<Integer, Void, CustomerVo>
{
    private CustomerDataSource dataSource;

    public ReadCustomerTask(CustomerDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected CustomerVo doInBackground(Integer... ids)
    {
        return dataSource.readCustomer(ids[0]);
    }

}
