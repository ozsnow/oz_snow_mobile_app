package com.ozsnow.mobile.app.activities;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.adapters.VoidOrderListAdapter;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.database.tasks.UpdateOrderTask;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import com.ozsnow.mobile.app.models.vos.OrderVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;
import com.ozsnow.mobile.app.models.vos.VoidOrderListVo;

import java.util.ArrayList;

import static com.ozsnow.mobile.app.OzSnowPosApplication.OZ_SNOW_PREF;

public class VoidOrderActivity extends AppCompatActivity
{
    private VoidOrderListAdapter adapter;
    private CustomerDataSource customerDataSource;
    private TourGuideDataSource guideDataSource;
    private OrderDataSource orderDataSource;
    private ArrayList<VoidOrderListVo> data;
    private RecyclerView recyclerView;
    private int terminalId;
    private VoidOrderListVo listVo;
    private AlertDialog.Builder builder;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_void_order);

        data = new ArrayList<>();
        adapter = new VoidOrderListAdapter(this, data);

        recyclerView = (RecyclerView)findViewById(R.id.orderListRecyclerView);

        recyclerView.setAdapter(adapter);

        SharedPreferences settings = getSharedPreferences(OZ_SNOW_PREF, MODE_PRIVATE);
        terminalId = settings.getInt(PreferenceList.TERMINAL_ID.toString(), -1);

        listVoidOrders();

    }


    private void listVoidOrders()
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run()
            {

                customerDataSource = new CustomerDataSource(VoidOrderActivity.this);
                guideDataSource = new TourGuideDataSource(VoidOrderActivity.this);
                orderDataSource = new OrderDataSource(VoidOrderActivity.this);

                ArrayList<OrderVo> orderVos = orderDataSource.readLiveOrders();

                for (int i = 0; i < orderVos.size(); i++)
                {
                    CustomerVo customer = customerDataSource.readCustomer(orderVos.get(i).getCustomerId());
                    TourGuideVo guide = guideDataSource.readTourGuide(orderVos.get(i).getGuideId());
                    VoidOrderListVo vo = new VoidOrderListVo();
                    vo.setCustomerVo(customer);
                    vo.setOrderVo(orderVos.get(i));
                    vo.setGuideVo(guide);
                    vo.setTerminalId(terminalId);

                    data.add(vo);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });

            }
        });
        thread.setName("Get Void Orders");
        thread.start();
    }



    public void voidOrder(int position)
    {
        this.position = position;
        listVo = adapter.getList().get(position);
        builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        builder.setTitle("Void Order");
        builder.setMessage("Are you sure you want to void the order");
        builder.setPositiveButton("OK", voidOrderListener);
        builder.setNegativeButton("Cancel", voidOrderListener);
        builder.show();
    }

    public void unvoidOrder(VoidOrderListVo vo)
    {
        listVo = vo;
        builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        builder.setTitle("Un-void Order");
        builder.setMessage("This transaction has been voided, are you sure you want to change it");
        builder.setPositiveButton("OK", voidOrderListener);
        builder.setNegativeButton("Cancel", voidOrderListener);
        builder.show();
    }

    DialogInterface.OnClickListener voidOrderListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    OrderVo order = listVo.getOrderVo();
                    order.setVoidOrder(true);
                    adapter.getList().get(position).getOrderVo().setVoidOrder(true);
                    updateOrder(order);
                    dialog.dismiss();
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    dialog.dismiss();
                    break;
            }
            adapter.notifyDataSetChanged();
        }
    };


    private void updateOrder(OrderVo vo)
    {
        OrderVo order = vo;
        UpdateOrderTask task = new UpdateOrderTask(orderDataSource);
        task.execute(order);
        listVo = null;
    }

}
