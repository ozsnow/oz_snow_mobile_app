package com.ozsnow.mobile.app.services.guide;

import android.os.Handler;
import android.os.Message;
import com.loopj.android.http.AsyncHttpClient;
import com.ozsnow.mobile.app.database.ProductDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.services.ServiceDTO;

/**
 * Created by russellmilburn on 06/04/2016.
 */
public class TourGuideHandler extends Handler
{
    private static final String TAG = TourGuideHandler.class.toString();
    private TourGuideService service;
    private AsyncHttpClient client;
    private ServiceDTO dto;
    private int startId;
    private TourGuideDataSource dataSource;


    @Override
    public void handleMessage(Message msg)
    {
        super.handleMessage(msg);
        dto = (ServiceDTO) msg.obj;
        startId = msg.arg1;

        dataSource.createTourGuide(dto.tourGuideVo);
        //dataSource.readAllTourGuides();

        service.stopSelf(startId);
    }

    public TourGuideService getService()
    {
        return service;
    }

    public void setService(TourGuideService service) {
        this.service = service;
        dataSource = new TourGuideDataSource(service.getBaseContext());
    }
}
