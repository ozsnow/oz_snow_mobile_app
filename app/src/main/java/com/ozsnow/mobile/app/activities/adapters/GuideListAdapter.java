package com.ozsnow.mobile.app.activities.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.GuideListActivity;
import com.ozsnow.mobile.app.activities.TourGuideFormActivity;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

import java.util.Calendar;
import java.util.List;

/**
 * Created by russellmilburn on 14/04/2016.
 */
public class GuideListAdapter extends RecyclerView.Adapter
{
    protected static final String TAG = GuideListAdapter.class.toString();

    protected List<TourGuideVo> guides;
    protected Context context;

    public GuideListAdapter(Context context, List<TourGuideVo> list)
    {
        this.context = context;
        guides = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.guide_list_item, viewGroup, false);
        return new GuideListHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        ((GuideListHolder) holder).bindView(position);
    }

    @Override
    public int getItemCount()
    {
        return guides.size();
    }

    private class GuideListHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {

        private TextView nameField;
        private int position;

        public GuideListHolder(View itemView)
        {
            super(itemView);
            nameField = (TextView) itemView.findViewById(R.id.guideNameTextView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            Log.i(TAG, "onClick");
            GuideListActivity activity = (GuideListActivity) context;
            TourGuideVo vo = guides.get(position);
            vo.setDate(Calendar.getInstance().getTime().getTime());
            activity.tourGuideSelected(vo);
        }

        public void bindView(int position)
        {
            this.position = position;
            nameField.setText(guides.get(position).getGuideName());
        }
    }
}
