package com.ozsnow.mobile.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.ozsnow.mobile.app.models.vos.*;

/**
 * Created by russellmilburn on 04/04/2016.
 */
public class CustomerDataSource extends BaseDataSource implements IDataSource
{
    private static final String TAG = CustomerDataSource.class.toString();

    public CustomerDataSource(Context context)
    {
        super(context);

        //resetHasSynced(CustomerVo.CustomerFields.CUSTOMER_TBL);
    }

    public int createCustomer(CustomerVo vo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(CustomerVo.CustomerFields.FIRST_NAME, vo.getFirstName());
        cv.put(CustomerVo.CustomerFields.LAST_NAME, vo.getLastName());
        cv.put(CustomerVo.CustomerFields.HAS_SYNCED, 0);

        long id = database.insert(CustomerVo.CustomerFields.CUSTOMER_TBL, null, cv);

        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);

        return Integer.parseInt(id +"");
    }

    public CustomerVo readCustomer(int id)
    {
        SQLiteDatabase database = open();
        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + CustomerVo.CustomerFields.CUSTOMER_TBL +
                        " WHERE " + CustomerVo.CustomerFields.ID + " = " + id, null);

        CustomerVo vo = null;
        if (cursor.moveToFirst())
        {
            do {
                vo = new CustomerVo();
                vo.setCustomerID(getIntFromColumnName(cursor, CustomerVo.CustomerFields.ID));
                vo.setFirstName(getStringFromColumnName(cursor, CustomerVo.CustomerFields.FIRST_NAME));
                vo.setLastName(getStringFromColumnName(cursor, CustomerVo.CustomerFields.LAST_NAME));
                if (getIntFromColumnName(cursor, CustomerVo.CustomerFields.HAS_SYNCED) == 1) {
                    vo.setHasSynced(true);
                } else {
                    vo.setHasSynced(false);
                }
            }
            while (cursor.moveToNext());
        }
        close(database);
        return vo;
    }

    public void deleteCustomer(int id)
    {
        SQLiteDatabase database = open();

        database.delete(CustomerVo.CustomerFields.CUSTOMER_TBL,
                CustomerVo.CustomerFields.ID + " = ?",
                new String[]{Integer.toString(id)});

        close(database);
    }

    @Override
    public void setHasRecordSynced(int id, boolean hasSynced)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();
        
        ContentValues updateValues = new ContentValues();
        updateValues.put(CustomerVo.CustomerFields.HAS_SYNCED, hasSynced);

        int value = database.update(CustomerVo.CustomerFields.CUSTOMER_TBL,
                updateValues,
                String.format("%s=%d", CustomerVo.CustomerFields.ID, id),
                null);

        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);

    }
}
