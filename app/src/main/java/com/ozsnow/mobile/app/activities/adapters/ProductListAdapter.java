package com.ozsnow.mobile.app.activities.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.ProductListActivity;
import com.ozsnow.mobile.app.models.vos.ProductVo;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by russellmilburn on 14/04/2016.
 */
public class ProductListAdapter extends RecyclerView.Adapter implements Filterable
{
    protected static final String TAG = ProductListAdapter.class.toString();

    private Context context;
    private List<ProductVo> products;
    private Filter filter;

    public ProductListAdapter(Context context, List<ProductVo> list)
    {
        this.context = context;
        products = new ArrayList<>();
        products.addAll(list);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_list_item, viewGroup, false);
        return new ProductListHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
    {
        ((ProductListHolder) viewHolder).bindView(position);
    }

    @Override
    public int getItemCount() {
        return products.size();
    }


    public void setFilter(Filter filter)
    {
        this.filter = filter;
    }

    @Override
    public Filter getFilter() {
        return filter;
    }




    private class ProductListHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private TextView productNameTextView;
        private TextView productDecsTextView;
        private TextView productPriceTextView;
        private RelativeLayout highLightContainer;
        private int position;


        public ProductListHolder(View itemView)
        {
            super(itemView);
            productNameTextView = (TextView) itemView.findViewById(R.id.productNameTextView);
            productDecsTextView = (TextView) itemView.findViewById(R.id.productDescTextView);
            productPriceTextView = (TextView) itemView.findViewById(R.id.productPriceTextView);
            highLightContainer = (RelativeLayout) itemView.findViewById(R.id.highlight_container);
            itemView.setOnClickListener(this);
        }

        public void bindView(int position)
        {
            this.position = position;
            ProductVo vo = products.get(position);
            productNameTextView.setText(vo.getProductName());
            productDecsTextView.setText(vo.getProductDesc());

            if(vo.getDuration() >0  && vo.getDuration() <3)
            {
                highLightContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.short_day));
            }
            else if (vo.getDuration() >= 3 && vo.getDuration() <5)
            {
                highLightContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.mid_day));
            }
            else if (vo.getDuration() >= 5)
            {
                highLightContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.long_day));
            }

            if (vo.getProductName().toLowerCase().indexOf("(performance)") != -1)
            {
                highLightContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.pro_colour));
            }
            if(vo.getProductName().toLowerCase().indexOf("(standard)") != -1)
            {
                highLightContainer.setBackgroundColor(ContextCompat.getColor(context, R.color.std_colour));
            }

            productPriceTextView.setText(NumberFormat.getCurrencyInstance().format(products.get(position).getPrice()));
        }


        @Override
        public void onClick(View view)
        {
            Log.i(TAG, "onClick");

            ProductVo vo = products.get(position);
            ProductListActivity activity = (ProductListActivity) context;
            activity.productSelected(vo);
        }
    }



    public static class ProductFilter extends Filter
    {
        private final ProductListAdapter adapter;
        private final List<ProductVo> originalList;
        private List<ProductVo> filterList;


        public ProductFilter(ProductListAdapter adapter, List<ProductVo> originalList)
        {
            super();
            this.adapter = adapter;
            this.originalList = originalList;
            filterList = new ArrayList<>();


        }

        @Override
        protected FilterResults performFiltering(final CharSequence constraint)
        {
            filterList.clear();
            FilterResults results = new FilterResults();

            int startValue = 0;
            int endValue = 0;
            if (constraint.length() == 0)
            {
                filterList.addAll(originalList);
            }
            else
            {
                if (constraint.equals("1-2"))
                {
                    startValue = 1;
                    endValue = 2;
                }
                else if (constraint.equals("3-4"))
                {
                    startValue = 3;
                    endValue = 4;
                }
                else if (constraint.equals("5-6"))
                {
                    startValue = 5;
                    endValue = 6;
                }

                for (final ProductVo vo : originalList)
                {
                    int noOfDaysHired = 0;
                    try {
                        noOfDaysHired = Integer.parseInt(String.valueOf(vo.getProductDesc().charAt(0)));
                    }
                    catch (NumberFormatException nfe)
                    {
                        if (startValue == 1)
                        {
                            filterList.add(vo);
                        }
                    }

                    if (noOfDaysHired >= startValue && noOfDaysHired <= endValue)
                    {
                        filterList.add(vo);
                    }

                }
            }

            results.values = filterList;
            results.count = filterList.size();
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results)
        {
            adapter.products.clear();
            adapter.products.addAll((List<ProductVo>) results.values);
            adapter.notifyDataSetChanged();
        }
    }
}
