package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

import java.util.ArrayList;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class ReadCurrentTourGuidesTask extends AsyncTask<Integer, Void, TourGuideVo>
{
    private TourGuideDataSource dataSource;

    public ReadCurrentTourGuidesTask(TourGuideDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected TourGuideVo doInBackground(Integer... ids)
    {
        return dataSource.readTourGuide(ids[0]);
    }

}
