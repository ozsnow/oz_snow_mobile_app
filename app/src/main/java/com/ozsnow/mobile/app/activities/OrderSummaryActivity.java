package com.ozsnow.mobile.app.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.adapters.SummaryOfProductsAdapter;
import com.ozsnow.mobile.app.database.ProductDataSource;
import com.ozsnow.mobile.app.models.vos.*;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class OrderSummaryActivity extends AppCompatActivity
{
    public static final String TAG = OrderSummaryActivity.class.toString();
    public static final String ORDER_LIST_VO_EXTRA = "orderListVoExtra";

    private RecyclerView recyclerView;
    private TextView orderTotalField;
    private TextView customerNameField;
    private TextView destinationField;
    private TextView dateField;
    private TextView guideField;
    private SummaryOfProductsAdapter adapter;
    private ProductDataSource productDataSource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_sumarry);


        productDataSource = new ProductDataSource(this);

        customerNameField = (TextView) findViewById(R.id.summary_order_customer_text_view);
        destinationField = (TextView) findViewById(R.id.summary_destination_text_field);
        dateField = (TextView) findViewById(R.id.summary_order_date_text_field);
        guideField = (TextView) findViewById(R.id.summary_order_tour_guide_text_view);
        orderTotalField = (TextView) findViewById(R.id.summary_orderTotalTextView);
        recyclerView = (RecyclerView) findViewById(R.id.summary_selectedProductListRecyclerView);

        VoidOrderListVo vo = (VoidOrderListVo) getIntent().getSerializableExtra(ORDER_LIST_VO_EXTRA);

        setTitle("Order Summary for Order ID: " + vo.getTerminalId() + "010" + vo.getOrderVo().getOrderId());

        customerNameField.setText(vo.getCustomerVo().getFirstName() + " " + vo.getCustomerVo().getLastName());

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy");
        String formatted = sdf.format(vo.getOrderVo().getOrderDate());
        dateField.setText(formatted);

        guideField.setText(vo.getGuideVo().getGuideName());


        orderTotalField.setText("Order Total:\t " + NumberFormat.getCurrencyInstance().format(vo.getOrderVo().getOrderTotal()));

        adapter = new SummaryOfProductsAdapter(this, vo.getOrderVo().getOrderItems());
        recyclerView.setAdapter(adapter);

        destinationField.setText(getDestination(vo.getOrderVo()));

    }

    private String getDestination(OrderVo vo)
    {
        for (int i = 0; i < vo.getOrderItems().size(); i++)
        {
            ProductVo product = productDataSource.getProduct(vo.getOrderItems().get(i).getProductId());

            if (!product.getMountain().equals("NA"))
            {
                return product.getMountain();
            }
        }
        return "No Destination specified for order";
    }

}
