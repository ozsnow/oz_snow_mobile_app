package com.ozsnow.mobile.app.services.guide;

import android.os.Looper;

/**
 * Created by russellmilburn on 06/04/2016.
 */
public class TourGuideThread extends Thread
{
    private TourGuideHandler tourGuideHandler;

    @Override
    public void run() {
        super.run();
        Looper.prepare();
        tourGuideHandler = new TourGuideHandler();
        Looper.loop();
    }

    public TourGuideHandler getTourGuideHandler()
    {
        return tourGuideHandler;
    }

    public void setTourGuideHandler(TourGuideHandler tourGuideHandler)
    {
        this.tourGuideHandler = tourGuideHandler;
    }
}
