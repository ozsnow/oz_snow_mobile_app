package com.ozsnow.mobile.app.services.sync;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.WifiManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ozsnow.mobile.app.MainActivity;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.database.IDataSource;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.database.tasks.SetHasSyncedTask;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import com.ozsnow.mobile.app.models.vos.OrderVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;
import com.ozsnow.mobile.app.services.OzSnowService;
import com.ozsnow.mobile.app.services.product.ProductDownloadService;
import cz.msebera.android.httpclient.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by russellmilburn on 15/05/2016.
 */
public class SyncDataService extends Service implements DataListener
{
    protected static final String TAG = SyncDataService.class.toString();

    private SyncDataHandler handler;

    public static final int IDLE_STATE = 0;
    public static final int GUIDE_STATE = 1;
    public static final int CUSTOMER_STATE = 2;
    public static final int ORDER_STATE = 3;
    public static final int COMPLETE_STATE = 4;

    private int currentState = IDLE_STATE;

    private TourGuideDataSource guideDataSource;
    private CustomerDataSource customerDataSource;
    private OrderDataSource orderDataSource;
    private SyncServiceListener listener;

    private ArrayList<TourGuideVo> guideList;
    private ArrayList<CustomerVo> customerList;
    private ArrayList<OrderVo> orderList;

    private int index = -1;
    private int total = 0;
    private int terminalId;
    private boolean isProcessing;

    private AsyncHttpClient client;
    private JSONArray guides;
    private JSONArray customers;
    private JSONArray orders;
    private int startId;
    private IBinder binder = new LocalBinder();


    @Override
    public void onCreate() {
        super.onCreate();

        client = new AsyncHttpClient();

        guideDataSource = new TourGuideDataSource(getApplicationContext());
        customerDataSource = new CustomerDataSource(getApplicationContext());
        orderDataSource = new OrderDataSource(getApplicationContext());

        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        terminalId = settings.getInt(PreferenceList.TERMINAL_ID.toString(), -1);



        SyncDataThread thread = new SyncDataThread();
        thread.setName("SyncDataThread");
        thread.start();

        while (thread.getHandler() == null)
        {

        }

        handler = thread.getHandler();
        handler.setService(this);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        this.startId = startId;
        Log.i(TAG, "onStartCommand");
        if (terminalId != -1)
        {

            Message message = Message.obtain();
            handler.sendMessage(message);
        }

        return START_REDELIVER_INTENT;
    }

    public class LocalBinder extends Binder
    {
        public SyncDataService getService()
        {
            return SyncDataService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return binder;
    }

    public void startSync()
    {
        Log.i(TAG, "isProcessing :" + isProcessing);
        if (isProcessing == true)
        {
            return;
        }

        isProcessing = true;

        guideList = new ArrayList<>();
        customerList = new ArrayList<>();
        orderList = new ArrayList<>();

        orderDataSource.setListener(this);
        getData();
    }

    private void getData()
    {
        orderDataSource.readUnsyncedOrders();
    }

    private void checkTourGuideSync(int guideId)
    {
        TourGuideVo vo = guideDataSource.readTourGuide(guideId);

        for (TourGuideVo aGuideList : guideList) {
            if (aGuideList.getGuideId() == getRemoteId(guideId)) {
                return;
            }
        }

        if (vo.hasSynced() == false)
        {
            vo.setGuideId(getRemoteId(vo.getGuideId()));
            guideList.add(vo);
            //Log.i(TAG, "list length: " + guideList.size());
        }
    }

    private void checkCustomerSync(int customerId)
    {
        CustomerVo vo = customerDataSource.readCustomer(customerId);

        for (CustomerVo aCustomerList : customerList) {
            if (aCustomerList.getCustomerID() == getRemoteId(customerId)) {
                return;
            }
        }
        if (vo.isHasSynced() == false)
        {
            vo.setCustomerID(getRemoteId(vo.getCustomerID()));
            customerList.add(vo);
            //Log.i(TAG, "customerList length: " + customerList.size());
        }
    }

    private void getCompleteOrder(int orderId)
    {
        OrderVo vo = orderDataSource.readOrder(orderId);
        vo.setOrderId(getRemoteId(orderId));
        vo.setCustomerId(getRemoteId(vo.getCustomerId()));
        vo.setGuideId(getRemoteId(vo.getGuideId()));
        orderList.add(vo);
    }


    @Override
    public void onDataReady(ArrayList<OrderVo> orders)
    {
        orderDataSource.setListener(null);

        for (OrderVo currentOrder : orders)
        {
            checkTourGuideSync(currentOrder.getGuideId());
            checkCustomerSync(currentOrder.getCustomerId());
            getCompleteOrder(currentOrder.getOrderId());
        }

        Log.i(TAG, "Order List size: " + orderList.size());
        Log.i(TAG, "Customer List size: " + customerList.size());
        Log.i(TAG, "Guide List size: " + guideList.size());

        JSONArray array = new JSONArray();
        array.put(guideList.toString());

//        Log.i(TAG, array.toString());

        try
        {
            sendData();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }




    private void sendData() throws Exception
    {

        setCurrentState(GUIDE_STATE);
        Gson gson = new Gson();
        guides = new JSONArray(gson.toJson(guideList, new TypeToken<ArrayList<TourGuideVo>>(){}.getType()));
        customers = new JSONArray(gson.toJson(customerList, new TypeToken<ArrayList<CustomerVo>>(){}.getType()));
        orders = new JSONArray(gson.toJson(orderList, new TypeToken<ArrayList<OrderVo>>(){}.getType()));


        setCurrentState(ORDER_STATE);
        next();
    }

    private void next()
    {
        index++;

        if (listener != null)
        {
            listener.onProgress(index);
        }

        switch (currentState)
        {
            case GUIDE_STATE:
                saveGuide();
                break;

            case CUSTOMER_STATE:
                saveCustomer();
                break;

            case ORDER_STATE:
                saveOrder();
                break;
            case COMPLETE_STATE:
                Log.i(TAG, "upload Complete");
                isProcessing = false;
                if (listener != null)
                {
                    Log.i(TAG, "Not Orders");
                    listener.onComplete();
                }

                SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.remove(PreferenceList.CURRENT_TOUR_GUIDE.toString());
                editor.commit();

                stopSelf(startId);
                break;
        }
    }

    public int getCurrentState() {
        return currentState;
    }

    private void setCurrentState(int value)
    {
        if (currentState == value )
        {
            return;
        }
        currentState = value;

        setTotal();

        if (listener != null)
        {
            listener.onChangeState(currentState, total);
        }
    }

    private void saveGuide()
    {
        JSONObject obj = null;
        try {
            obj = guides.getJSONObject(index);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestParams params = new RequestParams();
        params.put("guide", obj.toString());
        //Log.i(TAG, obj.toString());

        client.post(getApplicationContext(), OzSnowService.BASE_URL + "/service/guide/add", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
            {
                int guideId = getLocalId(guideList.get(index).getGuideId());
                Log.i(TAG, "guideId:" + guideId);
                executeHasSyncTask(guideDataSource, guideId);
                checkUploadComplete(guides);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Guide onFailure:" +  statusCode );
                abort();
            }
        });

    }

    private void checkUploadComplete(JSONArray a)
    {
        if (index < a.length()-1)
        {
            next();
        }
        else
        {
            Log.i(TAG, "finished");
            index = -1;
            if (getCurrentState() == ORDER_STATE)
            {
                setCurrentState(CUSTOMER_STATE);
            }
            else if (getCurrentState() == CUSTOMER_STATE)
            {
                setCurrentState(GUIDE_STATE);
            }
            else if (getCurrentState() == GUIDE_STATE)
            {
                setCurrentState(COMPLETE_STATE);

            }
            next();

        }
    }


    private void saveCustomer()
    {
        JSONObject obj = null;
        try {
            obj = customers.getJSONObject(index);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        RequestParams params = new RequestParams();
        params.put("customer", obj.toString());
        //Log.i(TAG, obj.toString());

        client.post(getApplicationContext(), OzSnowService.BASE_URL + "/service/customer/add", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
            {
                int customerId = getLocalId(customerList.get(index).getCustomerID());
                Log.i(TAG, "customerId:" + customerId);
                executeHasSyncTask(customerDataSource, customerId);
                checkUploadComplete(customers);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "customer onFailure:" +  statusCode );
                abort();
            }
        });
    }

    public void saveOrder()
    {
        JSONObject obj = null;
        try {
            obj = orders.getJSONObject(index);
            int id = obj.getInt("orderId");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestParams params = new RequestParams();
        params.put("order", obj.toString());

        client.post(getApplicationContext(), OzSnowService.BASE_URL + "/service/order/add", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody)
            {
                int orderId = getLocalId(orderList.get(index).getOrderId());
                Log.i(TAG, "orderId: " + orderId);
                executeHasSyncTask(orderDataSource, orderId);
                checkUploadComplete(orders);
            }

            @Override
            public void onProgress(long bytesWritten, long totalSize) {
                super.onProgress(bytesWritten, totalSize);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.i(TAG, "Order onFailure:" +  statusCode);
                abort();
            }
        });

    }

    public SyncServiceListener getListener() {
        return listener;
    }

    public void setListener(SyncServiceListener listener) {
        this.listener = listener;
    }

    private void executeHasSyncTask(IDataSource dataSource, int id)
    {
        SetHasSyncedTask task = new SetHasSyncedTask(dataSource);
        task.execute(id);
    }


    private int getRemoteId(int id)
    {
        return Integer.parseInt(terminalId + "010" + id);
    }

    private int getLocalId(int id)
    {
        String remoteId = String.valueOf(id);
        int length = remoteId.length();
        String terminalID = terminalId + "010";
        int i = remoteId.indexOf(terminalID);
        String localId = remoteId.substring(i + terminalID.length(), remoteId.length());
        return Integer.parseInt(localId);
    }

    private void setTotal()
    {
        if (currentState == GUIDE_STATE)
        {
            total = guideList.size();
        }
        else if (currentState == CUSTOMER_STATE)
        {
            total = customerList.size();
        }
        else if (currentState == ORDER_STATE)
        {
            total = orderList.size();
        }

    }

    private void abort()
    {
        isProcessing = false;
        stopSelf(startId);
    }
}
