package com.ozsnow.mobile.app.services.product;

import android.os.Looper;

/**
 * Created by russellmilburn on 26/03/2016.
 */
public class ProductServiceThread extends Thread
{
    ProductServiceHandler productServiceHandler;

    @Override
    public void run() {
        super.run();
        Looper.prepare();
        productServiceHandler = new ProductServiceHandler();
        Looper.loop();
    }

    public ProductServiceHandler getProductServiceHandler()
    {
        return productServiceHandler;
    }
}
