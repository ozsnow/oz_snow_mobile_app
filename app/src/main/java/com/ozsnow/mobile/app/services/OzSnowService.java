package com.ozsnow.mobile.app.services;

import android.app.IntentService;

/**
 * Created by russellmilburn on 11/03/2016.
 */
public abstract class OzSnowService extends IntentService
{
//    public static final String BASE_URL = "http://10.0.2.2:8080/api"; //Google Emulator

//    public static final String BASE_URL = "http://10.0.3.2:8080/api"; //Geny Emulator
//    public static final String BASE_URL = "http://192.168.0.10:8080/api"; //Nexus 7
//        public static final String BASE_URL = "http://124.149.158.200:7070/OzSnowREST/api"; //Nexus 7
//    public static final String BASE_URL = "http://ec2-54-206-21-97.ap-southeast-2.compute.amazonaws.com:8080/OzSnowREST/api"; //Staging URI
      public static final String BASE_URL = "http://snowtravel.com.au/mobhire/api/"; //Production URL

    public OzSnowService(String name) {
        super(name);
    }


}
