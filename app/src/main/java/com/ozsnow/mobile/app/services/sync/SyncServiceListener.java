package com.ozsnow.mobile.app.services.sync;

/**
 * Created by russellmilburn on 18/05/2016.
 */
public interface SyncServiceListener
{
    void onChangeState(int state, int total);
    void onProgress(int progress);
    void onComplete();

}
