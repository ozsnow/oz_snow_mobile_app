package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class CreateTourGuideTask extends AsyncTask<TourGuideVo, Void, Integer>
{
    private TourGuideDataSource dataSource;

    public CreateTourGuideTask(TourGuideDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected Integer doInBackground(TourGuideVo... tourGuideVos)
    {
        return dataSource.createTourGuide(tourGuideVos[0]);
    }


}
