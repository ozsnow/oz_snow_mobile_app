package com.ozsnow.mobile.app.services.product;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.activities.MainMenuActivity;
import com.ozsnow.mobile.app.activities.ProductDownloadActivity;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.emum.RemoteServiceCommand;
import com.ozsnow.mobile.app.services.ServiceDTO;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by russellmilburn on 11/03/2016.
 */
public class ProductDownloadService extends Service
{

    protected static final String TAG = ProductDownloadService.class.toString();

    private int loadProgress = 0;
    private IBinder binder = new LocalBinder();
    private ProductServiceHandler handler;


    @Override
    public IBinder onBind(Intent intent)
    {
        return binder;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        Log.i(TAG, "onCreate");

        ProductServiceThread thread = new ProductServiceThread();
        thread.setName("ProductServiceThread");
        thread.start();

        while (thread.getProductServiceHandler() == null)
        {

        }

        handler = thread.getProductServiceHandler();
        handler.setService(this);
    }

    public int getLoadProgress()
    {
        return loadProgress;
    }

    public void setLoadProgress(int loadProgress)
    {
        this.loadProgress = loadProgress;
        LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(ProductDownloadActivity.ON_LOAD_PROGRESS));
    }

    public void writeToPreferences(Date lastUpdated)
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(PreferenceList.LAST_PRODUCT_UPDATE.toString(), lastUpdated.getTime());
        editor.commit();
    }

    public void writeThredboSeason(JSONObject vo)
    {
        Log.i(TAG, vo.toString());
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PreferenceList.THREDBO_SEASON_DATES.toString(), vo.toString());
        editor.commit();
    }

    public void writePerisherSeason(JSONObject vo)
    {
        Log.i(TAG, vo.toString());
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PreferenceList.PERISHER_SEASON_DATES.toString(), vo.toString());
        editor.commit();
    }

    public void checkForNewProducts()
    {
        Intent intent = new Intent(this, ProductDownloadActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void openMainMenu()
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public class LocalBinder extends Binder
    {
        public ProductDownloadService getService()
        {
            return ProductDownloadService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        ServiceDTO vo;
        if (intent.hasExtra(RemoteServiceCommand.CHECK_PRODUCTS_UPDATED.name()))
        {
            vo = new ServiceDTO();
            vo.command = (RemoteServiceCommand) intent.getSerializableExtra(RemoteServiceCommand.CHECK_PRODUCTS_UPDATED.name());
            Message message = Message.obtain();
            message.obj = vo;
            message.arg1 = startId;
            handler.sendMessage(message);
        }

        if (intent.hasExtra(RemoteServiceCommand.GET_ALL_PRODUCTS.name()))
        {
            vo = new ServiceDTO();
            vo.command = (RemoteServiceCommand) intent.getSerializableExtra(RemoteServiceCommand.GET_ALL_PRODUCTS.name());
            Message message = Message.obtain();
            message.obj = vo;
            message.arg1 = startId;
            handler.sendMessage(message);
        }
        return Service.START_REDELIVER_INTENT;
    }






}
