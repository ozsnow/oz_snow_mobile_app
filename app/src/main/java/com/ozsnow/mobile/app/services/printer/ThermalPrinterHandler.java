package com.ozsnow.mobile.app.services.printer;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.ozsnow.mobile.app.models.vos.DeviceVo;

/**
 * Created by russellmilburn on 08/05/2016.
 */
public class ThermalPrinterHandler extends Handler
{
    private static final String TAG = ThermalPrinterHandler.class.toString();


    private ThermalPrinterService service;


    @Override
    public void handleMessage(Message msg)
    {
        switch (msg.what)
        {
            case ThermalPrinterService.SCAN_COMMAND:

                Log.i(TAG, "ThermalPrinterService.SCAN_COMMAND");
                getService().discoverDevices();
                break;

            case ThermalPrinterService.CONNECT_COMMAND:
                getService().connectToPrinter((DeviceVo) msg.obj);
                break;

            case ThermalPrinterService.PRINT_COMMAND:
                getService().writeToPrinter((String) msg.obj);
                break;

            case ThermalPrinterService.PRINT_IMAGE_COMMAND:
                getService().printImage((byte[]) msg.obj);
                break;
        }

    }

    public ThermalPrinterService getService() {
        return service;
    }

    public void setService(ThermalPrinterService service) {
        this.service = service;
    }
}
