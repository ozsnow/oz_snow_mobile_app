package com.ozsnow.mobile.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.ozsnow.mobile.app.activities.MainMenuActivity;
import com.ozsnow.mobile.app.activities.NumberKeypadActivity;
import com.ozsnow.mobile.app.activities.ProductDownloadActivity;
import com.ozsnow.mobile.app.models.emum.LocalServiceCommand;
import com.ozsnow.mobile.app.models.emum.RemoteServiceCommand;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;
import com.ozsnow.mobile.app.services.guide.TourGuideService;
import com.ozsnow.mobile.app.services.product.ProductDownloadService;

import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity
{
    private static final String PIN_CODE = "6969";

    private Button submitBtn;
    private EditText pinCodeIp;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        submitBtn = (Button) findViewById(R.id.submitBtn);
        pinCodeIp = (EditText) findViewById(R.id.pinCodeIp);

        submitBtn.setOnClickListener(submitListener);

    }


    View.OnClickListener submitListener = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            //TODO: comment out for release
//            checkForProductUpdates();

            String pin = pinCodeIp.getText().toString();
            if (pin.contentEquals(PIN_CODE))
            {
                checkForProductUpdates();
            }
            else
            {
                Toast.makeText(MainActivity.this, "Incorrect pin code",Toast.LENGTH_LONG).show();
            }
        }
    };

    private void checkForProductUpdates()
    {
        WifiManager wifi = (WifiManager)getSystemService(Context.WIFI_SERVICE);

        if (wifi.isWifiEnabled())
        {
            Intent productServiceIntent = new Intent();
            productServiceIntent.setClass(this, ProductDownloadService.class);
            productServiceIntent.putExtra(RemoteServiceCommand.CHECK_PRODUCTS_UPDATED.name(), RemoteServiceCommand.CHECK_PRODUCTS_UPDATED);
            startService(productServiceIntent);
        }
        else
        {
            Intent intent = new Intent(this, MainMenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }



    }



    public void submitTourGuide()
    {
        TourGuideVo vo = new TourGuideVo();
        vo.setGuideName("Russell");
        Date date = Calendar.getInstance().getTime();
        vo.setDate(date.getTime());

        Intent intent = new Intent(this, TourGuideService.class);
        intent.putExtra(LocalServiceCommand.CREATE_TOUR_GUIDE.name(), vo);

        startService(intent);



    }
}
