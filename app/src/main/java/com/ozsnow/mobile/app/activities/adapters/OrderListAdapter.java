package com.ozsnow.mobile.app.activities.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.OrderSummaryActivity;
import com.ozsnow.mobile.app.models.vos.VoidOrderListVo;

import java.util.List;

/**
 * Created by russellmilburn on 28/05/2016.
 */
public class OrderListAdapter extends VoidOrderListAdapter
{
    public static final String TAG = OrderListAdapter.class.toString();

    public OrderListAdapter(Context context, List<VoidOrderListVo> orders)
    {
        super(context, orders);
    }

    @Override
    public VoidOrderListItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_list_item, viewGroup, false);
        return new OrderListItemHolder(view);
    }

    public class OrderListItemHolder extends VoidOrderListItemHolder implements View.OnClickListener
    {
        public OrderListItemHolder(View itemView) {
            super(itemView);
            Log.i(TAG, "OrderListItemHolder");
        }

        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(context, OrderSummaryActivity.class);
            intent.putExtra(OrderSummaryActivity.ORDER_LIST_VO_EXTRA, super.vo);
            context.startActivity(intent);
        }
    }
}


