package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.models.vos.OrderVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class SaveOrderTask extends AsyncTask<OrderVo, Void, Boolean>
{
    private OrderDataSource dataSource;

    public SaveOrderTask(OrderDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected Boolean doInBackground(OrderVo... orderVos)
    {
        return dataSource.saveOrder(orderVos[0]);
    }


}
