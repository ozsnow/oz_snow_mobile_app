package com.ozsnow.mobile.app.models.vos;

import java.io.Serializable;

/**
 * Created by russellmilburn on 12/05/2016.
 */
public class VoidOrderListVo implements Serializable
{
    private CustomerVo customerVo;
    private OrderVo orderVo;
    private TourGuideVo guideVo;
    private int terminalId;

    public CustomerVo getCustomerVo() {
        return customerVo;
    }

    public void setCustomerVo(CustomerVo customerVo) {
        this.customerVo = customerVo;
    }

    public OrderVo getOrderVo() {
        return orderVo;
    }

    public void setOrderVo(OrderVo orderVo) {
        this.orderVo = orderVo;
    }

    public TourGuideVo getGuideVo() {
        return guideVo;
    }

    public void setGuideVo(TourGuideVo guideVo) {
        this.guideVo = guideVo;
    }

    public int getTerminalId() {
        return terminalId;
    }

    public void setTerminalId(int terminalId) {
        this.terminalId = terminalId;
    }
}
