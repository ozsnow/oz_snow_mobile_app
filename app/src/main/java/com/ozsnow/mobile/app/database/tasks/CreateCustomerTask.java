package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.models.vos.CustomerVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class CreateCustomerTask extends AsyncTask<CustomerVo, Void, Integer>
{
    private CustomerDataSource dataSource;

    public CreateCustomerTask(CustomerDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected Integer doInBackground(CustomerVo... customerVos)
    {
        return dataSource.createCustomer(customerVos[0]);
    }

}
