package com.ozsnow.mobile.app.models.vos;

import android.provider.BaseColumns;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by russellmilburn on 04/04/2016.
 */
public class CustomerVo implements Serializable
{
    public static final class CustomerFields implements BaseColumns
    {
        public static final String CUSTOMER_TBL = "CustomerTbl";

        public static final String ID = "id";
        public static final String FIRST_NAME = "firstName";
        public static final String LAST_NAME = "lastName";
        public static final String HAS_SYNCED = "hasSynced";
    }

    private int customerID = -1;
    private String firstName;
    private String lastName;
    private boolean hasSynced = false;

    public CustomerVo()
    {

    }


    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        this.customerID = customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isHasSynced() {
        return hasSynced;
    }

    public void setHasSynced(boolean hasSynced) {
        this.hasSynced = hasSynced;
    }

    @Override
    public String toString()
    {
        return "[CustomerVo id: " + this.customerID +
                ", firstName: " + firstName +
                ", lastName: " + lastName +
                ", hasSynced: " + hasSynced +
                "]";
    }
}
