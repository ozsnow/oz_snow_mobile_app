package com.ozsnow.mobile.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.ozsnow.mobile.app.models.vos.CategoryVo;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import com.ozsnow.mobile.app.models.vos.ProductTypeVo;
import com.ozsnow.mobile.app.models.vos.ProductVo;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by russellmilburn on 03/04/2016.
 */
public class ProductDataSource extends BaseDataSource
{
    private static final String TAG = ProductDataSource.class.toString();

    public ProductDataSource(Context context)
    {
        super(context);
    }

    public void deleteProductData()
    {
        SQLiteDatabase database = open();
        database.delete(ProductVo.ProductFields.PRODUCT_TBL, null, null);
        database.delete(ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_TBL, null, null);
        database.delete(CategoryVo.CategoryFields.CATEGORY_TBL, null, null);

        close(database);

    }

    public ArrayList<ProductVo> getLiftPasses(int categoryId, String mountain, boolean isPeak)
    {
        ArrayList<ProductVo> list = null;

        SQLiteDatabase database = open();

        String queryString = "SELECT * FROM ProductTbl, ProductTypeTbl WHERE " +
                "ProductTbl.productId = ProductTypeTbl.productId AND categoryId = ? AND " + ProductVo.ProductFields.MOUNTAIN + " = ?";

        Cursor cursor = database.rawQuery(queryString,
                new String[]{Integer.toString(categoryId), mountain});

        list = processCursor(cursor);
        close(database);

        ArrayList<ProductVo> liftPassPriceList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++)
        {
            ProductVo item = list.get(i);
            for (int j = 0; j < item.getProductTypeVo().getPriceArray().length; j++)
            {
                BigDecimal[] prices = item.getProductTypeVo().getPriceArray();
                if (prices[j].compareTo(BigDecimal.ZERO) > 0)
                {
                    String numberOfDays = (j + 1) +"";
//                    Log.i(TAG, numberOfDays + " Day " + item.getProductName() +  " at " + item.getMountain() + " $" + prices[j].toString());
                    ProductVo vo = new ProductVo();
                    vo.setProductID(item.getProductID());
                    vo.setProductName(item.getProductName());

                    if ((j + 1) == 6)
                    {
                        vo.setProductDesc("5.5 Days");
                    }
                    else
                    {
                        vo.setProductDesc(numberOfDays + " Days");
                    }


                    vo.setDuration((j + 1));
                    vo.setCategoryID(item.getCategoryID());
                    vo.setMountain(item.getMountain());
                    vo.setPrice(prices[j]);
                    vo.setProductTypeVo(item.getProductTypeVo());




                    if (isPeak)
                    {
                        if(!vo.getProductName().toLowerCase().matches(".*off peak.*"))
                        {
                            liftPassPriceList.add(vo);
                            Log.i(TAG, numberOfDays + " Day " + item.getProductName() +  " at " + item.getMountain() + " $" + prices[j].toString());
                        }
                    }
                    else
                    {
                        if(vo.getProductName().toLowerCase().matches(".*off peak.*"))
                        {
                            liftPassPriceList.add(vo);
                            Log.i(TAG, numberOfDays + " Day " + item.getProductName() +  " at " + item.getMountain() + " $" + prices[j].toString());
                        }
                    }
                }
            }
        }
        Collections.sort(liftPassPriceList);

        return liftPassPriceList;
    }


    public ArrayList<ProductVo> getClothes(int categoryId)
    {
        return getEquipment(4);
    }


    public ArrayList<ProductVo> getEquipment(int categoryId)
    {
        ArrayList<ProductVo> list = null;

        SQLiteDatabase database = open();

        String queryString = "SELECT * FROM ProductTbl, ProductTypeTbl WHERE " +
                "ProductTbl.productId = ProductTypeTbl.productId AND ProductTbl.categoryId = ?";

        Cursor cursor = database.rawQuery(queryString,
                new String[]{Integer.toString(categoryId)});

        list = processCursor(cursor);
        close(database);

        ArrayList<ProductVo> equipmentPriceList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++)
        {
            ProductVo item = list.get(i);
            for (int j = 0; j < item.getProductTypeVo().getPriceArray().length; j++)
            {
                BigDecimal[] prices = item.getProductTypeVo().getPriceArray();
                if (prices[j].compareTo(BigDecimal.ZERO) > 0)
                {
                    ProductVo vo = new ProductVo();
                    vo.setProductID(item.getProductID());
                    vo.setCategoryID(item.getCategoryID());
                    vo.setPrice(prices[j]);
                    vo.setDuration(j+ 1);
                    vo.setProductTypeVo(item.getProductTypeVo());
                    String numberOfDays = (j + 1) +"";
                    if (item.getProductTypeVo().isHireProduct())
                    {
//                        Log.i(TAG, numberOfDays + " Day hire price of " + item.getProductName() +  " is $" + prices[j].toString());
                        if (item.getProductTypeVo().getProductType().equals("NA") == true || item.getProductTypeVo().getProductType() == null)
                        {
                            vo.setProductName(item.getProductName() );
                        }
                        else
                        {
                            vo.setProductName(item.getProductName() + " (" + item.getProductTypeVo().getProductType() + ")" );

                        }

                        if (item.getProductDesc().toLowerCase().matches(".*free.*"))
                        {
                            vo.setPrice(new BigDecimal(0.00));
                        }

                        vo.setProductDesc(numberOfDays + " Day Hire");

                    }
                    else
                    {
//                        Log.i(TAG, item.getProductName() +  " costs $" + prices[j].toString());
                        vo.setProductName(item.getProductName());
                        vo.setProductDesc("per item");
                        vo.setDuration(15);
                    }



                    equipmentPriceList.add(vo);
                }
            }
        }

        Collections.sort(equipmentPriceList);

        return equipmentPriceList;
    }

    public ArrayList<ProductVo> getPackPriceList(int categoryId, String mountain, boolean isPeak)
    {
        ArrayList<ProductVo> list = null;

        SQLiteDatabase database = open();

        String queryString = "SELECT * FROM ProductTbl, ProductTypeTbl WHERE " +
                "ProductTbl.productId = ProductTypeTbl.productId AND categoryId = ? AND " + ProductVo.ProductFields.MOUNTAIN + " = ?";

        Cursor cursor = database.rawQuery(queryString,
                new String[]{Integer.toString(categoryId), mountain});

        list = processCursor(cursor);
        close(database);


        ArrayList<ProductVo> packsPriceList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++)
        {
            ProductVo item = list.get(i);
            for (int j = 0; j < item.getProductTypeVo().getPriceArray().length; j++)
            {
                BigDecimal[] prices = item.getProductTypeVo().getPriceArray();
                if (prices[j].compareTo(BigDecimal.ZERO) > 0)
                {
                    ProductVo vo = new ProductVo();
                    vo.setProductID(item.getProductID());
                    vo.setCategoryID(item.getCategoryID());
                    vo.setPrice(prices[j]);
                    vo.setDuration(j+ 1);
                    vo.setProductTypeVo(item.getProductTypeVo());
                    String numberOfDays = (j + 1) +"";
                    vo.setProductName(item.getProductName() + " (" + item.getProductTypeVo().getProductType()+ ")");
                    if ((j+ 1) == 6)
                    {
                        vo.setProductDesc("5.5 Day Pack " + item.getProductDesc());
                    }
                    else
                    {
                        vo.setProductDesc(numberOfDays + " Day Pack " + item.getProductDesc());
                    }

                    if (vo.getProductID() == 38 || vo.getProductID() == 44)
                    {
                        vo.setProductName(item.getProductName());
                        packsPriceList.add(vo);
                    }

//                    Log.i(TAG, numberOfDays + " Day " + item.getProductName() +  " is $" + prices[j].toString());

                    if (isPeak)
                    {
                        if(vo.getProductName().toLowerCase().matches(".*off peak.*") == false)
                        {
                            packsPriceList.add(vo);
                        }


                    }
                    else
                    {
                        if(vo.getProductName().toLowerCase().matches(".*off peak.*") == true)
                        {
                            packsPriceList.add(vo);
                        }
                    }



                }
            }
        }

        Collections.sort(packsPriceList);

        return packsPriceList;
    }




    public ArrayList<ProductVo> getProductByCategoryId(int categoryId)
    {
        ArrayList<ProductVo> list = new ArrayList<>();

        SQLiteDatabase database = open();
        String[] fields = new String[]
                {
                        ProductVo.ProductFields.PRODUCT_ID,
                        ProductVo.ProductFields.CATEGORY_ID,
                        ProductVo.ProductFields.PRODUCT_NAME,
                        ProductVo.ProductFields.PRODUCT_DESC,
                        ProductVo.ProductFields.MOUNTAIN,
                };

        Cursor cursor = database.query(ProductVo.ProductFields.PRODUCT_TBL,
                fields,
                ProductVo.ProductFields.CATEGORY_ID + " = ?",
                new String[]{Integer.toString(categoryId)},
                null,
                null,
                null);

        list = processCursor(cursor);
        close(database);
        return list;
    }

    private ArrayList<ProductVo> processCursor(Cursor cursor)
    {
        ArrayList<ProductVo> list = new ArrayList<>();
        ProductVo productVo = null;
        if (cursor.moveToFirst())
        {
            do {
                productVo = new ProductVo();
                productVo.setProductID(getIntFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_ID));
                productVo.setCategoryID(getIntFromColumnName(cursor, ProductVo.ProductFields.CATEGORY_ID));
                productVo.setProductName(getStringFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_NAME));
                productVo.setProductDesc(getStringFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_DESC));
                productVo.setMountain(getStringFromColumnName(cursor, ProductVo.ProductFields.MOUNTAIN));

                int productTypeId = getIntFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_ID);

                productVo.setProductTypeVo(readProductType(productTypeId));
                productVo.setCategoryVo(readCategory(productVo));
                list.add(productVo);
            }
            while (cursor.moveToNext());
        }
        return list;
    }

    public ArrayList<ProductVo> readProduct(int id)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        String queryString = "SELECT * FROM " + ProductVo.ProductFields.PRODUCT_TBL + " WHERE "
            + ProductVo.ProductFields.PRODUCT_ID + " = "+ id;

        Cursor cursor = database.rawQuery(queryString, null);

        List<ProductVo> list = processCursor(cursor);

        ArrayList<ProductVo> itemPerPriceList = new ArrayList<>();
        close(database);

        for (int i = 0; i < list.size(); i++)
        {
            ProductVo item = list.get(i);
            for (int j = 0; j < item.getProductTypeVo().getPriceArray().length; j++)
            {
                BigDecimal[] prices = item.getProductTypeVo().getPriceArray();
                if (prices[j].compareTo(BigDecimal.ZERO) > 0)
                {
                    ProductVo vo = new ProductVo();
                    vo.setProductID(item.getProductID());
                    vo.setCategoryID(item.getCategoryID());
                    vo.setPrice(prices[j]);
                    vo.setDuration(j+ 1);
                    vo.setProductTypeVo(item.getProductTypeVo());
                    String numberOfDays = (j + 1) +"";
                    vo.setProductName(item.getProductName() + " (" + item.getProductTypeVo().getProductType()+ ")");
                    if ((j+ 1) == 6)
                    {
                        vo.setProductDesc("5.5 Day Pack " + item.getProductDesc());
                    }
                    else
                    {
                        vo.setProductDesc(numberOfDays + " Day Pack " + item.getProductDesc());
                    }

                    itemPerPriceList.add(vo);

                }
            }
        }

        return itemPerPriceList;
    }

    public ProductVo getProduct(int id)
    {
        SQLiteDatabase database = open();

        ProductVo vo = null;

        String queryString = "SELECT * FROM " + ProductVo.ProductFields.PRODUCT_TBL + " WHERE "
                + ProductVo.ProductFields.PRODUCT_ID + " = "+ id;

        Cursor cursor = database.rawQuery(queryString, null);

        if (cursor.moveToFirst()) {
            do {
                vo = new ProductVo();
                vo.setProductID(getIntFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_ID));
                vo.setCategoryID(getIntFromColumnName(cursor, ProductVo.ProductFields.CATEGORY_ID));
                vo.setProductName(getStringFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_NAME));
                vo.setProductDesc(getStringFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_DESC));
                vo.setMountain(getStringFromColumnName(cursor, ProductVo.ProductFields.MOUNTAIN));

            } while (cursor.moveToNext());
        }

        return vo;
    }

    public ProductTypeVo readProductType(int productTypeId)
    {
        SQLiteDatabase database = open();

        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_TBL +
                        " WHERE " + ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_ID + "=" + productTypeId, null);

        ProductTypeVo vo = null;
        if (cursor.moveToFirst())
        {
            do {
                vo = new ProductTypeVo();
                vo.setProductID(getIntFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_ID));
                vo.setProductTypeID(getIntFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_ID));
                vo.setProductType(getStringFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRODUCT_TYPE));
                vo.setResCode(getStringFromColumnName(cursor, ProductTypeVo.ProductTypeFields.RES_CODE));

                if (getIntFromColumnName(cursor, ProductTypeVo.ProductTypeFields.IS_HIRE_PRODUCT) == 1)
                {
                    vo.setHireProduct(true);
                }
                else
                {
                    vo.setHireProduct(false);
                }

                vo.setPrice1D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_1D)));
                vo.setPrice2D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_2D)));
                vo.setPrice3D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_3D)));
                vo.setPrice4D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_4D)));
                vo.setPrice5D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_5D)));
                vo.setPrice6D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_6D)));
                vo.setPrice7D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_7D)));
                vo.setPrice8D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_8D)));
                vo.setPrice9D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_9D)));
                vo.setPrice10D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_10D)));
                vo.setPrice11D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_11D)));
                vo.setPrice12D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_12D)));
                vo.setPrice13D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_13D)));
                vo.setPrice14D(BigDecimal.valueOf(getDoubleFromColumnName(cursor, ProductTypeVo.ProductTypeFields.PRICE_14D)));

            }
            while (cursor.moveToNext());
        }
        close(database);
        return vo;
    }

    public CategoryVo readCategory(ProductVo productVo)
    {
        SQLiteDatabase database = open();

        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + CategoryVo.CategoryFields.CATEGORY_TBL +
                        " WHERE " + CategoryVo.CategoryFields.CATEGORY_ID + "=" + productVo.getCategoryID(), null);
        CategoryVo vo = null;
        if (cursor.moveToFirst()) {
            do {
                vo = new CategoryVo();
                vo.setCategoryID(getIntFromColumnName(cursor, CategoryVo.CategoryFields.CATEGORY_ID));
                vo.setCategoryName(getStringFromColumnName(cursor, CategoryVo.CategoryFields.CATEGORY_NAME));
            }
            while (cursor.moveToNext());
        }
        close(database);
        return vo;
    }


    public void createProduct(ProductVo productVo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(ProductVo.ProductFields.PRODUCT_ID, productVo.getProductID());
        cv.put(ProductVo.ProductFields.PRODUCT_NAME, productVo.getProductName());
        cv.put(ProductVo.ProductFields.PRODUCT_DESC, productVo.getProductDesc());
        cv.put(ProductVo.ProductFields.MOUNTAIN, productVo.getMountain());
        cv.put(ProductVo.ProductFields.CATEGORY_ID, productVo.getCategoryID());

        database.insert(ProductVo.ProductFields.PRODUCT_TBL, null, cv);
        database.setTransactionSuccessful();
        database.endTransaction();

        close(database);
    }


    public void createProductType(ProductTypeVo productTypeVo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_ID, productTypeVo.getProductTypeID());
        cv.put(ProductVo.ProductFields.PRODUCT_ID, productTypeVo.getProductID());
        cv.put(ProductTypeVo.ProductTypeFields.PRODUCT_TYPE, productTypeVo.getProductType());
        cv.put(ProductTypeVo.ProductTypeFields.IS_HIRE_PRODUCT, productTypeVo.isHireProduct());
        cv.put(ProductTypeVo.ProductTypeFields.RES_CODE, productTypeVo.getResCode());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_1D, productTypeVo.getPrice1D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_2D, productTypeVo.getPrice2D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_3D, productTypeVo.getPrice3D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_4D, productTypeVo.getPrice4D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_5D, productTypeVo.getPrice5D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_6D, productTypeVo.getPrice6D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_7D, productTypeVo.getPrice7D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_8D, productTypeVo.getPrice8D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_9D, productTypeVo.getPrice9D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_10D, productTypeVo.getPrice10D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_11D, productTypeVo.getPrice11D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_12D, productTypeVo.getPrice12D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_13D, productTypeVo.getPrice13D().doubleValue());
        cv.put(ProductTypeVo.ProductTypeFields.PRICE_14D, productTypeVo.getPrice14D().doubleValue());

        database.insert(ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_TBL, null, cv);

        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);
    }

    public void createCategory(CategoryVo categoryVo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(CategoryVo.CategoryFields.CATEGORY_ID, categoryVo.getCategoryID());
        cv.put(CategoryVo.CategoryFields.CATEGORY_NAME, categoryVo.getCategoryName());

        database.insert(CategoryVo.CategoryFields.CATEGORY_TBL, null, cv);

        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);
    }


}
