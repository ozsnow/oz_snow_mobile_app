package com.ozsnow.mobile.app.activities;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.adapters.OrderListAdapter;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import com.ozsnow.mobile.app.models.vos.OrderVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;
import com.ozsnow.mobile.app.models.vos.VoidOrderListVo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.ozsnow.mobile.app.OzSnowPosApplication.OZ_SNOW_PREF;


public class ViewOrdersActivity extends AppCompatActivity {

    private DatePicker startDatePicker;
    private DatePicker endDatePicker;
    private Button searchBtn;
    private RecyclerView recyclerView;
    private OrderListAdapter adapter;
    private TourGuideDataSource guideDataSource;
    private OrderDataSource orderDataSource;
    private CustomerDataSource customerDataSource;
    protected ArrayList<VoidOrderListVo> listdata;
    private int terminalId ;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_orders);

        SharedPreferences settings = getSharedPreferences(OZ_SNOW_PREF, MODE_PRIVATE);
        terminalId = settings.getInt(PreferenceList.TERMINAL_ID.toString(), -1);

        listdata = new ArrayList<>();

        startDatePicker = (DatePicker) findViewById(R.id.order_search_start_date_spinner);
        endDatePicker = (DatePicker) findViewById(R.id.order_search_end_date_spinner);
        searchBtn = (Button) findViewById(R.id.order_search_btn);
        recyclerView = (RecyclerView) findViewById(R.id.order_resutls_recycler_view);

        adapter = new OrderListAdapter(this, listdata);

        recyclerView.setAdapter(adapter);

        searchBtn.setOnClickListener(searchListener);
    }

    View.OnClickListener searchListener = new View.OnClickListener() {
        @Override
        public void onClick(View v)
        {

            Calendar start = Calendar.getInstance();
            start.set(Calendar.YEAR, startDatePicker.getYear());
            start.set(Calendar.MONTH, startDatePicker.getMonth());
            start.set(Calendar.DAY_OF_MONTH, startDatePicker.getDayOfMonth());
            start.set(Calendar.MINUTE, 0);
            start.set(Calendar.HOUR, 0);
            start.set(Calendar.SECOND, 0);
            start.set(Calendar.MILLISECOND, 0);
            start.set(Calendar.AM_PM, Calendar.AM);

            Calendar end = Calendar.getInstance();
            end.set(Calendar.YEAR, endDatePicker.getYear());
            end.set(Calendar.MONTH, endDatePicker.getMonth());
            end.set(Calendar.DAY_OF_MONTH, endDatePicker.getDayOfMonth());
            end.set(Calendar.MINUTE, 59);
            end.set(Calendar.HOUR, 11);
            end.set(Calendar.SECOND, 59);
            end.set(Calendar.MILLISECOND, 0);
            end.set(Calendar.AM_PM, Calendar.PM);


            if (start.getTime().getTime() > end.getTime().getTime())
            {
                //Start time cannot set after the end date search
                Toast.makeText(ViewOrdersActivity.this, "Start Date cannot be set after End Date.", Toast.LENGTH_LONG).show();
                return;
            }

            if (end.getTime().getTime() < start.getTime().getTime())
            {
                //End time cannot be set before the start time
                Toast.makeText(ViewOrdersActivity.this, "End Date cannot be set before Start Date.", Toast.LENGTH_LONG).show();
                return;
            }

            if (end.getTime().getTime() == start.getTime().getTime())
            {
                //start date and end date cannot be equal
                Toast.makeText(ViewOrdersActivity.this, "End Date and Start Date cannot be equal", Toast.LENGTH_LONG).show();
                return;
            }

            listdata = new ArrayList<>();
            adapter.notifyDataSetChanged();

            listOrders(start.getTime(), end.getTime());

        }
    };


    private void listOrders(final Date startDate, final Date endDate)
    {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run()
            {

                customerDataSource = new CustomerDataSource(ViewOrdersActivity.this);
                guideDataSource = new TourGuideDataSource(ViewOrdersActivity.this);
                orderDataSource = new OrderDataSource(ViewOrdersActivity.this);

                ArrayList<OrderVo> orderVos = orderDataSource.readOrdersByDate(startDate, endDate);

                for (int i = 0; i < orderVos.size(); i++)
                {
                    CustomerVo customer = customerDataSource.readCustomer(orderVos.get(i).getCustomerId());
                    TourGuideVo guide = guideDataSource.readTourGuide(orderVos.get(i).getGuideId());
                    VoidOrderListVo vo = new VoidOrderListVo();
                    vo.setCustomerVo(customer);
                    vo.setOrderVo(orderVos.get(i));
                    vo.setGuideVo(guide);
                    vo.setTerminalId(terminalId);
                    listdata.add(vo);
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.setList(listdata);
                        adapter.notifyDataSetChanged();
                    }
                });

            }
        });
        thread.setName("List Orders");
        thread.start();
    }
}
