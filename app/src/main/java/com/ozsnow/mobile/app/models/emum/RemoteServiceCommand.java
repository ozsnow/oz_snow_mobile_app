package com.ozsnow.mobile.app.models.emum;

import java.io.Serializable;

/**
 * Created by russellmilburn on 06/03/2016.
 */
public enum RemoteServiceCommand implements Serializable
{
    GET_TERMINAL_ID("/service/terminal"),
    CHECK_PRODUCTS_UPDATED("/service/products/checkProductsUpdated"),
    GET_ALL_PRODUCTS("/service/products/getAllProducts");


    private String commandPath;

    private RemoteServiceCommand(String commandPath)
    {
        this.commandPath = commandPath;
    }

    public String getCommandPath() {
        return commandPath;
    }
}
