package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.models.vos.OrderVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class CreateOrderTask extends AsyncTask<OrderVo, Void, OrderVo>
{
    private OrderDataSource dataSource;

    public CreateOrderTask(OrderDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected OrderVo doInBackground(OrderVo... orderVos)
    {
        return dataSource.createOrder(orderVos[0]);
    }


}
