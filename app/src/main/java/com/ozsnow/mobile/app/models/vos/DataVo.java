package com.ozsnow.mobile.app.models.vos;

import org.json.JSONObject;

/**
 * Created by russellmilburn on 29/02/2016.
 */
public class DataVo
{
    private JSONObject jsonObject;

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }
}
