package com.ozsnow.mobile.app.services.terminal;

import android.os.Looper;

/**
 * Created by russellmilburn on 25/03/2016.
 */
public class TerminalDataThread extends Thread
{
    private TerminalDataHandler terminalDataHandler;

    @Override
    public void run()
    {
        super.run();
        Looper.prepare();
        terminalDataHandler = new TerminalDataHandler();
        Looper.loop();

    }

    public TerminalDataHandler getTerminalDataHandler() {
        return terminalDataHandler;
    }

    public void setTerminalDataHandler(TerminalDataHandler terminalDataHandler) {
        this.terminalDataHandler = terminalDataHandler;
    }
}
