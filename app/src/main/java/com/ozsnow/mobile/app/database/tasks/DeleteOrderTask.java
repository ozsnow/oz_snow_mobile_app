package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.models.vos.OrderVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class DeleteOrderTask extends AsyncTask<Integer, Void, Void>
{
    private OrderDataSource dataSource;

    public DeleteOrderTask(OrderDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected Void doInBackground(Integer... ids)
    {
        dataSource.deleteOrder(ids[0]);
        return null;
    }


}
