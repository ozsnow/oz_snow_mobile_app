package com.ozsnow.mobile.app.activities;



import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.adapters.ProductSelectedListAdapter;
import com.ozsnow.mobile.app.activities.fragments.OrderMenuFragment;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.database.tasks.*;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import com.ozsnow.mobile.app.models.vos.OrderVo;
import com.ozsnow.mobile.app.models.vos.ProductVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ExecutionException;

/**
 * Created by russellmilburn on 14/04/2016.
 */
public class OrderDetailsActivity extends AppCompatActivity
{
    public static final String TAG = OrderDetailsActivity.class.toString();

    public static final String ORDER_MENU_FRAGMENT = "orderMenuFragment";
    public static final String ORDER_TOTAL_EXTRA = "orderTotalExtra";
    public static final String CUSTOMER_EXTRA = "customerExtra";
    public static final String GUIDE_EXTRA = "guideExtra";

    private ProductSelectedListAdapter selectedProductsAdapter;
    private RecyclerView recyclerView;
    private TextView orderTotalField;
    private TextView customerNameField;
    private TextView destinationField;
    private TextView dateField;
    private TextView guideField;

    private CustomerDataSource customerDataSource;
    private TourGuideDataSource guideDataSource;
    private OrderDataSource orderDataSource;

    private CustomerVo currentCustomer;
    private TourGuideVo currentGuide;
    private OrderVo currentOrder;
    private BigDecimal orderTotal;



    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        customerDataSource = new CustomerDataSource(this);
        guideDataSource = new TourGuideDataSource(this);
        orderDataSource = new OrderDataSource(this);

        recyclerView = (RecyclerView) findViewById(R.id.selectedProductListRecyclerView);
        orderTotalField = (TextView) findViewById(R.id.orderTotalTextView);
        selectedProductsAdapter = new ProductSelectedListAdapter(this, new ArrayList<ProductVo>());
        selectedProductsAdapter.registerAdapterDataObserver(changeObserver);
        recyclerView.setAdapter(selectedProductsAdapter);

        customerNameField = (TextView) findViewById(R.id.order_customer_text_view);
        destinationField = (TextView) findViewById(R.id.destination_text_field);
        dateField = (TextView) findViewById(R.id.order_date_text_field);
        guideField = (TextView) findViewById(R.id.order_tour_guide_text_view);


        OrderMenuFragment fragment = (OrderMenuFragment) getSupportFragmentManager().findFragmentByTag(ORDER_MENU_FRAGMENT);

        if (fragment == null)
        {
            OrderMenuFragment orderMenuFragment = new OrderMenuFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.orderMenuPlaceHolder, orderMenuFragment, ORDER_MENU_FRAGMENT);
            fragmentTransaction.commit();
        }

        retrieveGuide();
        retrieveCustomer();
        retrieveDestination();

        createOrder();

    }


    private void createOrder()
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        int orderId = settings.getInt(PreferenceList.CURRENT_ORDER_ID.toString(), -1);

        if (orderId == -1)
        {
            calcTotalOrder();
            //Create New Order;
            OrderVo order = new OrderVo();
            order.setCustomerId(currentCustomer.getCustomerID());
            order.setGuideId(currentGuide.getGuideId());
            Log.i(TAG, "createOrder");
            order.setOrderDate(currentGuide.getDate());
            order.setOrderTotal(getOrderTotal());

            try {
                currentOrder = new CreateOrderTask(orderDataSource).execute(order).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }

//            SharedPreferences.Editor editor = settings.edit();
//            editor.putInt(PreferenceList.CURRENT_ORDER_ID.toString(), order.getOrderId());
//            editor.commit();

        }
        else
        {
            Log.e(TAG, "Old Order found");
            SharedPreferences.Editor editor = settings.edit();
            editor.remove(PreferenceList.CURRENT_ORDER_ID.toString());
            editor.commit();
        }

    }

    public void cancelOrder()
    {
        Log.i(TAG, currentOrder.getOrderId()+"");

        DeleteCustomerTask deleteCustomerTask =  new DeleteCustomerTask(customerDataSource);
        deleteCustomerTask.execute(currentCustomer.getCustomerID());

        DeleteOrderTask deleteOrderTask = new DeleteOrderTask(orderDataSource);
        deleteOrderTask.execute(currentOrder.getOrderId());

        Toast.makeText(OrderDetailsActivity.this, "The order has been cancelled" , Toast.LENGTH_LONG).show();

        Intent intent = new Intent(OrderDetailsActivity.this, MainMenuActivity.class);
        intent.setAction(MainMenuActivity.RESET_ORDER);
        startActivity(intent);

    }

    public void saveOrderToDatabase()
    {
        if (currentOrder != null)
        {
            if (selectedProductsAdapter.getSelectedProducts().size() != 0)
            {
                currentOrder.setSelectedProducts((ArrayList<ProductVo>) selectedProductsAdapter.getSelectedProducts());
                currentOrder.setOrderTotal(getOrderTotal());

                SaveOrderTask task = new SaveOrderTask(orderDataSource);
                task.execute(currentOrder);

                Intent intent = new Intent(OrderDetailsActivity.this, NumberKeypadActivity.class);
                intent.putExtra(ORDER_TOTAL_EXTRA, currentOrder);
                intent.putExtra(CUSTOMER_EXTRA, currentCustomer);
                intent.putExtra(GUIDE_EXTRA, currentGuide);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(this, "You have not added any products to the order.", Toast.LENGTH_SHORT).show();
            }
        }
    }


    private void retrieveGuide()
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        int tourGuideId = settings.getInt(PreferenceList.CURRENT_TOUR_GUIDE.toString(), -1);

        if (tourGuideId != -1)
        {
            TourGuideDataSource dataSource = new TourGuideDataSource(this);
            ReadCurrentTourGuidesTask task = new ReadCurrentTourGuidesTask(dataSource);
            TourGuideVo guide = null;

            try {
                guide = task.execute(tourGuideId).get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            currentGuide = guide;
            Log.i(TAG, "retrieveGuide");

            guideField.setText(guide.getGuideName());
            setDateOnView(new Date(currentGuide.getDate()));

        }
    }

    protected void setDateOnView(Date date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy");
        String formatted = sdf.format(date);
        dateField.setText(formatted);
    }

    private void retrieveDestination()
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        String destination = settings.getString(PreferenceList.DESTINATION.toString(), "thredbo");
        destinationField.setText(destination);
    }


    private void retrieveCustomer()
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        int customerId = settings.getInt(PreferenceList.CURRENT_CUSTOMER.toString(), -1);

        ReadCustomerTask task = new ReadCustomerTask(customerDataSource);
        try {
            currentCustomer = task.execute(customerId).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        if (currentCustomer != null)
        {
            customerNameField.setText(currentCustomer.getFirstName() + " " + currentCustomer.getLastName());
        }


    }

    RecyclerView.AdapterDataObserver changeObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            super.onChanged();
            calcTotalOrder();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

//        Log.i(TAG,  "onActivityResult: requestCode: " + requestCode);
//        Log.i(TAG,  "onActivityResult: resultCode: " + resultCode);

        switch (requestCode)
        {
            case ProductListActivity.SELECT_PRODUCT_REQUEST:

//                Log.i(TAG,  "ProductListActivity.SELECT_PRODUCT_REQUEST");
                if (resultCode == RESULT_OK)
                {
//                    Log.i(TAG,  "RESULT_OK");
                    if (data.hasExtra(ProductListActivity.SELECTED_PRODUCT_EXTRA))
                    {
//                        Log.i(TAG,  "ProductListActivity.SELECTED_PRODUCT_EXTRA");
                        ProductVo vo = (ProductVo) data.getSerializableExtra(ProductListActivity.SELECTED_PRODUCT_EXTRA);
                        selectedProductsAdapter.add(vo);
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed()
    {
        DeleteOrderTask deleteOrderTask = new DeleteOrderTask(orderDataSource);
        deleteOrderTask.execute(currentOrder.getOrderId());
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    public void calcTotalOrder()
    {
//        Log.i(TAG, "Calc Order");
        BigDecimal total = new BigDecimal(0.00);

        for (int i = 0; i < selectedProductsAdapter.getSelectedProducts().size(); i++)
        {
            total = total.add(selectedProductsAdapter.getSelectedProducts().get(i).getPrice());
        }

        orderTotalField.setText("Order Total:\t " + NumberFormat.getCurrencyInstance().format(total));

        setOrderTotal(total);

        Log.i(TAG, "Total: " + total.toString());

    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }


}