package com.ozsnow.mobile.app;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import com.ozsnow.mobile.app.database.OzSnowDatabaseHelper;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.emum.RemoteServiceCommand;
import com.ozsnow.mobile.app.services.terminal.TerminalDataService;

/**
 * Created by russellmilburn on 01/03/2016.
 */
public class OzSnowPosApplication extends Application
{
    public static final String TAG = OzSnowPosApplication.class.toString();

    public static final String OZ_SNOW_PREF = TAG + "preferences";

    private static OzSnowPosApplication singleton;

    private OzSnowDatabaseHelper helper;

    @Override
    public void onCreate()
    {
        super.onCreate();
        SharedPreferences settings = getSharedPreferences(OZ_SNOW_PREF, MODE_PRIVATE);

        Log.i(TAG, PreferenceList.TERMINAL_ID.toString());

        //helper = new OzSnowDatabaseHelper(getApplicationContext());

        if (settings.getInt(PreferenceList.TERMINAL_ID.toString(), -1) == -1)
        {
            Intent startIntent = new Intent();
            startIntent.setClass(OzSnowPosApplication.this, TerminalDataService.class);
            startIntent.putExtra(RemoteServiceCommand.GET_TERMINAL_ID.name(), RemoteServiceCommand.GET_TERMINAL_ID);
            startService(startIntent);
        }

        //PreferenceManager.getDefaultSharedPreferences()




//        Intent productServiceIntent = new Intent();
//        productServiceIntent.setClass(this, ProductDownloadService.class);
//        productServiceIntent.putExtra(RemoteServiceCommand.CHECK_PRODUCTS_UPDATED.name(), RemoteServiceCommand.CHECK_PRODUCTS_UPDATED);
//        startService(productServiceIntent);



//        Intent intent = new Intent(OzSnowPosApplication.this, PrinterIntentService.class);
//        startService(intent);
        Log.i(TAG, "onCreate() " + settings.getInt(PreferenceList.TERMINAL_ID.toString(), -1));

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        Log.i(TAG, "onTerminate");
    }


}
