package com.ozsnow.mobile.app.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.*;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.database.tasks.CreateCustomerTask;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.CustomerVo;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class CustomerFormActivity extends AppCompatActivity
{
    public static final String TAG = CustomerFormActivity.class.toString();

    private EditText firstNameField;
    private TextInputLayout inputLayoutFName;
    private TextInputLayout inputLayoutLName;
    private EditText lastNameField;
    private Button nextBtn;
    private CustomerDataSource customerDataSource;
    private RadioButton thredboRadioButton;
    private RadioButton perisherRadioButton;
    private String destination;
    private boolean hasSaved = false;
    private TextView legalField;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_form);

        customerDataSource = new CustomerDataSource(this);

        nextBtn = (Button) findViewById(R.id.customerNextBtn);

        firstNameField = (EditText) findViewById(R.id.custFirstNameField);
        lastNameField = (EditText) findViewById(R.id.custLastNameField);

        legalField = (TextView) findViewById(R.id.customer_legal_text);
        legalField.setMovementMethod(new ScrollingMovementMethod());

        firstNameField.addTextChangedListener(new MyTextWatcher(firstNameField));
        lastNameField.addTextChangedListener(new MyTextWatcher(lastNameField));

        inputLayoutFName = (TextInputLayout) findViewById(R.id.input_layout_fname);
        inputLayoutLName = (TextInputLayout) findViewById(R.id.input_layout_lname);

        thredboRadioButton = (RadioButton) findViewById(R.id.thredboRadioBtn);
        perisherRadioButton = (RadioButton) findViewById(R.id.perisherRadioBtn);

        setDestination(thredboRadioButton.getText().toString());

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(!validateName())
                {
                    return;
                }

                if (!hasSaved)
                {
                    CustomerVo customerVo = new CustomerVo();
                    customerVo.setFirstName(firstNameField.getText().toString());
                    customerVo.setLastName(lastNameField.getText().toString());

                    saveToCurrentCustomer(customerVo);
                    saveDestination();

                    Intent intent = new Intent(CustomerFormActivity.this, OrderDetailsActivity.class);
                    startActivity(intent);
                }
                else
                {
                    saveDestination();

                    Intent intent = new Intent(CustomerFormActivity.this, OrderDetailsActivity.class);
                    startActivity(intent);
                }
            }
        });

        Log.i(TAG, "has Saved: " + hasSaved);

        thredboRadioButton.setOnClickListener(radioButtonListener);
        perisherRadioButton.setOnClickListener(radioButtonListener);
    }

    private View.OnClickListener radioButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean checked = ((RadioButton) view).isChecked();

            // Check which radio button was clicked
            switch(view.getId()) {
                case R.id.thredboRadioBtn:
                    if (checked)
                        setDestination(((RadioButton) view).getText().toString());
                        break;
                case R.id.perisherRadioBtn:
                    if (checked)
                        setDestination(((RadioButton) view).getText().toString());
                        break;
            }
        }
    };


    private void setDestination(String value)
    {
        destination = value;
    }

    private void saveToCurrentCustomer(CustomerVo vo)
    {

        CreateCustomerTask task = new CreateCustomerTask(customerDataSource);
        Integer customerId = null;
        try {
            customerId = task.execute(vo).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        vo.setCustomerID(customerId);

        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(PreferenceList.CURRENT_CUSTOMER.toString(), vo.getCustomerID());
        editor.commit();

        hasSaved = true;
    }

    private void saveDestination()
    {
        SharedPreferences settings = getSharedPreferences(OzSnowPosApplication.OZ_SNOW_PREF, MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PreferenceList.DESTINATION.toString(), destination);
        editor.commit();
    }



    private boolean validateName()
    {
        if (firstNameField.getText().toString().trim().isEmpty())
        {
            inputLayoutFName.setError(getString(R.string.customer_fn_err_msg));
            return false;
        }
        else
        {
            inputLayoutFName.setError("");
        }


        if (lastNameField.getText().toString().trim().isEmpty())
        {
            inputLayoutLName.setError(getString(R.string.customer_ln_err_msg));
            return false;
        }
        else
        {
            inputLayoutLName.setError("");
        }

        return true;
    }

    private void requestFocus(View view)
    {
        if (view.requestFocus())
        {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher
    {
        private View view;

        public MyTextWatcher(View view)
        {
            this.view = view;
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s)
        {
            switch (view.getId())
            {
                case R.id.custFirstNameField:
                    validateName();
                    break;
                case R.id.custLastNameField:
                    validateName();
                    break;
            }

        }
    };
}