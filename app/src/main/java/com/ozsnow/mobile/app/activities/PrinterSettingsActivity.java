package com.ozsnow.mobile.app.activities;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.ozsnow.mobile.app.OzSnowPosApplication;
import com.ozsnow.mobile.app.services.printer.ThermalPrinterService;
import com.ozsnow.mobile.app.utils.PrinterFormatUtils;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.adapters.DeviceListAdapter;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.database.tasks.ReadCurrentTourGuidesTask;
import com.ozsnow.mobile.app.database.tasks.ReadCustomerTask;
import com.ozsnow.mobile.app.database.tasks.ReadOrderTask;
import com.ozsnow.mobile.app.models.emum.PreferenceList;
import com.ozsnow.mobile.app.models.vos.*;
import com.ozsnow.mobile.app.services.printer.ConnectionHandler;
import com.zj.btsdk.BluetoothService;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.ozsnow.mobile.app.OzSnowPosApplication.OZ_SNOW_PREF;

public class PrinterSettingsActivity extends AppCompatActivity implements ThermalPrinterService.ConnectionListener
{
    public static final String SCAN_REQUEST = "scanRequest";
    public static final String CONNECT_REQUEST = "connectRequest";
    public static final String PRINT_REQUEST = "printRequest";
    public static final String PRINT_IMAGE_REQUEST = "printImageRequest";


    public static final String DEVICE_EXTRA = "deviceExtra";
    public static final String PRINT_ORDER_EXTRA = "printOrderExtra";
    public static final String PRINT_IMAGE_EXTRA = "printImageExtra";

    public static final int REQUEST_ENABLE_BT = 2;
    public static final int CONNECT_TO_DEVICE_REQUEST = 1;

    public static final String TAG = PrinterSettingsActivity.class.toString();

    private Button scanBtn;
    private DeviceListAdapter adapter;
    private RecyclerView recyclerView;
    private boolean isConnected;
    private boolean isBound;


    private ThermalPrinterService printerService;


    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder binder)
        {
            Log.i(TAG, "onServiceConnected() ");
            isBound = true;
            ThermalPrinterService.LocalBinder localBinder = (ThermalPrinterService.LocalBinder) binder;
            printerService = localBinder.getService();
            printerService.setListener(PrinterSettingsActivity.this);

            checkBluetoothOpen();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isBound = false;
        }
    };

    private void checkBluetoothOpen()
    {
        if (printerService.isOpen() == false)
        {
            Log.i(TAG, "Bluetooth is not open");
            updateTitle("Please allow bluetooth");
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_printer_settings);

        setTitle("No printer found scan for printers");

        adapter = new DeviceListAdapter(this, new ArrayList<DeviceVo>());

        scanBtn = (Button) findViewById(R.id.scan_btn);

        scanBtn.setOnClickListener(scanClickListener);
        scanBtn.setEnabled(false);
        scanBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.cardBackground));


        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(mReceiver, filter);



        recyclerView = (RecyclerView) findViewById(R.id.printer_recycler_view);
        recyclerView.setAdapter(adapter);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();

            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                switch (state) {
                    case BluetoothAdapter.STATE_OFF:
                        scanBtn.setEnabled(false);
                        scanBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.cardBackground));
                        adapter.removeAll();
                        break;
                    case BluetoothAdapter.STATE_ON:
                        scanBtn.setEnabled(true);
                        scanBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.primary_dark));
                        break;

                }
            }
        }
    };


    @Override
    protected void onStart()
    {
        super.onStart();
        Log.i(TAG, "onStart");
        Intent intent = new Intent(PrinterSettingsActivity.this, ThermalPrinterService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mReceiver);
        if (isBound)
        {
            unbindService(connection);
            isBound = false;
        }
    }

    View.OnClickListener scanClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            updateTitle("Scanning for devices...");
            Intent intent = new Intent(PrinterSettingsActivity.this, ThermalPrinterService.class);
            intent.setAction(SCAN_REQUEST);
            startService(intent);
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQUEST_ENABLE_BT:
                if (resultCode == RESULT_OK)
                {
                    //TODO: Display Bluetooth open
                    updateTitle("Bluetooth in now on, please scan for printer");
                    scanBtn.setEnabled(true);
                    scanBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.primary_dark));

                }
                else
                {
                    scanBtn.setEnabled(false);
                    scanBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.cardBackground));
                }
                break;
            case CONNECT_TO_DEVICE_REQUEST:
        }

    }




    public void updateTitle(String value)
    {
        setTitle(value);
    }

    public void connectToDevice(DeviceVo vo)
    {
        adapter.getDevices().indexOf(vo);

        if (isConnected)
        {
            return;
        }

        Intent intent = new Intent(PrinterSettingsActivity.this, ThermalPrinterService.class);
        intent.setAction(CONNECT_REQUEST);
        intent.putExtra(DEVICE_EXTRA, vo);
        startService(intent);

    }


    @Override
    public void onConnectToService() {
        isConnected = printerService.isConnected();
        if (isConnected)
        {
            scanBtn.setEnabled(false);
            scanBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.cardBackground));
        }
        else
        {
            scanBtn.setEnabled(true);
            scanBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.primary_dark));
        }
    }

    @Override
    public void onConnectionStatus(String action)
    {
        if (ConnectionHandler.DEVICE_CONNECTED.equals(action))
        {
            Log.i(TAG, "BluetoothServiceHandler.DEVICE_CONNECTED");
            updateTitle("Connected to device");
            isConnected = true;

            scanBtn.setEnabled(false);
            scanBtn.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.cardBackground));
        }
        else if (ConnectionHandler.DEVICE_CONNECTING.equals(action))
        {
            updateTitle("Connecting to device...");
        }
        else if (ConnectionHandler.DEVICE_CONNECTION_LOST.equals(action))
        {
            updateTitle("Connecting lost");
            isConnected = false;
        }
    }

    @Override
    public void onDeviceFound(DeviceVo vo)
    {
        updateTitle("Device Found: " +vo.getDeviceName());
        adapter.add(vo);
    }

    @Override
    public void onScanningStatus(String action)
    {
        updateTitle("Scan Complete, Select Device to continue");
    }
}
