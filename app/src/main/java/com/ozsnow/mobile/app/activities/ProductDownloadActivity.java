package com.ozsnow.mobile.app.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.*;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.models.emum.RemoteServiceCommand;
import com.ozsnow.mobile.app.services.product.ProductDownloadService;

/**
 * Created by russellmilburn on 15/03/2016.
 */
public class ProductDownloadActivity extends Activity
{
    public static final String TAG = ProductDownloadActivity.class.toString();

    public static final String ON_LOAD_PROGRESS = "onLoadProgress";


    private ProductDownloadService productDownloadService;
    private ProgressDialog progressDialog;
    private boolean bound = false;


    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder binder)
        {
            Log.i(TAG, "onServiceConnected() ");
            bound = true;
            ProductDownloadService.LocalBinder localBinder = (ProductDownloadService.LocalBinder) binder;
            productDownloadService = localBinder.getService();

        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            bound = false;
        }
    };

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            progressDialog.setProgress(productDownloadService.getLoadProgress());

            if (productDownloadService.getLoadProgress() == 100)
            {
                openMainMenu();
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_download);

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, new IntentFilter(ON_LOAD_PROGRESS));

        Log.i(TAG, "onCreate() ");

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Downloading Products....");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgress(0);
        progressDialog.setMax(100);
        progressDialog.show();

        Intent intent = new Intent(this, ProductDownloadService.class);
        //intent.putExtra(RemoteServiceCommand.CHECK_PRODUCTS_UPDATED.name(), RemoteServiceCommand.CHECK_PRODUCTS_UPDATED);
        intent.putExtra(RemoteServiceCommand.GET_ALL_PRODUCTS.name(), RemoteServiceCommand.GET_ALL_PRODUCTS);
        startService(intent);
    }

    public void openMainMenu()
    {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }



    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart()");
        Intent intent = new Intent(ProductDownloadActivity.this, ProductDownloadService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);

    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStart() ");
        if (bound)
        {
            unbindService(connection);
            bound = false;
        }
    }
}