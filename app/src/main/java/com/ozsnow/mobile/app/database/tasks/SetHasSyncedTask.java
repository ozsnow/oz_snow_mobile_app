package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.database.IDataSource;
import com.ozsnow.mobile.app.models.vos.CustomerVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class SetHasSyncedTask extends AsyncTask<Integer, Void, Void>
{
    private IDataSource dataSource;

    public SetHasSyncedTask(IDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected Void doInBackground(Integer... ids)
    {
        dataSource.setHasRecordSynced(ids[0], true);
        return null;
    }

}
