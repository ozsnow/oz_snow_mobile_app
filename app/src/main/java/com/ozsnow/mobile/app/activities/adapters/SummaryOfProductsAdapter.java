package com.ozsnow.mobile.app.activities.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.models.vos.OrderDetailsVo;
import com.ozsnow.mobile.app.models.vos.ProductVo;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by russellmilburn on 29/05/2016.
 */
public class SummaryOfProductsAdapter extends RecyclerView.Adapter<SummaryOfProductsAdapter.ProductDisplayViewHolder>
{
    private Context context;
    private List<OrderDetailsVo> selectedItems;

    public SummaryOfProductsAdapter(Context context, List<OrderDetailsVo> items) {
        this.context = context;
        selectedItems = items;
    }

    @Override
    public SummaryOfProductsAdapter.ProductDisplayViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_product_item, parent, false);
        return new ProductDisplayViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductDisplayViewHolder viewHolder, int position) {
        viewHolder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return selectedItems.size();
    }

    public class ProductDisplayViewHolder extends RecyclerView.ViewHolder
    {
        private int position;

        private TextView productNameTextView;
        private TextView productDescTextView;
        protected ImageButton removeItemBtn;
        private TextView priceTextView;

        public ProductDisplayViewHolder(View itemView)
        {
            super(itemView);
            productNameTextView = (TextView) itemView.findViewById(R.id.selectedProductItemNameTextView);
            productDescTextView = (TextView) itemView.findViewById(R.id.selectedProductItemDescTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.selectedProductItemPriceTextView);

            removeItemBtn = (ImageButton) itemView.findViewById(R.id.removeProductBtn);
        }

        public void bindView(int position)
        {
            this.position = position;
            OrderDetailsVo vo = selectedItems.get(position);

//            Log.i(TAG, "bindView" + vo.toString());

            productNameTextView.setText(vo.getProductName());
            productDescTextView.setText(vo.getProductDesc());
            priceTextView.setText(NumberFormat.getCurrencyInstance().format(vo.getPrice()));
            removeItemBtn.setVisibility(View.INVISIBLE);

        }
    }

}
