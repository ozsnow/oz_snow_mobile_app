package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.CustomerDataSource;
import com.ozsnow.mobile.app.models.vos.CustomerVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class DeleteCustomerTask extends AsyncTask<Integer, Void, Void>
{
    private CustomerDataSource dataSource;

    public DeleteCustomerTask(CustomerDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected Void doInBackground(Integer... ids)
    {
        dataSource.deleteCustomer(ids[0]);
        return null;
    }

}
