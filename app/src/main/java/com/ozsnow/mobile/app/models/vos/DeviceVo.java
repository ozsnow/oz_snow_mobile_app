package com.ozsnow.mobile.app.models.vos;

import java.io.Serializable;

/**
 * Created by russellmilburn on 01/05/2016.
 */
public class DeviceVo implements Serializable
{
    private String deviceName;
    private String macAddress;
    private boolean isBonded;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public boolean isBonded() {
        return isBonded;
    }

    public void setBonded(boolean bonded) {
        isBonded = bonded;
    }
}
