package com.ozsnow.mobile.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.adapters.GuideListAdapter;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.database.tasks.ReadAllTourGuidesTask;
import com.ozsnow.mobile.app.models.vos.ProductVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

public class GuideListActivity extends AppCompatActivity
{
    protected RecyclerView recyclerView;
    private GuideListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_list);

        TourGuideDataSource dataSource = new TourGuideDataSource(getBaseContext());

        ReadAllTourGuidesTask task = new ReadAllTourGuidesTask(dataSource);

        ArrayList<TourGuideVo> list = null;

        try {
            list = task.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        adapter = new GuideListAdapter(this, list);

        recyclerView = (RecyclerView) findViewById(R.id.tourGuideRecyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getBaseContext());
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.setAdapter(adapter);

    }

    public void tourGuideSelected(TourGuideVo tourGuideVo)
    {

        Intent resultIntent = new Intent();
        resultIntent.putExtra(TourGuideFormActivity.TOUR_GUIDE_EXTRA, tourGuideVo);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
}
