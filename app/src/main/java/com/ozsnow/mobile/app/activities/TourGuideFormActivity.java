package com.ozsnow.mobile.app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.fragments.DataPickerDialog;
import com.ozsnow.mobile.app.database.TourGuideDataSource;
import com.ozsnow.mobile.app.database.tasks.CreateTourGuideTask;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

/**
 * Created by russellmilburn on 07/04/2016.
 */
public class TourGuideFormActivity extends AppCompatActivity
{
    public static final String TAG = TourGuideFormActivity.class.toString();

    public static final String TOUR_GUIDE_EXTRA = "tourGuideExtra";
    private static String DATE_PICKER_DIALOG = "datePickerDialog";

    private EditText firstNameIp;
    private TextView dateTextView;
    private Button changeDateBtn;
    private Button saveBtn;
    private Button cancelBtn;
    private TourGuideDataSource dataSource;
    private Calendar datePicked;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_form);

        dataSource = new TourGuideDataSource(this);

        firstNameIp = (EditText) findViewById(R.id.tourGuideNameEditText);
        dateTextView = (TextView) findViewById(R.id.tourDateEditText);

        changeDateBtn = (Button) findViewById(R.id.changeDateBtn);
        saveBtn = (Button) findViewById(R.id.saveGuideBtn);
        cancelBtn = (Button) findViewById(R.id.cancelGuideBtn);

        changeDateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                showDatePickerDialog(view);
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                saveTourGuide();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnToMainMenu();
            }
        });

        getFragmentManager();

        final Calendar c = Calendar.getInstance();
        datePicked = c;
        setDateOnView(c);
    }




    public void showDatePickerDialog(View view)
    {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        DataPickerDialog df = new DataPickerDialog();

        Bundle bundle = new Bundle();
        df.setArguments(bundle);
        df.show(ft, DATE_PICKER_DIALOG);
    }


    protected void setDateOnView(Calendar cal)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-yyyy");
        String formatted = sdf.format(cal.getTime());
        dateTextView.setText(formatted);
    }

    private void saveTourGuide()
    {
        TourGuideVo vo = new TourGuideVo();
        vo.setGuideName(firstNameIp.getText().toString());
        vo.setDate(getDatePicked().getTime().getTime());

        Log.i(TAG, vo.toString());

        int guideId = -1;

        CreateTourGuideTask createTourGuide = new CreateTourGuideTask(dataSource);
        try {
            guideId = createTourGuide.execute(vo).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        vo.setGuideId(guideId);

        Log.i(TAG, vo.toString());

        Intent resultIntent = new Intent();
        resultIntent.putExtra(TOUR_GUIDE_EXTRA, vo);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    private void returnToMainMenu()
    {
        Intent resultIntent = new Intent();
        setResult(RESULT_CANCELED, resultIntent);
        finish();
    }

    public Calendar getDatePicked() {
        return datePicked;
    }

    public void setDatePicked(Calendar datePicked) {
        this.datePicked = datePicked;
        setDateOnView(this.datePicked);
    }
}