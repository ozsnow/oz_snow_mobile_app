package com.ozsnow.mobile.app.services.sync;

import android.os.Looper;
import com.ozsnow.mobile.app.services.terminal.TerminalDataHandler;
import com.ozsnow.mobile.app.services.terminal.TerminalDataService;

/**
 * Created by russellmilburn on 15/05/2016.
 */
public class SyncDataThread extends Thread
{
    protected static final String TAG = SyncDataThread.class.toString();
    private SyncDataHandler handler;

    @Override
    public void run() {
        super.run();
        Looper.prepare();
        handler = new SyncDataHandler();
        Looper.loop();
    }

    public SyncDataHandler getHandler() {
        return handler;
    }

    public void setHandler(SyncDataHandler handler) {
        this.handler = handler;
    }
}
