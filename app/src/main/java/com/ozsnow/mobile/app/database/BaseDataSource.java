package com.ozsnow.mobile.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.ozsnow.mobile.app.models.vos.OrderVo;

/**
 * Created by russellmilburn on 04/04/2016.
 */
public class BaseDataSource
{
    private Context mContext;
    private OzSnowDatabaseHelper databaseHelper;

    public BaseDataSource(Context context)
    {
        mContext = context;
        databaseHelper = new OzSnowDatabaseHelper(mContext);





    }

    protected void resetHasSynced(String tableName)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues updateValues = new ContentValues();
        updateValues.put("hasSynced", false);

        database.update(tableName,
                updateValues,
                null,
                null);


        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);
    }

    protected SQLiteDatabase open()
    {
        return databaseHelper.getWritableDatabase();
    }

    protected void close(SQLiteDatabase database)
    {
        database.close();
    }

    protected int getIntFromColumnName(Cursor cursor, String columnName )
    {
        int columnIndex = cursor.getColumnIndex(columnName);
        return cursor.getInt(columnIndex);
    }

    protected String getStringFromColumnName(Cursor cursor, String columnName )
    {
        int columnIndex = cursor.getColumnIndex(columnName);
        return cursor.getString(columnIndex);
    }

    protected Double getDoubleFromColumnName(Cursor cursor, String columnName )
    {
        int columnIndex = cursor.getColumnIndex(columnName);
        return cursor.getDouble(columnIndex);
    }

    protected Long getLongFromColumnName(Cursor cursor, String columnName )
    {
        int columnIndex = cursor.getColumnIndex(columnName);
        return cursor.getLong(columnIndex);
    }

    private void recordHasSynced()
    {

    }

}
