package com.ozsnow.mobile.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import com.ozsnow.mobile.app.models.vos.OrderVo;
import com.ozsnow.mobile.app.models.vos.TourGuideVo;

import java.util.ArrayList;

/**
 * Created by russellmilburn on 06/04/2016.
 */
public class TourGuideDataSource extends BaseDataSource implements IDataSource
{
    protected static final String TAG = TourGuideDataSource.class.toString();


    public TourGuideDataSource(Context context)
    {
        super(context);

        //resetHasSynced(TourGuideVo.TourGuideFields.TOUR_GUIDE_TBL);
    }

    public Integer createTourGuide(TourGuideVo vo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues cv = new ContentValues();
        cv.put(TourGuideVo.TourGuideFields.GUIDE_NAME, vo.getGuideName());
        cv.put(TourGuideVo.TourGuideFields.DATE, vo.getDate());
        cv.put(TourGuideVo.TourGuideFields.HAS_SYNCED, 0);

        long id = database.insert(TourGuideVo.TourGuideFields.TOUR_GUIDE_TBL, null, cv);

        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);

        return Integer.parseInt(id +"");
    }

    public ArrayList<TourGuideVo> readAllTourGuides()
    {
        ArrayList<TourGuideVo> list = new ArrayList<>();

        SQLiteDatabase database = open();
        String[] fields = new String[]
                {
                        TourGuideVo.TourGuideFields.GUIDE_ID,
                        TourGuideVo.TourGuideFields.GUIDE_NAME,
                        TourGuideVo.TourGuideFields.DATE,
                        TourGuideVo.TourGuideFields.HAS_SYNCED
                };

        Cursor cursor = database.query(TourGuideVo.TourGuideFields.TOUR_GUIDE_TBL,
                fields,
                null,
                null,
                null,
                null,
                null);

        TourGuideVo tourGuideVo = null;
        if (cursor.moveToFirst())
        {
            do {
                tourGuideVo = new TourGuideVo();
                tourGuideVo.setGuideId(getIntFromColumnName(cursor, TourGuideVo.TourGuideFields.GUIDE_ID));
                tourGuideVo.setGuideName(getStringFromColumnName(cursor, TourGuideVo.TourGuideFields.GUIDE_NAME));

                tourGuideVo.setDate(getLongFromColumnName(cursor, TourGuideVo.TourGuideFields.DATE));
                if (getIntFromColumnName(cursor, TourGuideVo.TourGuideFields.HAS_SYNCED) == 1)
                {
                    tourGuideVo.setHasSynced(true);
                }
                else
                {
                    tourGuideVo.setHasSynced(false);
                }
                list.add(tourGuideVo);
            }
            while (cursor.moveToNext());
        }
        close(database);
        return list;
    }

    public TourGuideVo readTourGuide(int id)
    {
        SQLiteDatabase database = open();
        String[] fields = new String[]
                {
                        TourGuideVo.TourGuideFields.GUIDE_ID,
                        TourGuideVo.TourGuideFields.GUIDE_NAME,
                        TourGuideVo.TourGuideFields.DATE,
                        TourGuideVo.TourGuideFields.HAS_SYNCED
                };

        Cursor cursor = database.query(TourGuideVo.TourGuideFields.TOUR_GUIDE_TBL,
                fields,
                TourGuideVo.TourGuideFields.GUIDE_ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null);

        TourGuideVo tourGuideVo = null;
        if (cursor.moveToFirst())
        {
            do {
                tourGuideVo = new TourGuideVo();
                tourGuideVo.setGuideId(getIntFromColumnName(cursor, TourGuideVo.TourGuideFields.GUIDE_ID));
                tourGuideVo.setGuideName(getStringFromColumnName(cursor, TourGuideVo.TourGuideFields.GUIDE_NAME));

                tourGuideVo.setDate(getLongFromColumnName(cursor, TourGuideVo.TourGuideFields.DATE));
                if (getIntFromColumnName(cursor, TourGuideVo.TourGuideFields.HAS_SYNCED) == 1)
                {
                    tourGuideVo.setHasSynced(true);
                }
                else
                {
                    tourGuideVo.setHasSynced(false);
                }
            }
            while (cursor.moveToNext());
        }
        close(database);
        return tourGuideVo;
    }




    public void updateTourGuide(TourGuideVo vo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues updateValues = new ContentValues();
        updateValues.put(TourGuideVo.TourGuideFields.GUIDE_NAME, vo.getGuideName());
        updateValues.put(TourGuideVo.TourGuideFields.DATE, vo.getDate());
        updateValues.put(TourGuideVo.TourGuideFields.HAS_SYNCED, vo.hasSynced());

        database.update(TourGuideVo.TourGuideFields.TOUR_GUIDE_TBL,
                updateValues,
                String.format("%s=%d", TourGuideVo.TourGuideFields.GUIDE_ID, vo.getGuideId()),
                null);


        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);
    }

    public void deleteTourGuide(int tourGuideId)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();
        String clause = String.format("%s=%d", TourGuideVo.TourGuideFields.GUIDE_ID, tourGuideId);
        database.delete(TourGuideVo.TourGuideFields.TOUR_GUIDE_TBL, clause, null );

        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);

    }


    @Override
    public void setHasRecordSynced(int id, boolean hasSynced)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues updateValues = new ContentValues();
        updateValues.put(TourGuideVo.TourGuideFields.HAS_SYNCED, hasSynced);

        database.update(TourGuideVo.TourGuideFields.TOUR_GUIDE_TBL,
                updateValues,
                String.format("%s=%d", TourGuideVo.TourGuideFields.GUIDE_ID, id),
                null);


        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);

    }
}
