package com.ozsnow.mobile.app.database;

/**
 * Created by russellmilburn on 17/05/2016.
 */
public interface IDataSource
{
    void setHasRecordSynced(int id, boolean hasSynced);
}
