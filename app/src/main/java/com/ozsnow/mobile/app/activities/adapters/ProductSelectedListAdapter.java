package com.ozsnow.mobile.app.activities.adapters;

import android.content.Context;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.OrderDetailsActivity;
import com.ozsnow.mobile.app.models.vos.ProductVo;

import java.text.NumberFormat;
import java.util.List;

/**
 * Created by russellmilburn on 20/04/2016.
 */
public class ProductSelectedListAdapter extends RecyclerView.Adapter<ProductSelectedListAdapter.SelectedProductHolder>
{
    protected static final String TAG = ProductSelectedListAdapter.class.toString();

    private Context context;
    private List<ProductVo> selectedProducts;

    public ProductSelectedListAdapter(final Context context, List<ProductVo> productVos)
    {
        this.context = context;
        selectedProducts = productVos;
    }

    @Override
    public ProductSelectedListAdapter.SelectedProductHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.select_product_item, parent, false);
        return new SelectedProductHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductSelectedListAdapter.SelectedProductHolder viewHolder, int position)
    {
        viewHolder.bindView(position);
    }

    public void add(ProductVo vo)
    {
//        Log.i(TAG, "add: " +vo.toString());
        selectedProducts.add(vo);
        int itemIndex = selectedProducts.indexOf(vo);
        notifyItemInserted(itemIndex);
        notifyDataSetChanged();

    }

    public void remove(ProductVo vo)
    {
        int itemIndex = selectedProducts.indexOf(vo);
        selectedProducts.remove(itemIndex);
        notifyItemRemoved(itemIndex);
        notifyDataSetChanged();
    }

    public List<ProductVo> getSelectedProducts() {
        return selectedProducts;
    }

    public void setSelectedProducts(List<ProductVo> selectedProducts) {
        this.selectedProducts = selectedProducts;
    }

    @Override
    public int getItemCount() {
        return selectedProducts.size();
    }

    public class SelectedProductHolder extends RecyclerView.ViewHolder
    {
        private int position;

        private TextView productNameTextView;
        private TextView productDescTextView;
        protected ImageButton removeItemBtn;
        private TextView priceTextView;

        public SelectedProductHolder(View itemView)
        {
            super(itemView);

            productNameTextView = (TextView) itemView.findViewById(R.id.selectedProductItemNameTextView);
            productDescTextView = (TextView) itemView.findViewById(R.id.selectedProductItemDescTextView);
            priceTextView = (TextView) itemView.findViewById(R.id.selectedProductItemPriceTextView);

            removeItemBtn = (ImageButton) itemView.findViewById(R.id.removeProductBtn);

        }



        public void bindView(int position)
        {
            this.position = position;
            Log.i(TAG, position +"");

            ProductVo vo = selectedProducts.get(position);

//            Log.i(TAG, "bindView" + vo.toString());

            productNameTextView.setText(vo.getProductName());
            productDescTextView.setText(vo.getProductDesc());
            priceTextView.setText(NumberFormat.getCurrencyInstance().format(vo.getPrice()));

            removeItemBtn.setOnClickListener(removeItemClickListener);
        }


        View.OnClickListener removeItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                ProductVo vo = selectedProducts.get(position);
                remove(vo);
            }
        };
    }


}
