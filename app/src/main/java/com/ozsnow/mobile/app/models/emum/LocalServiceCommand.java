package com.ozsnow.mobile.app.models.emum;

import java.io.Serializable;

/**
 * Created by russellmilburn on 06/04/2016.
 */
public enum LocalServiceCommand implements Serializable
{
    CREATE_TOUR_GUIDE;

    private LocalServiceCommand()
    {

    }
}
