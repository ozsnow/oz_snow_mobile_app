package com.ozsnow.mobile.app.services.sync;

import com.ozsnow.mobile.app.models.vos.OrderVo;

import java.util.ArrayList;

/**
 * Created by russellmilburn on 15/05/2016.
 */
public interface DataListener
{
    void onDataReady(ArrayList<OrderVo> orders);
}
