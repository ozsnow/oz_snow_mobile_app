package com.ozsnow.mobile.app.services.printer;

import android.os.Looper;

/**
 * Created by russellmilburn on 08/05/2016.
 */
public class ThermalPrinterThread extends Thread
{
    private ThermalPrinterHandler handler;

    @Override
    public void run()
    {
        super.run();
        Looper.prepare();
        handler = new ThermalPrinterHandler();
        Looper.loop();
    }

    public ThermalPrinterHandler getHandler() {
        return handler;
    }

    public void setHandler(ThermalPrinterHandler handler) {
        this.handler = handler;
    }
}
