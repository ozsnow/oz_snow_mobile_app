package com.ozsnow.mobile.app.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.nfc.Tag;
import android.util.Log;
import com.ozsnow.mobile.app.models.vos.*;
import com.ozsnow.mobile.app.services.printer.ThermalPrinterService;
import com.ozsnow.mobile.app.services.sync.DataListener;
import com.ozsnow.mobile.app.services.sync.SyncDataService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by russellmilburn on 28/04/2016.
 */
public class OrderDataSource extends BaseDataSource implements IDataSource
{
    public static final String TAG = OrderDataSource.class.toString();
    private DataListener listener;

    public OrderDataSource(Context context)
    {
        super(context);


        //resetHasSynced(OrderVo.OrderFields.ORDER_TBL);
    }

    public DataListener getListener() {
        return listener;
    }

    public void setListener(DataListener listener) {
        this.listener = listener;
    }

    public OrderVo createOrder(OrderVo vo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues cv = new ContentValues();

        cv.put(OrderVo.OrderFields.CUSTOMER_ID, vo.getCustomerId());
        cv.put(TourGuideVo.TourGuideFields.GUIDE_ID, vo.getGuideId());
        cv.put(OrderVo.OrderFields.ORDER_TOTAL, vo.getOrderTotal().doubleValue());
        cv.put(OrderVo.OrderFields.HAS_SYNCED, vo.isHasSynced());
        cv.put(OrderVo.OrderFields.IS_VOID_ORDER, vo.isVoidOrder());
        cv.put(OrderVo.OrderFields.TRIP_DURATION, vo.getTripDuration());
        cv.put(OrderVo.OrderFields.ORDER_DATE, vo.getOrderDate());

        long id = database.insert(OrderVo.OrderFields.ORDER_TBL, null, cv);

        vo.setOrderId(Integer.parseInt(id + ""));
        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);
        return vo;

    }

    public void createOrderDetail(OrderDetailsVo vo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues cv = new ContentValues();

        cv.put(OrderVo.OrderFields.ORDER_ID, vo.getOrderId());
        cv.put(ProductVo.ProductFields.PRODUCT_ID, vo.getProductId());
        cv.put(ProductVo.ProductFields.PRODUCT_NAME, vo.getProductName());
        cv.put(ProductVo.ProductFields.PRODUCT_DESC, vo.getProductDesc());
        cv.put(ProductVo.ProductFields.CATEGORY_ID, vo.getCategoryId());
        cv.put(OrderDetailsVo.OrderDetailsFields.NUMBER_OF_DAYS, vo.getNoOfDays());
        cv.put(OrderDetailsVo.OrderDetailsFields.QUANTITY, vo.getQuantity());
        cv.put(OrderDetailsVo.OrderDetailsFields.PRICE, vo.getPrice().doubleValue());
        cv.put(OrderDetailsVo.OrderDetailsFields.RES_CODE, vo.getResCode());

        database.insert(OrderDetailsVo.OrderDetailsFields.ORDER_DETAILS_TBL, null, cv);

        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);
    }

    public int checkNumberOfUnsyncedOrders()
    {
        SQLiteDatabase database = open();

        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + OrderVo.OrderFields.ORDER_TBL + ", " + OrderDetailsVo.OrderDetailsFields.ORDER_DETAILS_TBL +
                        " WHERE "+ OrderVo.OrderFields.HAS_SYNCED  + " =0" +
                " AND OrderTbl.orderId = OrderDetailsTbl.orderId", null);

        int rowCount = cursor.getCount();
        close(database);

        return rowCount;
    }

    public void readUnsyncedOrders()
    {
        ArrayList<OrderVo> unsyncedOrders = new ArrayList<>();
        SQLiteDatabase database = open();

        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + OrderVo.OrderFields.ORDER_TBL + ", " + OrderDetailsVo.OrderDetailsFields.ORDER_DETAILS_TBL +
                        " WHERE "+ OrderVo.OrderFields.HAS_SYNCED  + " =0" +
                        " AND OrderTbl.orderId = OrderDetailsTbl.orderId", null);

        OrderVo vo = null;
        if (cursor.moveToFirst())
        {

            do
            {
                vo = new OrderVo();
                vo.setOrderId(getIntFromColumnName(cursor, OrderVo.OrderFields.ORDER_ID));
                vo.setCustomerId(getIntFromColumnName(cursor, OrderVo.OrderFields.CUSTOMER_ID));
                vo.setGuideId(getIntFromColumnName(cursor, TourGuideVo.TourGuideFields.GUIDE_ID));
                Double orderTotal = getDoubleFromColumnName(cursor, OrderVo.OrderFields.ORDER_TOTAL);
                if (orderTotal != 0.00)
                {
                    unsyncedOrders.add(vo);
                }

            }
            while (cursor.moveToNext());
        }

        close(database);

        if (listener != null)
        {
            listener.onDataReady(unsyncedOrders);
        }

    }


    public void updateOrder(OrderVo vo)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues updateValues = new ContentValues();
        updateValues.put(OrderVo.OrderFields.CUSTOMER_ID, vo.getCustomerId());
        updateValues.put(TourGuideVo.TourGuideFields.GUIDE_ID, vo.getGuideId());
        updateValues.put(OrderVo.OrderFields.ORDER_TOTAL, vo.getOrderTotal().doubleValue());
        updateValues.put(OrderVo.OrderFields.HAS_SYNCED, vo.isHasSynced());
        updateValues.put(OrderVo.OrderFields.IS_VOID_ORDER, vo.isVoidOrder());
        updateValues.put(OrderVo.OrderFields.TRIP_DURATION, vo.getTripDuration());
        updateValues.put(OrderVo.OrderFields.ORDER_DATE, vo.getOrderDate());

        database.update(OrderVo.OrderFields.ORDER_TBL,
                updateValues,
                String.format("%s=%d", OrderVo.OrderFields.ORDER_ID, vo.getOrderId()),
                null);


        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);
    }

    public ArrayList<OrderVo> readLiveOrders()
    {
        SQLiteDatabase database = open();

        ArrayList<OrderVo> liveOrders = new ArrayList<>();

        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + OrderVo.OrderFields.ORDER_TBL +
//                        " WHERE " + OrderVo.OrderFields.IS_VOID_ORDER + " =0" +
                        " WHERE " + OrderVo.OrderFields.HAS_SYNCED  + " =0", null);

        OrderVo vo = null;
        if (cursor.moveToFirst())
        {
            do
            {
                vo = new OrderVo();
                vo.setOrderId(getIntFromColumnName(cursor, OrderVo.OrderFields.ORDER_ID));
                vo.setCustomerId(getIntFromColumnName(cursor, OrderVo.OrderFields.CUSTOMER_ID));
                vo.setGuideId(getIntFromColumnName(cursor, TourGuideVo.TourGuideFields.GUIDE_ID));
                vo.setOrderTotal(new BigDecimal(getDoubleFromColumnName(cursor, OrderVo.OrderFields.ORDER_TOTAL)));
                vo.setOrderDate(getLongFromColumnName(cursor, OrderVo.OrderFields.ORDER_DATE));

                if (getIntFromColumnName(cursor, OrderVo.OrderFields.IS_VOID_ORDER) == 1)
                {
                    vo.setVoidOrder(true);
                }
                else
                {
                    vo.setVoidOrder(false);
                }

                if (getIntFromColumnName(cursor, OrderVo.OrderFields.HAS_SYNCED) == 1)
                {
                    vo.setHasSynced(true);
                }
                else
                {
                    vo.setHasSynced(false);
                }

                vo.setTripDuration(getIntFromColumnName(cursor, OrderVo.OrderFields.TRIP_DURATION));

                liveOrders.add(vo);

            }
            while (cursor.moveToNext());

        }

        close(database);

        return liveOrders;

    }

    public boolean saveOrder(OrderVo vo)
    {
        updateOrder(vo);

        for (int i = 0; i < vo.getSelectedProducts().size(); i++)
        {
            ProductVo selectedProduct = vo.getSelectedProducts().get(i);

            OrderDetailsVo orderDetailsVo = new OrderDetailsVo();
            orderDetailsVo.setOrderId(vo.getOrderId());
            orderDetailsVo.setQuantity(1);
            orderDetailsVo.setProductName(selectedProduct.getProductName());
            orderDetailsVo.setProductDesc(selectedProduct.getProductDesc());
            orderDetailsVo.setProductId(selectedProduct.getProductID());
            orderDetailsVo.setCategoryId(selectedProduct.getCategoryID());

            int noOfDays = 0;
            try {
                noOfDays = Integer.parseInt(String.valueOf(selectedProduct.getProductDesc().charAt(0)));
                if (selectedProduct.getProductDesc().charAt(1) == '.')
                {
                    noOfDays = 6;
                }

            }
            catch (NumberFormatException nfe)
            {

            }

            orderDetailsVo.setNoOfDays(noOfDays);
            orderDetailsVo.setPrice(selectedProduct.getPrice());
            orderDetailsVo.setResCode(selectedProduct.getProductTypeVo().getResCode());

            createOrderDetail(orderDetailsVo);

        }



        return true;
    }


    public ArrayList<OrderDetailsVo> readOrderDetailItems(Integer orderId)
    {
        ArrayList<OrderDetailsVo> items = new ArrayList<>();

        SQLiteDatabase database = open();

        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + OrderDetailsVo.OrderDetailsFields.ORDER_DETAILS_TBL +
                        " WHERE " + OrderVo.OrderFields.ORDER_ID + " = " + orderId, null);

        OrderDetailsVo vo = null;
        if (cursor.moveToFirst())
        {
            do
            {
                vo = new OrderDetailsVo();
                vo.setOrderDetailsId(getIntFromColumnName(cursor, OrderDetailsVo.OrderDetailsFields.ORDER_DETAILS_ID));
                vo.setOrderId(getIntFromColumnName(cursor, OrderVo.OrderFields.ORDER_ID));
                vo.setProductId(getIntFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_ID));
                vo.setProductName(getStringFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_NAME));
                vo.setProductDesc(getStringFromColumnName(cursor, ProductVo.ProductFields.PRODUCT_DESC));
                vo.setCategoryId(getIntFromColumnName(cursor, ProductVo.ProductFields.CATEGORY_ID));
                vo.setQuantity(getIntFromColumnName(cursor, OrderDetailsVo.OrderDetailsFields.QUANTITY));
                vo.setNoOfDays(getIntFromColumnName(cursor, OrderDetailsVo.OrderDetailsFields.NUMBER_OF_DAYS));
                vo.setPrice(new BigDecimal(getDoubleFromColumnName(cursor, OrderDetailsVo.OrderDetailsFields.PRICE)));
                vo.setResCode(getStringFromColumnName(cursor, OrderDetailsVo.OrderDetailsFields.RES_CODE));

                items.add(vo);

            }
            while (cursor.moveToNext());

        }
        close(database);

        return items;
    }

    public OrderVo readOrder(Integer id)
    {
        SQLiteDatabase database = open();

        Cursor cursor = database.rawQuery(
                "SELECT * FROM " + OrderVo.OrderFields.ORDER_TBL +
                        " WHERE " + OrderVo.OrderFields.ORDER_ID + " = " + id, null);


        OrderVo vo = null;
        if (cursor.moveToFirst())
        {
            do
            {
                vo = new OrderVo();
                vo.setOrderId(getIntFromColumnName(cursor, OrderVo.OrderFields.ORDER_ID));
                vo.setCustomerId(getIntFromColumnName(cursor, OrderVo.OrderFields.CUSTOMER_ID));
                vo.setGuideId(getIntFromColumnName(cursor, TourGuideVo.TourGuideFields.GUIDE_ID));
                vo.setOrderTotal(new BigDecimal(getDoubleFromColumnName(cursor, OrderVo.OrderFields.ORDER_TOTAL)));
                vo.setOrderDate(getLongFromColumnName(cursor, OrderVo.OrderFields.ORDER_DATE));

                if (getIntFromColumnName(cursor, OrderVo.OrderFields.IS_VOID_ORDER) == 1)
                {
                    vo.setVoidOrder(true);
                }
                else
                {
                    vo.setVoidOrder(false);
                }

                if (getIntFromColumnName(cursor, OrderVo.OrderFields.HAS_SYNCED) == 1)
                {
                    vo.setHasSynced(true);
                }
                else
                {
                    vo.setHasSynced(false);
                }

                vo.setTripDuration(getIntFromColumnName(cursor, OrderVo.OrderFields.TRIP_DURATION));

            }
            while (cursor.moveToNext());

        }
        close(database);

        vo.setOrderItems(readOrderDetailItems(vo.getOrderId()));
        return vo;
    }

    public void deleteOrder(int id)
    {
        SQLiteDatabase database = open();

        database.delete(OrderVo.OrderFields.ORDER_TBL,
                OrderVo.OrderFields.ORDER_ID + " = ?",
                new String[]{Integer.toString(id)});

        deleteOrderDetails(id);

        close(database);
    }

    public void deleteOrderDetails(int id)
    {
        SQLiteDatabase database = open();
        database.delete(OrderDetailsVo.OrderDetailsFields.ORDER_DETAILS_TBL,
                OrderVo.OrderFields.ORDER_ID + " = ?",
                new String[]{Integer.toString(id)});
        close(database);
    }

    public ArrayList<OrderVo> readOrdersByDate(Date startDate, Date endDate)
    {
        ArrayList<OrderVo> orders = new ArrayList<>();
        SQLiteDatabase database = open();

        Log.i(TAG, startDate + "");
        Log.i(TAG, endDate + "");



        Cursor cursor = database.rawQuery(
                "SELECT " + OrderVo.OrderFields.ORDER_ID +" FROM " + OrderVo.OrderFields.ORDER_TBL +
                        " WHERE " + OrderVo.OrderFields.ORDER_DATE + " > " + startDate.getTime()+
                        " AND " + OrderVo.OrderFields.ORDER_DATE +" < " + endDate.getTime(), null);




        if (cursor.moveToFirst())
        {
            do
            {
                int id = getIntFromColumnName(cursor, OrderVo.OrderFields.ORDER_ID);
                OrderVo vo = readOrder(id);
                orders.add(vo);

            }
            while (cursor.moveToNext());
        }

        close(database);

        return orders;

    }



    @Override
    public void setHasRecordSynced(int id, boolean hasSynced)
    {
        SQLiteDatabase database = open();
        database.beginTransaction();

        ContentValues updateValues = new ContentValues();
        updateValues.put(OrderVo.OrderFields.HAS_SYNCED, hasSynced);

        database.update(OrderVo.OrderFields.ORDER_TBL,
                updateValues,
                String.format("%s=%d", OrderVo.OrderFields.ORDER_ID, id),
                null);


        database.setTransactionSuccessful();
        database.endTransaction();
        close(database);
    }
}
