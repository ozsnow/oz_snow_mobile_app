package com.ozsnow.mobile.app.services.printer;


import android.app.Notification;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Binder;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.PrinterSettingsActivity;
import com.ozsnow.mobile.app.models.vos.DeviceVo;
import com.zj.btsdk.BluetoothService;

import static android.R.attr.id;

/**
 * Created by russellmilburn on 08/05/2016.
 */
public class ThermalPrinterService extends Service
{
    private static final String TAG = ThermalPrinterService.class.toString();

    public static final int SCAN_COMMAND = 1;
    public static final int CONNECT_COMMAND = 2;
    public static final int PRINT_COMMAND = 3;
    public static final int PRINT_IMAGE_COMMAND = 4;

    private ThermalPrinterHandler printerHandler;
    private BluetoothService printerConnection;
    private ConnectionHandler bluetoothHandler;
    private Binder binder = new LocalBinder();
    private boolean isConnected;
    private int serviceId;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "onCreate");

        this.listener = null;

        ThermalPrinterThread thread = new ThermalPrinterThread();
        thread.setName("BluetoothThread");
        thread.start();


        while (thread.getHandler() == null)
        {

        }

        bluetoothHandler = new ConnectionHandler(getApplicationContext(), ThermalPrinterService.this);
        printerConnection = new BluetoothService(getApplicationContext(), bluetoothHandler);

        IntentFilter receiverFilter = new IntentFilter();
        receiverFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        receiverFilter.addAction(BluetoothDevice.ACTION_FOUND);
        receiverFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(bluetoothReceiver, receiverFilter);

        printerHandler = thread.getHandler();
        printerHandler.setService(this);

        //Init BluetoothService
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        Log.i(TAG, "onStartCommand() ");

        serviceId = startId;

        Message message = null;

        if (intent.getAction().equals(PrinterSettingsActivity.SCAN_REQUEST))
        {
            message = Message.obtain();
            message.what = SCAN_COMMAND;
            printerHandler.sendMessage(message);
        }
        else if (intent.getAction().equals(PrinterSettingsActivity.CONNECT_REQUEST))
        {
            message = Message.obtain();
            message.what = CONNECT_COMMAND;
            message.obj = intent.getSerializableExtra(PrinterSettingsActivity.DEVICE_EXTRA);
            printerHandler.sendMessage(message);
        }
        else if (intent.getAction().equals(PrinterSettingsActivity.PRINT_REQUEST))
        {
            message = Message.obtain();
            message.what = PRINT_COMMAND;
            message.obj = intent.getSerializableExtra(PrinterSettingsActivity.PRINT_ORDER_EXTRA);
            printerHandler.sendMessage(message);
        }
        else if (intent.getAction().equals(PrinterSettingsActivity.PRINT_IMAGE_REQUEST))
        {
            message = Message.obtain();
            message.what = PRINT_IMAGE_COMMAND;
            message.obj = intent.getSerializableExtra(PrinterSettingsActivity.PRINT_IMAGE_EXTRA);
            printerHandler.sendMessage(message);
        }


        return START_NOT_STICKY;
    }


    private BroadcastReceiver bluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent)
        {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                Log.i(TAG, "Device Found");

                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                DeviceVo vo = new DeviceVo();
                vo.setDeviceName(device.getName());
                vo.setMacAddress(device.getAddress());

                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    Log.i(TAG, "Not Bonded: " +device.getName() + " " + device.getAddress());
                    vo.setBonded(false);
                }
                else
                {
                    Log.i(TAG, "Bonded: " +device.getName() + " " + device.getAddress());
                    vo.setBonded(true);
                }

                if(listener != null)
                {
                    listener.onDeviceFound(vo);
                }


            }
            else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
            {
                Log.i(TAG, "Discover Finished");
                if(listener != null)
                {
                    listener.onScanningStatus(action);
                }
            }
            else if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action))
            {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);

                if (BluetoothAdapter.STATE_OFF == state)
                {
                    stopService();
                }
            }
        }
    };





    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        Log.i(TAG, "onBind() ");
        return binder;
    }

    public boolean isOpen()
    {
        if (printerConnection != null)
        {
            return printerConnection.isBTopen();
        }
        return false;
    }

    public void onConnectionStatus(String action)
    {
        if (listener != null)
        {
            listener.onConnectionStatus(action);
        }
    }

    public void printImage(byte[] image)
    {
        printerConnection.write(image);
    }

    public void writeToPrinter(String order)
    {
        printerConnection.sendMessage(order, "GBK");
    }

    public boolean isConnected()
    {
        return isConnected;
    }

    public void setConnected(boolean connected) {

        isConnected = connected;
        if (isConnected)
        {
            Notification.Builder notificationBuilder = new Notification.Builder(this);
            notificationBuilder.setSmallIcon(R.drawable.ic_printer_connected);
            notificationBuilder.setContentTitle("Bluetooth Printer Connected");
            Notification notification = notificationBuilder.build();
            startForeground(12, notification);
        }
        else
        {
            stopForeground(true);
        }
    }

    public void connectToPrinter(DeviceVo vo)
    {
        printerConnection.cancelDiscovery();
        BluetoothDevice device = printerConnection.getDevByMac(vo.getMacAddress());
        printerConnection.connect(device);
    }


    public void discoverDevices()
    {
        if (printerConnection.isDiscovering())
        {
            printerConnection.cancelDiscovery();
        }

        printerConnection.startDiscovery();
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopService();
    }

    private void stopService()
    {
        if (isConnected())
        {
            stopForeground(true);
        }
        unregisterReceiver(bluetoothReceiver);
        stopSelf(serviceId);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");

        stopService();

    }

    public class LocalBinder extends Binder
    {
        public ThermalPrinterService getService()
        {
            return ThermalPrinterService.this;
        }
    }

    private ThermalPrinterService.ConnectionListener listener;

    public ConnectionListener getListener() {
        return listener;
    }

    public void setListener(ConnectionListener listener) {
        this.listener = listener;
        listener.onConnectToService();
    }

    public interface ConnectionListener
    {
        void onConnectToService();
        void onConnectionStatus(String action);
        void onDeviceFound(DeviceVo vo);
        void onScanningStatus(String action);


    }
}
