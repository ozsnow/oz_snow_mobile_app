package com.ozsnow.mobile.app.database.tasks;

import android.os.AsyncTask;
import com.ozsnow.mobile.app.database.OrderDataSource;
import com.ozsnow.mobile.app.models.vos.OrderVo;

/**
 * Created by russellmilburn on 10/04/2016.
 */
public class ReadOrderTask extends AsyncTask<Integer, Void, OrderVo>
{
    private OrderDataSource dataSource;

    public ReadOrderTask(OrderDataSource dataSource)
    {
        super();
        this.dataSource = dataSource;
    }

    @Override
    protected OrderVo doInBackground(Integer... ids)
    {
        return dataSource.readOrder(ids[0]);
    }


}
