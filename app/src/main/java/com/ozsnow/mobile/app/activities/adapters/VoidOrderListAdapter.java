package com.ozsnow.mobile.app.activities.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ozsnow.mobile.app.R;
import com.ozsnow.mobile.app.activities.VoidOrderActivity;
import com.ozsnow.mobile.app.models.vos.VoidOrderListVo;

import java.text.NumberFormat;
import java.util.List;

import static com.ozsnow.mobile.app.OzSnowPosApplication.TAG;

/**
 * Created by russellmilburn on 11/05/2016.
 */
public class VoidOrderListAdapter extends RecyclerView.Adapter<VoidOrderListAdapter.VoidOrderListItemHolder>
{
    protected Context context;
    protected List<VoidOrderListVo> voidOrderListVos;

    public VoidOrderListAdapter(Context context, List<VoidOrderListVo> orders)
    {
        this.context = context;
        this.voidOrderListVos = orders;
    }

    @Override
    public VoidOrderListItemHolder onCreateViewHolder(ViewGroup viewGroup, int viewType)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.order_list_item, viewGroup, false);
        return new VoidOrderListItemHolder(view);
    }

    public List<VoidOrderListVo> getList()
    {
        return voidOrderListVos;
    }

    public void setList(List<VoidOrderListVo> voidOrderListVos) {
        this.voidOrderListVos = voidOrderListVos;
    }

    @Override
    public void onBindViewHolder(VoidOrderListItemHolder holder, int position)
    {
        holder.bindView(position);
    }

    @Override
    public int getItemCount() {
        return voidOrderListVos.size();
    }

    public class VoidOrderListItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private TextView orderIdField;
        private TextView customerNameField;
        private TextView orderTotalField;
        private CardView cardView;
        protected VoidOrderListVo vo;
        protected int position;



        public VoidOrderListItemHolder(View itemView) {
            super(itemView);

            orderIdField = (TextView) itemView.findViewById(R.id.void_order_id_field);
            customerNameField = (TextView) itemView.findViewById(R.id.void_customer_name_field);
            orderTotalField = (TextView) itemView.findViewById(R.id.void_order_total_field);
            cardView = (CardView) itemView.findViewById(R.id.void_order_card_view);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v)
        {
            VoidOrderActivity activity = (VoidOrderActivity) context;
            if (vo.getOrderVo().isVoidOrder() == false)
            {
                cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cardBackground));
                //voidOrderListVos.get(position).getOrderVo().setVoidOrder(true);
                activity.voidOrder(position);
                notifyDataSetChanged();
            }


        }

        public void bindView(int position)
        {

            this.position = position;
            vo = voidOrderListVos.get(position);
            orderIdField.setText("Order ID: " + vo.getTerminalId() + "010" + vo.getOrderVo().getOrderId());
            customerNameField.setText("Customer: " + vo.getCustomerVo().getFirstName() + " " +  vo.getCustomerVo().getLastName());
            orderTotalField.setText("Order Total: " + NumberFormat.getCurrencyInstance().format(vo.getOrderVo().getOrderTotal()));

            if (vo.getOrderVo().isVoidOrder())
            {
                cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.void_order));
            }
            else
            {
                cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.cardBackground));
            }
        }
    }
}
