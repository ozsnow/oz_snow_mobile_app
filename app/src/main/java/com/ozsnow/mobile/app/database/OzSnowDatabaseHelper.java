package com.ozsnow.mobile.app.database;

import android.content.ContentResolver;
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;
import com.ozsnow.mobile.app.models.vos.*;

/**
 * Created by russellmilburn on 06/03/2016.
 */
public class OzSnowDatabaseHelper extends SQLiteOpenHelper
{


    private static final String TAG = OzSnowDatabaseHelper.class.toString();
    public static final String AUTHORITY = "com.ozsnow.mobile.app.provider";
    public static final Uri BASE_CONTENT_URI = new Uri.Builder().scheme(ContentResolver.SCHEME_CONTENT).authority(AUTHORITY).build();

    public static final String DB_NAME = "OzSnowPosDatabase.db";
    public static final int DB_VERSION = 2;


    public OzSnowDatabaseHelper(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
        Log.i(TAG, "OzSnowDatabaseHelper() ");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase)
    {
        Log.i(TAG, "onCreate() ");

        String productCreateQuery = "CREATE TABLE " + ProductVo.ProductFields.PRODUCT_TBL + " " +
                "(" + ProductVo.ProductFields.PRODUCT_ID + " INTEGER PRIMARY KEY, " +
                ProductVo.ProductFields.PRODUCT_NAME + " VARCHAR, " +
                ProductVo.ProductFields.PRODUCT_DESC + " VARCHAR, " +
                ProductVo.ProductFields.MOUNTAIN + " VARCHAR, " +
                ProductVo.ProductFields.CATEGORY_ID + " INTEGER," +
                "FOREIGN KEY ("+ ProductVo.ProductFields.CATEGORY_ID+ ") REFERENCES " +
                CategoryVo.CategoryFields.CATEGORY_TBL +
                "(" + CategoryVo.CategoryFields.CATEGORY_ID +"));";

        String productTypeCreateQuery = "CREATE TABLE " + ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_TBL + " " +
                "(" + ProductTypeVo.ProductTypeFields.PRODUCT_TYPE_ID + " INTEGER PRIMARY KEY, " +
                ProductTypeVo.ProductTypeFields.PRODUCT_TYPE + " VARCHAR, " +
                ProductTypeVo.ProductTypeFields.IS_HIRE_PRODUCT + " INTEGER, " +
                ProductTypeVo.ProductTypeFields.RES_CODE + " VARCHAR, " +
                ProductTypeVo.ProductTypeFields.PRICE_1D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_2D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_3D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_4D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_5D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_6D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_7D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_8D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_9D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_10D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_11D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_12D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_13D + " REAL, " +
                ProductTypeVo.ProductTypeFields.PRICE_14D + " REAL, " +
                ProductVo.ProductFields.PRODUCT_ID + " INTEGER)";

        //Category table set up
        String categoryCreateQuery = "CREATE TABLE " + CategoryVo.CategoryFields.CATEGORY_TBL + " " +
                "(" + CategoryVo.CategoryFields.CATEGORY_ID + " INTEGER PRIMARY KEY, " +
                CategoryVo.CategoryFields.CATEGORY_NAME + " VARCHAR)";

        String customerCreateQuery = "CREATE TABLE " + CustomerVo.CustomerFields.CUSTOMER_TBL + " " +
                "(" + CustomerVo.CustomerFields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CustomerVo.CustomerFields.FIRST_NAME + " VARCHAR, " +
                CustomerVo.CustomerFields.LAST_NAME + " VARCHAR, " +
                CustomerVo.CustomerFields.HAS_SYNCED + " INTEGER)";

        String tourGuideCreateQuery = "CREATE TABLE " + TourGuideVo.TourGuideFields.TOUR_GUIDE_TBL + " " +
                "(" + TourGuideVo.TourGuideFields.GUIDE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TourGuideVo.TourGuideFields.GUIDE_NAME + " VARCHAR, " +
                TourGuideVo.TourGuideFields.DATE + " REAL, " +
                TourGuideVo.TourGuideFields.HAS_SYNCED + " INTEGER)";

        String orderCreateQuery = "CREATE TABLE " + OrderVo.OrderFields.ORDER_TBL + " " +
                "(" + OrderVo.OrderFields.ORDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                OrderVo.OrderFields.CUSTOMER_ID + " INTEGER, " +
                TourGuideVo.TourGuideFields.GUIDE_ID + " INTEGER, " +
                OrderVo.OrderFields.IS_VOID_ORDER + " INTEGER, " +
                OrderVo.OrderFields.HAS_SYNCED + " INTEGER, " +
                OrderVo.OrderFields.TRIP_DURATION + " INTEGER, " +
                OrderVo.OrderFields.ORDER_TOTAL + " REAL, " +
                OrderVo.OrderFields.ORDER_DATE + " REAL)";

        String orderDetailsCreateQuery = "CREATE TABLE " + OrderDetailsVo.OrderDetailsFields.ORDER_DETAILS_TBL + " " +
                "(" + OrderDetailsVo.OrderDetailsFields.ORDER_DETAILS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                OrderVo.OrderFields.ORDER_ID + " INTEGER, " +
                ProductVo.ProductFields.PRODUCT_ID + " INTEGER, " +
                ProductVo.ProductFields.PRODUCT_NAME + " VARCHAR, " +
                ProductVo.ProductFields.PRODUCT_DESC + " VARCHAR, " +
                ProductVo.ProductFields.CATEGORY_ID + " INTEGER, " +
                OrderDetailsVo.OrderDetailsFields.QUANTITY + " INTEGER, " +
                OrderDetailsVo.OrderDetailsFields.RES_CODE + " VARCHAR, " +
                OrderDetailsVo.OrderDetailsFields.NUMBER_OF_DAYS + " INTEGER, " +
                OrderDetailsVo.OrderDetailsFields.PRICE + " REAL)";


        try {
            sqLiteDatabase.execSQL(categoryCreateQuery);
            sqLiteDatabase.execSQL(productCreateQuery);
            sqLiteDatabase.execSQL(productTypeCreateQuery);
            sqLiteDatabase.execSQL(customerCreateQuery);
            sqLiteDatabase.execSQL(tourGuideCreateQuery);
            sqLiteDatabase.execSQL(orderCreateQuery);
            sqLiteDatabase.execSQL(orderDetailsCreateQuery);

        }
        catch (SQLException sqle)
        {
            sqle.printStackTrace();
        }
    }



    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
    {
        //sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + UserData.USER_TABLE_NAME);
        //onCreate(sqLiteDatabase);
        Log.i(TAG, "onUpgrade");

//        if (newVersion == 2)
//        {
//            sqLiteDatabase.execSQL("ALTER TABLE OrderDetailsTbl ADD NoOfDays INTEGER DEFAULT 0 NULL;");
//        }
    }
}
