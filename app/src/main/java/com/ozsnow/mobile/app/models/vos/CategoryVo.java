package com.ozsnow.mobile.app.models.vos;

import android.provider.BaseColumns;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by russellmilburn on 03/04/2016.
 */
public class CategoryVo implements Serializable {

    public static final class  CategoryFields implements BaseColumns
    {
        public static final String CATEGORY_TBL = "CategoryTbl";
        public static final String CATEGORY_ID = "categoryId";
        public static final String CATEGORY_NAME = "categoryName";
    }

    private int categoryID;
    private String categoryName;

    public CategoryVo()
    {

    }

    public CategoryVo(JSONObject vo)
    {
        try {
            this.categoryID = vo.getInt(CategoryFields.CATEGORY_ID);
            this.categoryName = vo.getString(CategoryFields.CATEGORY_NAME);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public int getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString()
    {
        return "[CategoryVo id: " + this.categoryID +
                ", categoryName: " + this.categoryName +
                "]";
    }
}
