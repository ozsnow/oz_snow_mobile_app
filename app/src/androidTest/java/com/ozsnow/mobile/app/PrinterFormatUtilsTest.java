package com.ozsnow.mobile.app;

import android.support.test.runner.AndroidJUnit4;
import com.ozsnow.mobile.app.models.vos.OrderDetailsVo;
import com.ozsnow.mobile.app.models.vos.ProductVo;
import com.ozsnow.mobile.app.utils.PrinterFormatUtils;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.math.BigDecimal;

/**
 * Created by russellmilburn on 07/05/2016.
 */
@RunWith(AndroidJUnit4.class)
public class PrinterFormatUtilsTest extends TestCase
{
    private ProductVo productCat1;
    private ProductVo productCat2;
    private ProductVo productCat2A;
    private ProductVo productCat2B;
    private ProductVo productCat2C;
    private ProductVo productCat3;
    private OrderDetailsVo orderItem;

    @Before
    public void setUp() throws Exception {
        super.setUp();

//        setC
        productCat1 = new ProductVo();
        productCat1.setProductID(1);
        productCat1.setCategoryID(1);
        productCat1.setProductName("Skis, Poles and Boots");
        productCat1.setProductDesc("Skis, Poles, Boots");

        productCat2 = new ProductVo();
        productCat2.setCategoryID(2);
        productCat2.setProductID(9);
        productCat2.setProductName("Off Peak Ski Pack ");
        productCat2.setProductDesc("includes Lift Ticket, Skis, Poles and Boots ");

        productCat2A = new ProductVo();
        productCat2A.setCategoryID(2);
        productCat2A.setProductID(10);
        productCat2A.setProductName("Off Peak Ski Pack With Lessons");
        productCat2A.setProductDesc("includes Lift Ticket, Skis, Poles, Boots and Lessons ");


        productCat2B = new ProductVo();
        productCat2B.setCategoryID(2);
        productCat2B.setProductID(11);
        productCat2B.setProductName("Off Peak Snowboard Pack ");
        productCat2B.setProductDesc("includes Lift Ticket, Snowboard and Boots");

        productCat2C = new ProductVo();
        productCat2C.setCategoryID(2);
        productCat2C.setProductID(12);
        productCat2C.setProductName("Off Peak Snowboard Pack With Lessons ");
        productCat2C.setProductDesc("includes Lift Ticket, Snowboard, Boots and Lessons");

        productCat3 = new ProductVo();
        productCat3.setProductID(17);
        productCat3.setCategoryID(3);
        productCat3.setProductName("Off Peak Lift Pass");
        productCat3.setProductDesc("Off Peak Lift Pass");

        orderItem = new OrderDetailsVo();
        orderItem.setQuantity(1);
        orderItem.setProductName("Off Peak Lift Pass");
        orderItem.setProductDesc("3 Days");
        orderItem.setPrice(new BigDecimal(215.00));



    }

    public void testFormatLineItem() throws Exception {

    }

    public void testFormatHireItem() throws Exception {

    }

    public void testFormatPackItem() throws Exception {

    }

    public void testFormatListPassForPrinter() throws Exception {

    }

    @Test
    public void testFormatItem() throws Exception
    {
        PrinterFormatUtils.formatItem(productCat2, orderItem);
        PrinterFormatUtils.formatItem(productCat2A, orderItem);
        PrinterFormatUtils.formatItem(productCat2B, orderItem);
        PrinterFormatUtils.formatItem(productCat2C, orderItem);

        //System.out.println(s);

    }

}